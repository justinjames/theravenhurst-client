# Also see (https://bitbucket.org/tombrokeoff/theravenhurst-server)

#To Do
* clean up top of api/player/index.js
* fix title vert spacing on player page and maybe add eliminations title (done)
* add 'biggest pot' to player page / stats (done)
* check why some names are outside of bar on ldrbrd (done)
* add overall net to leaderboard (done)
* try switching red / dark gray on ldrbrd
* fix menu hover (a hover) no underline (done)
* try hover effect on players page avas bg border (done - not sure i like)
* add title to player page "stats" (done)
* fix edit game for phone view (done i think)
* fix high hand card display order, ex h14,h12,c14,c12,d14 (done, i think)
* add tab fwd / back for game view (done)
* download link on stats next to title
* make name clickable to player view in smack (done)
* reorder items on compare page (dont, just added gap after overall net)
* on player page, have phone & email inline unless view not wide (done)
* try 'Players : Justin ...' where Players is back link (done)
* try fade on over header img on player (done)
* on player second elim mt only when 2 are stacked (done)
* mess with having div fade over player header img (nah)
* underline on back 2 players on hover (done sort of)
* see how img looks on leaderbrd
* see how imgs look on compare
* try 2 rows of high hands
* fix find on players (DONE)
* move inactive players to bottom (done)
* try 2 cols on wide profile edit (done)
* try non blue text on home (done)
* try diff font for names on home
* i think player header is messed up, too far laft.  possibky add par div with fixed and make cur relative. (done)
* elimination ratio on stats (done)
* net ratio on stats (done)
* setup route to get stats average (done)
* fix the high hand net on game view (DONE)
* on home add a link to 'view full game' (DONE)
* add new smack then change tabs and come back (does not show up) (DONE)
* add TheRavenhurst.com header to home (when logo not showing) (DONE)
* check all calc() in scss to make sure they're valid (DONE)
* put in ngOnDestroy + unsubscribes
* finish layout of announcements (DONE kind of)
* build create new announcement functionality (DONE)
* finish layout of smack / messages (DONE)
* smack / messages create 2 subjects in message service - behv subj for init load of old msgs and subj for all new inc msgs (DONE)
* home page - get latest point winners (DONE)
* get game should include knockouts (DONE)
* add knockouts to socket server
* add finalize game to socket server
* have game updates update _leaders leadersSubject inside of players-service
* leaderboard (done for now but needs more items once we start getting more data) (DONE?)
* reorder phone number / email on new player (DONE)
* refactor the forms for new player, edit player, profile (DONE?)
* create new player did not update in list (DONE)

#basic commands
`ng build` `ng build --prod` `npm start` <-- will expect server to be running on :1234
