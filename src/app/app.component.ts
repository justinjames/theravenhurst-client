import { Component, OnInit } from '@angular/core'

import { GameService, PlayerService } from './core'

@Component({
    selector: 'raven-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    player = null

    constructor(
        // private scrollFixService : ScrollFixService,
        // private gameService : GameService,
        private playerService : PlayerService
    ){
        // this.scrollFixService.init()
        // this.gameService.init()
        this.playerService.init()
    }

    ngOnInit() {
        this.subscribeToPlayer()
    }

    private subscribeToPlayer() {
        this.playerService.playerSubject.subscribe(p => {
            // console.log(p)
            this.player = p
        })
    }
}