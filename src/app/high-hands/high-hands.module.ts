import { NgModule } from '@angular/core'

import { CoreModule } from '../core'
import { HighHandsComponent } from './high-hands.component'
import { HighHandsRouting } from './high-hands.routing'

@NgModule({
    imports: [
        CoreModule,
        HighHandsRouting
    ],
    declarations: [
        HighHandsComponent
    ]
})
export class HighHandsModule {}