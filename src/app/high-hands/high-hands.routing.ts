import { RouterModule } from '@angular/router'

import { AdminGuard, AuthenticatedGuard } from '../core'
import { HighHandsComponent } from './high-hands.component'

export const HighHandsRouting = RouterModule.forChild([
    { path: 'high-hands', component: HighHandsComponent, canActivate: [ AuthenticatedGuard ] }
])