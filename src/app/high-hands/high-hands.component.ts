import { Component, OnInit } from '@angular/core'

import { BrowserTitleService, DateService, Game, GameService, Hand, HandService, SortableHand, PlayerService } from '../core'

@Component({
    selector: 'raven-games',
    templateUrl: './high-hands.component.html',
    styleUrls: ['./high-hands.component.scss']
})
export class HighHandsComponent implements OnInit {

    sortedHands : SortableHand[] = []

    years : number[] = []
    year : number
    firstYear = 2017
    loading = true

    constructor(
        private browserTitleService : BrowserTitleService,
        private dateService : DateService,
        private gameService : GameService,
        private handService : HandService,
        private playerService : PlayerService
    ) {}

    ngOnInit() {
        this.browserTitleService.title = 'High Hands'
        this.setupYears()
    }

    private setupYears() {

        const now = new Date()
        let cur = now.getFullYear()

        while(cur >= this.firstYear) {
            this.years.push(cur--)
        }

        if(this.dateService.selectedYear) {
            this.year = this.dateService.selectedYear
        }else{
            this.year = this.years[0]
        }

        this.onYearChanged()
    }

    onYearChanged() {
        this.dateService.selectedYear = this.year
        this.loadHighHands()
    }

    private loadHighHands() {

        this.loading = true

        this.handService.loadHighHands(this.year)
            .then(hands => {
                this.sortedHands = this.handService.sortHighHands(hands)
                this.loading = false
            })
            .catch(err => {
                console.log(err)
            })
    }
}