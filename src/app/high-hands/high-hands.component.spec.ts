import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighHandsComponent } from './games.component';

describe('HighHandsComponent', () => {
  let component: HighHandsComponent;
  let fixture: ComponentFixture<HighHandsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighHandsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighHandsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
