import { Component, Input, AfterViewInit, OnChanges, SimpleChanges, ElementRef } from '@angular/core'

import * as d3 from 'd3'
import * as d3Scale from 'd3-scale'
import { line } from 'd3-shape'
import { Selection, select } from 'd3-selection'

import { Margins, GraphData } from '../../core'

@Component({
    selector: 'raven-stat-graph',
    templateUrl: './stat-graph.component.html',
    styleUrls: ['./stat-graph.component.scss']
})
export class StatGraphComponent implements AfterViewInit, OnChanges {

    @Input() data : GraphData[]
    @Input() prop : string
    @Input() resize : number

    myId = `graph${Math.round(Math.random() * 4000)}`

    private margins : Margins = {
        top: 0,
        right: 0,
        bottom: 0,
        left: 0
    }
    private fullWidth : number
    private fullHeight : number
    private graphWidth : number
    private graphHeight : number
    private xScale : d3Scale.ScaleLinear<any, any>
    private yScale : d3Scale.ScaleBand<any>
    private svg : Selection<any, any, any, any>
    private barGroup : Selection<any, any, any, any>
    private textGroupNames : Selection<any, any, any, any>
    private textGroupValues : Selection<any, any, any, any>
    private xMax : number
    private yMax : number

    private graphSetup = false
    private needToUpdate = false

    private barScalePerc = .94
    private textHeight = 10
    private minWidthForText = 220
    private minWidthForRightText = 40
    private transDuration = 240
    private transDelay = 140

    constructor(
        private el : ElementRef
    ) {}

    ngAfterViewInit() {
        this.setupGraph()
    }

    ngOnChanges(ch : SimpleChanges) {

        if(ch.resize.currentValue > 0) {
            this.checkIfResizeAffectedAnything()
        }else{
            if(this.graphSetup) {
                this.update()
            }else{
                this.needToUpdate = true
            }
        }
    }

    private setupGraph() {

        this.svg = d3.select('#' + this.myId)

        this.setGraphSizeVars()

        this.barGroup = this.svg.append('g')
            .attr('class', 'barGroup')
            .attr('transform', 'translate(' + this.margins.left + ',' + this.margins.top + ')')

        this.textGroupNames = this.svg.append('g')
            .attr('class', 'textGroupNames')
            .attr('transform', 'translate(' + this.margins.left + ',' + this.margins.top + ')')

        this.textGroupValues = this.svg.append('g')
            .attr('class', 'textGroupValues')
            .attr('transform', 'translate(' + this.margins.left + ',' + this.margins.top + ')')

        this.xScale = d3.scaleLinear()
        this.yScale = d3.scaleBand()

        this.setScaleRanges()

        this.graphSetup = true

        if(this.needToUpdate) {
            this.update()
        }
    }

    private checkIfResizeAffectedAnything() {
        if(this.fullWidth != this.el.nativeElement.offsetWidth) {
            this.setGraphSizeVars()
            this.setScaleRanges()
            this.update()
        }
    }

    private setGraphSizeVars() {
        this.fullWidth = this.el.nativeElement.offsetWidth
        this.fullHeight = this.el.nativeElement.offsetHeight
        this.graphWidth = this.fullWidth - this.margins.left - this.margins.right
        this.graphHeight = this.fullHeight - this.margins.top - this.margins.bottom
    }

    private setScaleRanges() {
        this.xScale.rangeRound([0, this.graphWidth])
        this.yScale.rangeRound([0, this.graphHeight])
    }

    private update() {
        this.updateDomains()
        this.updateBars()
        this.updateText()
    }

    private updateDomains() {

        const xVals = this.data.map(d => d.x)
        const yVals = this.data.map(d => d.y)

        this.xMax = this.data.length > 0 ? d3.max(xVals) : 10
        this.yMax = this.data.length > 0 ? d3.max(yVals) : 10

        this.xScale.domain([0, this.xMax])
        this.yScale.domain(yVals)
    }

    private getColor(item : GraphData, arr : GraphData[], text : boolean, right? : boolean) : string {

        let inside = false

        if(text) {

            const barW = this.xScale(item.x)
            const textPos = this.getTextPos(item, this, true, right)

            inside = textPos <= barW
        }

        const max = d3.max(arr, (i) => i.x)
        const allNotTop = arr.filter(i => i.x != max)
        // const allVals = allNotTop.map(i => i.y)
        // const low = d3.min(allVals)
        // const high = d3.max(allVals)

        let isLeader = item.x == max

        if(!isLeader && this.prop == 'totalPoints') {

            let numbMax = 0

            arr.forEach(i => {
                if(i.x == max) {
                    ++numbMax
                }
            })

            if(numbMax == 1) {

                const filtered = arr.filter(i => i.x != max)
                const secMax = d3.max(filtered, (i) => i.x)

                numbMax = 0

                filtered.forEach(i => {
                    if(i.x == secMax) {
                        ++numbMax
                    }
                })

                if(numbMax == 1) {
                    isLeader = item.x == secMax
                }
            }
        }

        // if(text) {
        //     console.log(item, isLeader, right, inside)
        // }

        if(item.name == 'Average') {
            return text ? inside ? '#fff' : '#000' : '#9e0000'
        }else{
            if(isLeader) {
                return text ? inside ?  '#fff' : '#444' : '#444'
            }else{
                return text ? '#000' : '#ddd'
            }
        }
    }

    private updateBars() {

        this.barGroup.selectAll('.bar')
            .data(this.data)
            .enter()
            .append('rect')
            .attr('class', 'bar')
            .attr('opacity', 0)
            .attr('x', 0)
            .attr('width', 0)
            .attr('y', d => this.yScale(d.y))
            .attr('height', d => this.yScale.bandwidth() * this.barScalePerc)
            .attr('fill', (d, i) => this.getColor(d, this.data, false))

        this.barGroup.selectAll('.bar')
            .data(this.data)
            .interrupt()
            .transition()
            .delay(d => this.transDelay * d.y)
            .duration(d => this.transDuration)
            .attr('opacity', 1)
            .attr('width', d => this.xScale(d.x))
            .attr('y', d => this.yScale(d.y))
            .attr('height', d => this.yScale.bandwidth() * this.barScalePerc)

        this.barGroup.selectAll('.bar')
            .data(this.data)
            .exit()
            .interrupt()
            .transition()
            .duration(d => this.transDuration)
            .attr('opacity', 0)
            .attr('width', 0)
            .attr('y', (d : GraphData) => this.yScale(<GraphData>d.y))
            .attr('height', d => this.yScale.bandwidth() * this.barScalePerc)
            .remove()
    }

    private getTextPos(d : GraphData, th : any, x : boolean, right : boolean) : number {

        const barHeight = Math.floor(this.yScale.bandwidth() * this.barScalePerc)
        const barWidth = this.xScale(d.x)
        const space = barHeight - this.textHeight
        const gap = space / 2
        const rightEdge = this.xScale(d.x)
        let textOutside = barWidth < this.minWidthForText
        let xGap = gap

        if(right) {
            textOutside = barWidth < this.minWidthForRightText
        }else{
            textOutside = barWidth < this.minWidthForText
        }

        // if(right) {
            // console.log(textOutside, this.minWidthForRightText, barWidth)
        // }

        // console.log(d, barWidth, this.minWidthForText)
        // console.log(barWidth, this.minWidthForText, this.minWidthForRightText, barWidth)

        if(textOutside) {
            xGap = 8
        }

        if(x) {

            if(right) {
                return textOutside ? -10 : rightEdge - xGap
            }

            if(textOutside) {
                return barWidth + xGap
            }

            return gap
        }

        return this.yScale(d.y) + gap + this.textHeight
    }

    private getValueDisplay(d : GraphData) : string {

        let ret = d.xDisp ? String(d.xDisp) : String(d.x)

        if(d.type == 'currency') {
            ret = '$' + ret
        }else if(d.type == 'percent') {
            ret += '%'
        }

        return ret
    }

    private isTextInBar() {

    }

    private updateText() {

        this.textGroupNames.selectAll('text')
            .data(this.data)
            .enter()
            .append('text')
            .attr('class', 'graphNameText')
            .attr('opacity', 0)
            .attr('x', d => this.getTextPos(d, this, true, false))
            .attr('y', d => this.getTextPos(d, this, false, false))
            .attr('fill', (d, i) => this.getColor(d, this.data, true))
            .text(d => {

                let end = ''

                if(this.getTextPos(d, this, true, true) == -10) {
                    end = ' ' + this.getValueDisplay(d)
                }

                return d.name + end
            })

        this.textGroupNames.selectAll('text')
            .data(this.data)
            .interrupt()
            .transition()
            .delay(d => this.transDelay * d.y)
            .duration(d => this.transDuration)
            .attr('opacity', 1)
            .attr('x', d => this.getTextPos(d, this, true, false))
            .attr('y', d => this.getTextPos(d, this, false, false))

        this.textGroupNames.selectAll('text')
            .data(this.data)
            .exit()
            .interrupt()
            .transition()
            .duration(d => this.transDuration)
            .attr('opacity', 0)
            .attr('x', d => this.getTextPos(d, this, true, false))
            .attr('y', d => this.getTextPos(d, this, false, false))
            .remove()

        /////////

        this.textGroupValues.selectAll('text')
            .data(this.data)
            .enter()
            .append('text')
            .attr('class', 'graphValueText')
            .attr('opacity', 0)
            .attr('text-anchor', 'end')
            .attr('x', d => this.getTextPos(d, this, true, false))
            .attr('y', d => this.getTextPos(d, this, false, false))
            .attr('fill', (d, i) => this.getColor(d, this.data, true, true))
            .text(d => this.getValueDisplay(d))

        this.textGroupValues.selectAll('text')
            .data(this.data)
            .interrupt()
            .transition()
            .delay(d => this.transDelay * d.y)
            .duration(d => this.transDuration)
            .attr('opacity', 1)
            .attr('x', d => this.getTextPos(d, this, true, true))
            .attr('y', d => this.getTextPos(d, this, false, false))

        this.textGroupValues.selectAll('text')
            .data(this.data)
            .exit()
            .interrupt()
            .transition()
            .duration(d => this.transDuration)
            .attr('opacity', 0)
            .attr('x', d => this.getTextPos(d, this, true, false))
            .attr('y', d => this.getTextPos(d, this, false, false))
            .remove()
    }
}