import { Component, OnInit } from "@angular/core";

import * as XLSX from "xlsx";

import {
    ArrayService,
    BrowserTitleService,
    DateService,
    Player,
    PlayerStats,
    PlayerStatsProp,
    PlayerService,
} from "../core";

@Component({
    selector: "raven-leaderboard",
    templateUrl: "./leaderboard.component.html",
    styleUrls: ["./leaderboard.component.scss"],
})
export class LeaderboardComponent implements OnInit {
    // totalEntries : number
    // attendance : number
    // totalPoints : number
    // winPercent : number
    // totalEntryCost : number
    // totalPotMoneyWon : number
    // potNet : number
    // bountiesWon : number
    // bountiesLost : number
    // bountiesNet : number
    // totalHighHandEntries : number
    // highHandsNet : number
    // highHandsWon
    // overallNet : number
    // totalEliminations : number
    // totalEliminated : number
    // knockoutWins : KnockoutTotal[]
    // knockoutLosses : KnockoutTotal[]
    // highHands : Hand[]
    // player? : Player

    players: PlayerStats[] = [];
    averages: PlayerStats = null;
    years: number[] = [];
    year: number;
    firstYear = 2017;
    loading = true;
    totalGames: number;
    yearlyPot: number;
    doingXlsx = false;

    stats: PlayerStatsProp[] = [
        {
            stat: "Points",
            prop: "totalPoints",
            class: "numb",
            notes: `This is the only stat on here that matters, but they're all kind of fun to look at.`,
            includeAverage: true,
        },
        {
            stat: "Tourneys Played",
            prop: "totalEntries",
            class: "numb",
            includeAverage: true,
        },
        {
            stat: "Win Percentage",
            prop: "winPercent",
            percent: true,
            class: "numb",
        },
        {
            stat: "Total Thirds",
            prop: "totalThirdPlaceFinishes",
            class: "numb",
            includeAverage: true,
        },
        {
            stat: "Total Pot Net",
            prop: "potNet",
            currency: true,
            class: "numb",
        },
        {
            stat: "Overall Net",
            prop: "overallNet",
            currency: true,
            class: "numb",
        },
        {
            stat: "Best Streak",
            prop: "longestStreakLength",
            class: "numb",
            notes: `Asterisk denotes streak is active.`,
            includeAverage: true,
        },
        {
            stat: "Total Eliminations",
            prop: "totalEliminations",
            class: "numb",
            includeAverage: true,
        },
        {
            stat: "Most Eliminations in a Game",
            prop: "mostKos",
            class: "numb",
            // includeAverage: true,
        },
        {
            stat: "Total Rebuys",
            prop: "totalRebuys",
            class: "numb",
            // includeAverage: true,
        },
        {
            stat: "Total Games Without a Rebuy",
            prop: "totalGamesWithoutRebuy",
            class: "numb",
            // includeAverage: true,
        },
        {
            stat: "Total Wins Without a Rebuy",
            prop: "totalWinsWithoutRebuy",
            class: "numb",
            // includeAverage: true,
        },
        {
            stat: "Total Bounties Collected",
            prop: "bountiesWon",
            class: "numb",
            includeAverage: true,
        },
        {
            stat: "Total High Hands Won",
            prop: "highHandsWon",
            class: "numb",
            includeAverage: true,
        },
    ];

    private totItems = 2;
    private itemsLoaded = 0;

    constructor(
        private arrayService: ArrayService,
        private browserTitleService: BrowserTitleService,
        private dateService: DateService,
        private playerService: PlayerService
    ) {}

    ngOnInit() {
        this.browserTitleService.title = `Leaderboard`;
        this.setupYears();
    }

    private setupYears() {
        const now = new Date();
        let cur = now.getFullYear();

        while (cur >= this.firstYear) {
            this.years.push(cur--);
        }

        if (this.dateService.selectedYear) {
            this.year = this.dateService.selectedYear;
        } else {
            this.year = this.years[0];
        }

        this.onYearChanged();
    }

    onYearChanged() {
        this.dateService.selectedYear = this.year;
        this.loading = true;
        this.players.length = 0;
        this.averages = null;
        this.itemsLoaded = 0;

        this.playerService
            .readAllStats(this.year)
            .then((playersStats) => {
                this.players = playersStats;

                if (this.players && this.players.length > 0) {
                    this.totalGames = this.players[0].totalGamesForYear;
                    this.yearlyPot = this.totalGames * 20;
                }

                this.itemLoaded();
            })
            .catch((err) => {
                console.log(err);
            });

        this.playerService
            .readAverageStats(this.year)
            .then((avgs) => {
                this.averages = avgs;
                this.itemLoaded();
            })
            .catch((err) => {
                console.log(err);
            });
    }

    private itemLoaded() {
        ++this.itemsLoaded;

        if (this.itemsLoaded == this.totItems) {
            setTimeout(() => {
                this.loading = false;
            });
        }
    }

    download() {
        this.doingXlsx = true;

        const wb: XLSX.WorkBook = XLSX.utils.book_new();

        this.stats.forEach((stat) => {
            let data: (string | number)[][] = [];

            this.players.forEach((p) => {
                const theseStats: (string | number)[] = [];

                theseStats.push(`${p.player.first} ${p.player.last}`);
                theseStats.push(p[stat.prop]);

                data.push(theseStats);
            });

            data = data.sort((a, b) => {
                const pa = <string>a[0];
                const playera = pa.toLowerCase().replace(/ /g, "");
                const pb = <string>b[0];
                const playerb = pb.toLowerCase().replace(/ /g, "");
                const stata = <number>a[1];
                const statb = <number>b[1];

                if (stata == statb) {
                    if (playera < playerb) {
                        return -1;
                    } else if (playerb < playera) {
                        return 1;
                    }

                    return 0;
                } else {
                    return statb - stata;
                }
            });

            data.splice(0, 0, [
                `The Ravenhurst Leaderboard: ${stat.stat} as of ${new Date()}`,
            ]);

            const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(data);

            XLSX.utils.book_append_sheet(wb, ws, stat.stat);
        });

        const now = new Date();
        const year = now.getFullYear();

        const month = now.getMonth() + 1;
        const day = now.getDate();
        let hour = now.getHours();
        const min = now.getMinutes();

        const amPm = hour > 11 ? "pm" : "am";

        if (hour > 11) {
            hour -= 12;
        }

        if (hour == 0) {
            hour = 12;
        }

        const monthStr = month < 10 ? `0${month}` : String(month);
        const dayStr = day < 10 ? `0${day}` : String(day);
        const hourStr = hour < 10 ? `0${hour}` : String(hour);
        const minStr = min < 10 ? `0${min}` : String(min);

        const dateString = `${monthStr}-${dayStr}-${year} ${hourStr}-${minStr}-${amPm}`;
        const fileName = `TheRavenhurstLeaderboard-${dateString}`;

        XLSX.writeFile(wb, `${fileName}.xlsx`);

        this.doingXlsx = false;
    }
}
