import { NgModule } from '@angular/core'

import { CoreModule } from '../core'
import { LeaderboardComponent } from './leaderboard.component'
import { LeaderboardRouting } from './leaderboard.routing'
import { StatGroupComponent } from './stat-group/stat-group.component'
import { StatGraphComponent } from './stat-graph/stat-graph.component'

@NgModule({
    imports: [
        CoreModule,
        LeaderboardRouting
    ],
    declarations: [
        LeaderboardComponent,
        StatGroupComponent,
        StatGraphComponent
    ]
})
export class LeaderboardModule { }
