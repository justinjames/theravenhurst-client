import {
    Component,
    OnInit,
    OnDestroy,
    Input,
    ElementRef,
    AfterViewInit,
} from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import 'rxjs/add/operator/debounceTime';

import {
    CSVService,
    PlayerService,
    PlayerStats,
    PlayerStatsProp,
    GraphData,
} from '../../core';

@Component({
    selector: 'raven-stat-group',
    templateUrl: './stat-group.component.html',
    styleUrls: ['./stat-group.component.scss'],
})
export class StatGroupComponent implements AfterViewInit, OnDestroy {
    @Input() stat: PlayerStatsProp;
    @Input() data: PlayerStats[];
    @Input() averages: PlayerStats;

    myGraphData: GraphData[];
    graphDataGenerated = false;

    resize = 0;

    private weHaveResized: Subject<string> = new Subject<string>();
    private debounceResizeTime = 340;
    private $resizeSub: Subscription;
    private displayed = false;

    constructor(
        private el: ElementRef,
        private csv: CSVService,
        private playerService: PlayerService
    ) {}

    ngAfterViewInit() {
        this.checkPos();
        this.subscribeToResize();
    }

    ngOnDestroy() {
        this.subscribeToResize(true);
    }

    onScroll(e: Event) {
        this.checkPos();
    }

    onResize() {
        this.weHaveResized.next('a');
    }

    downloadStat() {
        this.csv.downloadStringAsCsv('test,whats,up,bro\rman,you,know,it');
    }

    private subscribeToResize(unsub?: boolean) {
        if (unsub) {
            this.$resizeSub.unsubscribe();
            return;
        }

        this.$resizeSub = this.weHaveResized
            .debounceTime(this.debounceResizeTime)
            .subscribe(m => {
                if (this.displayed) {
                    ++this.resize;
                } else {
                    this.checkPos();
                }
            });
    }

    private checkPos() {
        if (!this.displayed) {
            const top = this.el.nativeElement.offsetTop;
            const height = this.el.nativeElement.offsetHeight;
            const mid = top + height / 2;
            const winSY = window.scrollY;
            const winIH = window.innerHeight;
            const winBot = winSY + winIH;

            if (mid < winBot) {
                this.displayed = true;
                this.genGraphData();
            }
        }
    }

    private genGraphData() {
        const onlyPlayersWithStats = this.data.filter(pd => {
            return pd[this.stat.prop] != 0;
        });
        const curPlayerId = this.playerService.player._id;

        let sortedData = this.sortData(
            onlyPlayersWithStats,
            this.stat.prop,
            this.stat.string
        );

        if (this.stat.currency) {
            sortedData = sortedData.filter(d => d[this.stat.prop] > 0);
        }

        const curUserIndex = sortedData.findIndex(
            s => s.player._id == curPlayerId
        );
        const curUserStat =
            curUserIndex == -1 ? null : sortedData[curUserIndex];
        const includesCur = curUserIndex != -1;
        const curIsTopTen = includesCur ? curUserIndex < 10 : false;
        const sliceEndpoint = !includesCur || curIsTopTen ? 10 : 9;

        sortedData = sortedData.slice(0, sliceEndpoint);

        if (includesCur && !curIsTopTen) {
            sortedData.push(curUserStat);
        }

        this.myGraphData = sortedData.map((sd, y) => {
            let x = sd[this.stat.prop];
            let xDisp: number;
            let type = 'number';
            let name = `${sd.player.first} ${sd.player.last}`;

            if (
                this.stat.prop == 'longestStreakLength' &&
                sd.longestStreakActive
            ) {
                name += '*';
            }

            if (this.stat.percent) {
                x *= 100;
                xDisp = x.toFixed(2);
                type = 'percent';
            } else if (this.stat.currency) {
                type = 'currency';
            }

            return {
                x,
                xDisp,
                y,
                name,
                type,
            };
        });

        if (this.stat.includeAverage) {
            const avg: GraphData = {
                x: this.averages[this.stat.prop].toFixed(2),
                y: this.myGraphData.length,
                name: 'Average',
                type: this.myGraphData[0].type,
            };

            this.myGraphData.push(avg);

            this.myGraphData = this.myGraphData.sort((a, b) => {
                return b.x - a.x;
            });

            this.myGraphData.forEach((d, i) => {
                d.y = i;
            });
        }

        setTimeout(() => {
            this.graphDataGenerated = true;
        });
    }

    private sortData(
        arr: PlayerStats[],
        prop: string,
        isString: boolean
    ): PlayerStats[] {
        let switchedIs = false;
        let ret: PlayerStats[] = [];

        ret = arr.sort((a, b) => {
            let av: string | number = a[prop];
            let bv: string | number = b[prop];

            if (!isString && av == bv) {
                av = a.player.first;
                bv = b.player.first;
                isString = true;
                switchedIs = true;
            }

            if (isString) {
                if (switchedIs) {
                    switchedIs = false;
                    isString = false;
                }

                if (av < bv) {
                    return -1;
                } else if (av > bv) {
                    return 1;
                }

                return 0;
            } else {
                return <number>bv - <number>av;
            }
        });

        return ret;
    }
}
