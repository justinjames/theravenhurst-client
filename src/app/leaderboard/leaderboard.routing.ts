import { RouterModule } from '@angular/router'

import { AuthenticatedGuard } from '../core'
import { LeaderboardComponent } from './leaderboard.component'

export const LeaderboardRouting = RouterModule.forChild([
    { path: 'leaderboard', component: LeaderboardComponent, canActivate: [ AuthenticatedGuard ] }
])