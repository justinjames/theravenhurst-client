import { NgModule } from '@angular/core'

import { CoreModule } from '../core'

import { LoginComponent } from './login.component'
import { LoginRouting } from './login.routing'

@NgModule({
    imports: [
        CoreModule,
        LoginRouting
    ],
    declarations: [
        LoginComponent
    ]
})
export class LoginModule {}