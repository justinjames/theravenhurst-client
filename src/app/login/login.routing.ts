import { RouterModule } from '@angular/router'

import { LoginComponent } from './login.component'
import { NotAuthenticatedGuard } from '../core'

export const LoginRouting = RouterModule.forChild([
    { path: 'login', component: LoginComponent, canActivate: [ NotAuthenticatedGuard ] }
])