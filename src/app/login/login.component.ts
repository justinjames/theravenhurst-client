import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Params } from '@angular/router'

// import { BrowserTitleService } from '../core/services/browser-title.service'
import { BrowserTitleService, ButtonTexts, FormInput, NavigationService, Player, PlayerService } from '../core'

@Component({
    selector: 'raven-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    private buttonTexts : ButtonTexts = {
        initial: "Login",
        processing: 'Processing...'
    }

    items : FormInput[] = [
        new FormInput({
            key: 'email',
            label: 'Email',
            value: '',
            required: true,
            type: 'email'
        }),
        new FormInput({
            key: 'password',
            label: 'Password',
            value: '',
            required: true,
            type: 'password'
        })
    ]
    form : FormGroup = null
    formSubmitting : boolean = false
    noPassword : boolean = false
    buttonText : string = this.buttonTexts.initial
    errorMessage : string = ""

    constructor(
        private browserTitleService : BrowserTitleService,
        private navigationService : NavigationService,
        private playerService : PlayerService
    ) {}

    ngOnInit() {
        this.browserTitleService.title = 'Login'
        this.form = this.toFormGroup(this.items)
    }

    toFormGroup(items: FormInput[] ) {

        let group: any = {}

        items.forEach(input => {
            group[input.key] = input.required ? new FormControl(input.value || '', Validators.required) : new FormControl(input.value || '')
        })

        return new FormGroup(group)
    }

    onSubmit() {

        let payload : any = JSON.stringify(this.form.value)

        this.formSubmitting = true
        this.buttonText = this.buttonTexts.processing
        this.errorMessage = ""

        this.playerService.login(payload)
            .then(res => {
                if(res && res.success) {

                    let navTo : string[] = []
                    let qp : Params = {}

                    if(this.navigationService.loginRoute) {
                        this.navigationService.loginRoute.url.forEach(u => {
                            navTo.push(<string>u.path)
                        })
                        qp = this.navigationService.loginRoute.queryParams
                    }

                    if(!navTo.length) {
                        navTo = ['']
                    }

                    this.playerService.player = res.player as Player
                    this.navigationService.goto(navTo, qp)
                    this.navigationService.loginRoute = null

                }else{
                    this.formSubmitting = false
                    this.buttonText = this.buttonTexts.initial
                    this.errorMessage = res.info
                }
            })
            .catch(err => {
                this.formSubmitting = false
                this.buttonText = this.buttonTexts.initial
                this.errorMessage = `${err}`
            })
    }

}