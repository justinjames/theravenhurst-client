import { Routes, RouterModule } from '@angular/router'

import { NotFoundComponent } from './not-found'

export const routes: Routes = [
    { path: '**', component: NotFoundComponent }
]
export const AppRoutes = RouterModule.forRoot(routes)