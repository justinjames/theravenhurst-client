import { InjectionToken } from '@angular/core'
import { AppConfigInterface } from './app.config.interface'

export let APP_CONFIG = new InjectionToken('app.config')
export const AppConfig: AppConfigInterface = {
    apiEndpoint : '/api',
}
