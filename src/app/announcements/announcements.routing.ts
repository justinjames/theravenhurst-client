import { RouterModule } from '@angular/router'

import { AdminGuard, AuthenticatedGuard } from '../core'
import { AnnouncementsComponent } from './announcements.component'
import { NewAnnouncementComponent } from './new-announcement/new-announcement.component'
import { AnnouncementComponent } from './announcement/announcement.component'
import { EditAnnouncementComponent } from './edit-announcement/edit-announcement.component'

export const AnnouncementsRouting = RouterModule.forChild([
    { path: 'announcements', component: AnnouncementsComponent, canActivate: [ AuthenticatedGuard ] },
    { path: 'announcements/new', component: NewAnnouncementComponent, canActivate: [ AuthenticatedGuard, AdminGuard ] },
    { path: 'announcements/:announcementId', component: AnnouncementComponent, canActivate: [ AuthenticatedGuard ] },
    { path: 'announcements/:announcementId/edit', component: EditAnnouncementComponent, canActivate: [ AuthenticatedGuard, AdminGuard ] }
])