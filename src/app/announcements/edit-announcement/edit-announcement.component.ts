import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { FormControl, FormGroup, Validators } from '@angular/forms'

import { BrowserTitleService } from '../../core'

@Component({
    selector: 'raven-edit-announcement',
    templateUrl: './edit-announcement.component.html',
    styleUrls: ['./edit-announcement.component.scss']
})
export class EditAnnouncementComponent implements OnInit {

    // private buttonTexts : ButtonTexts = {
    //     initial: 'Save',
    //     processing: 'Processing...'
    // }

    // private buttonTexts2 : ButtonTexts = {
    //     initial: 'Update Password',
    //     processing: 'Processing...'
    // }

    // private originalAnnouncement : Announcement = {}

    // private props : string[] = [
    //     'first',
    //     'last',
    //     'email',
    //     'nick',
    //     'say',
    //     'phone'
    // ]

    // items : FormInput[] = [
    //     new FormInput({
    //         key: 'first',
    //         label: 'First name',
    //         value: '',
    //         required: true,
    //         type: 'text'
    //     }),
    //     new FormInput({
    //         key: 'last',
    //         label: 'Last name',
    //         required: true,
    //         type: 'text'
    //     }),
    //     new FormInput({
    //         key: 'nick',
    //         label: 'Nickname (optional)',
    //         type: 'text'
    //     }),
    //     new FormInput({
    //         key: 'say',
    //         label: 'Saying (optional)',
    //         type: 'text'
    //     }),
    //     new FormInput({
    //         key: 'phone',
    //         label: 'Phone number (optional)',
    //         type: 'text'
    //     }),
    //     new FormInput({
    //         key: 'email',
    //         label: 'Email (optional)',
    //         required: false,
    //         type: 'email'
    //     }),
    //     new FormInput({
    //         key: 'password',
    //         label: 'Password (optional)',
    //         required: false,
    //         type: 'text'
    //     })
    // ]
    // items2 : FormInput[] = [
    //     new FormInput({
    //         key: 'password',
    //         label: 'New password',
    //         required: true,
    //         type: 'text'
    //     })
    // ]
    // form : FormGroup = null
    // form2 : FormGroup = null
    // formSubmitting : boolean = false
    // buttonText : string = this.buttonTexts.initial
    // errorMessage : string = ""
    // formSubmitting2 : boolean = false
    // buttonText2 : string = this.buttonTexts2.initial
    // errorMessage2 : string = ""
    // announcementId : string
    // hasPass = false
    // matchesOriginal = true

    constructor(
        private activatedRoute : ActivatedRoute,
        private browserTitleService : BrowserTitleService,
        // private navigationService : NavigationService,
        // private announcementService : AnnouncementService
    ) {}

    ngOnInit() {
        this.browserTitleService.title = 'Edit Announcement'
        // this.subscribeToRoute()
    }

    // toFormGroup(items: FormInput[] ) {

    //     let group: any = {}

    //     items.forEach(input => {
    //         group[input.key] = input.required ? new FormControl(input.value || '', Validators.required) : new FormControl(input.value || '')
    //     })

    //     return new FormGroup(group)
    // }

    // private subscribeToRoute() {
    //     this.activatedRoute.params.subscribe(params => {
    //         this.announcementId = params['announcementId']
    //         this.loadMyAnnouncement()
    //     })
    // }

    // private loadMyAnnouncement() {
    //     this.announcementService.getAnnouncement(this.announcementId)
    //         .then(response => {

    //             this.setOriginalAnnouncement(response.announcement)
    //             this.hasPass = response.hasPass

    //             if(this.hasPass) {

    //                 const last = this.items.length - 1

    //                 this.items = this.items.slice(0, last)
    //                 this.form2 = this.toFormGroup(this.items2)
    //             }

    //             this.form = this.toFormGroup(this.items)
    //             this.form.patchValue(response.announcement)
    //         })
    //         .catch(err => {
    //             console.log('err', err)
    //         })
    // }

    // private setOriginalAnnouncement(pl : Announcement) {
    //     this.props.forEach(p => {
    //         this.originalAnnouncement[p] = pl[p]
    //     })
    // }

    // formValueChanged() {

    //     let matches = true

    //     this.props.forEach(p => {
    //         if(this.form.value[p] != this.originalAnnouncement[p]) {
    //             matches = false
    //         }
    //     })

    //     this.matchesOriginal = matches

    // }

    // onSubmit() {

    //     let payloadAnnouncement : Announcement = {}

    //     for(const p in this.originalAnnouncement) {
    //         if(this.form.value[p] != this.originalAnnouncement[p]) {
    //             payloadAnnouncement[p] = this.form.value[p]
    //         }
    //     }

    //     const payload : any = JSON.stringify(payloadAnnouncement)

    //     this.formSubmitting = true
    //     this.buttonText = this.buttonTexts.processing
    //     this.errorMessage = ''

    //     this.announcementService.updateAnnouncement(this.announcementId, payload)
    //         .then(response => {
    //             this.formSubmitting = false
    //             this.buttonText = this.buttonTexts.initial
    //             this.setOriginalAnnouncement(response)
    //             this.matchesOriginal = true
    //         })
    //         .catch(error => {
    //             this.formSubmitting = false
    //             this.buttonText = this.buttonTexts.initial
    //             this.errorMessage = `There was an error creating new announcement: ${error}`
    //         })
    // }

    // onSubmitPassword() {

    //     const payload : any = JSON.stringify(this.form2.value)

    //     this.formSubmitting2 = true
    //     this.buttonText2 = this.buttonTexts2.processing
    //     this.errorMessage2 = ''

    //     this.announcementService.udpdatePassword(this.announcementId, payload)
    //     .then(response => {
    //         this.formSubmitting2 = false
    //         this.buttonText2 = this.buttonTexts2.initial
    //         this.form2.patchValue({password: ''})
    //     })
    //     .catch(error => {
    //         this.formSubmitting2 = false
    //         this.buttonText2 = this.buttonTexts2.initial
    //         this.errorMessage2 = `There was an error creating new announcement: ${error}`
    //     })
    // }
}