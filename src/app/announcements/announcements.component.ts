import { Component, OnInit } from '@angular/core'

import { Announcement, AnnouncementService, BrowserTitleService, PlayerService } from '../core'

@Component({
    selector: 'raven-announcements',
    templateUrl: './announcements.component.html',
    styleUrls: ['./announcements.component.scss']
})
export class AnnouncementsComponent implements OnInit {

    announcements : Announcement[]
    admin = false

    constructor(
        private announcementService : AnnouncementService,
        private browserTitleService : BrowserTitleService,
        private playerService : PlayerService
    ) {}

    ngOnInit() {
        this.browserTitleService.title = 'Announcements'
        this.admin = this.playerService.player.admin
        this.subscribeToAnnouncements()
    }

    private subscribeToAnnouncements() {
        this.announcementService.announcementsSubject.subscribe(anns => {
            this.announcements = anns
        })
    }
}