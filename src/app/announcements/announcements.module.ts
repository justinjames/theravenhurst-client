import { NgModule } from '@angular/core'

import { CoreModule } from '../core'
import { AnnouncementsComponent } from './announcements.component'
import { AnnouncementsRouting } from './announcements.routing'
import { NewAnnouncementComponent } from './new-announcement/new-announcement.component'
import { AnnouncementComponent } from './announcement/announcement.component'
import { EditAnnouncementComponent } from './edit-announcement/edit-announcement.component'

@NgModule({
    imports: [
        CoreModule,
        AnnouncementsRouting
    ],
    declarations: [
        AnnouncementsComponent,
        NewAnnouncementComponent,
        AnnouncementComponent,
        EditAnnouncementComponent
    ]
})
export class AnnouncementsModule { }