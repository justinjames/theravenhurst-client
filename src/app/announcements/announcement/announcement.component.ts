import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { BrowserTitleService, Announcement, AnnouncementService } from '../../core'

@Component({
    selector: 'raven-announcement',
    templateUrl: './announcement.component.html',
    styleUrls: ['./announcement.component.scss']
})
export class AnnouncementComponent implements OnInit {

    // announcementId = ''
    // announcement : Announcement = null
    // admin = false
    // extraName : string
    // noAnnouncement = false
    // phoneNumber : string

    constructor(
        // private activatedRoute : ActivatedRoute,
        private browserTitleService : BrowserTitleService,
        // private announcementService : AnnouncementService
    ) { }

    ngOnInit() {
        this.browserTitleService.title = 'Announcement'
        // this.admin = this.announcementService.announcement.admin
        // this.subscribeToRoute()
    }

    private subscribeToRoute() {
        // this.activatedRoute.params.subscribe(params => {
        //     this.announcementId = params['announcementId']
        //     this.loadMyAnnouncement()
        // })
    }

    private loadMyAnnouncement() {
        // this.announcementService.getAnnouncement(this.announcementId)
        //     .then(response => {
        //         this.announcement = response.announcement
        //         this.browserTitleService.title = this.announcement.first + ' ' + this.announcement.last
        //         this.setExtraName()
        //         this.setPhoneNumber()
        //     })
        //     .catch(err => {
        //         console.log('err >> ', err)
        //         this.noAnnouncement = true
        //     })
    }

    private setExtraName() {
        // this.extraName = this.announcementService.getExtraName(this.announcement)
    }

    private setPhoneNumber() {
        // if(this.announcement.phone) {
        //     this.phoneNumber = this.announcement.phone.slice(0, 3) + '.' + this.announcement.phone.slice(3, 6) + '.' + this.announcement.phone.slice(6, 10)
        // }
    }

}