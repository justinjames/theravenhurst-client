import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'

import { BrowserTitleService, Announcement, AnnouncementService, NavigationService } from '../../core'

@Component({
    selector: 'raven-new-announcement',
    templateUrl: './new-announcement.component.html',
    styleUrls: ['./new-announcement.component.scss']
})
export class NewAnnouncementComponent implements OnInit {

    newAnnouncementText = ''
    submitting = false

    constructor(
        private browserTitleService : BrowserTitleService,
        private announcementService : AnnouncementService,
        private navigationService : NavigationService
    ) {}

    ngOnInit() {
        this.browserTitleService.title = 'New Announcement'
        // this.form = this.toFormGroup(this.items)
    }

    addNewAnnouncement() {

        this.submitting = true

        this.announcementService.createNewAnnouncement(this.newAnnouncementText)
            .then(response => {
                this.submitting = false
                this.navigationService.goto('')
            })
            .catch(error => {
                this.submitting = false
            })
    }

}
