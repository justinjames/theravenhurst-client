import { RouterModule } from '@angular/router'

import { AuthenticatedGuard } from '../core'
import { HomeComponent } from './home.component'

export const HomeRouting = RouterModule.forChild([
    { path: '', component: HomeComponent, canActivate: [ AuthenticatedGuard ] }
])