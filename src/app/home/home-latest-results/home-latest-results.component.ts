import { Component, OnInit } from '@angular/core';

import {
    Entry,
    Game,
    GameService,
    Hand,
    HandService,
    LatestResults,
    Player,
    SortableHand,
} from '../../core';

@Component({
    selector: 'raven-home-latest-results',
    templateUrl: './home-latest-results.component.html',
    styleUrls: ['./home-latest-results.component.scss'],
})
export class HomeLatestResultsComponent implements OnInit {
    latestResults: LatestResults;
    game: Game;
    displayDate: Date;
    // highHand : SortableHand = null
    highHands: SortableHand[] = null;
    entries: Entry[];
    winningEntries: Entry[];
    higHandPot: number;

    constructor(private gameService: GameService, private handService: HandService) {}

    ngOnInit() {
        this.subscribeToLatest();
    }

    private subscribeToLatest() {
        this.gameService.latestResultsSubject.subscribe(lr => {
            if (lr && lr.game) {
                // console.log(lr);

                this.entries = lr.entries;
                this.winningEntries = this.entries.filter(e => e.pt);

                this.highHands = [];

                if (lr.highHands && lr.highHands.length) {
                    lr.highHands.forEach(highHand => {
                        const hh = Object.assign(highHand, {});

                        const _highHand: SortableHand = {};
                        _highHand.hand = hh.h.split(',');
                        _highHand.type = this.handService.determineHandType(_highHand.hand);
                        _highHand.display = this.handService.getHandDisplay(_highHand);

                        _highHand.g = Object.assign(lr.game, {});
                        _highHand.p = highHand.p;

                        this.highHands.push(_highHand);

                        this.higHandPot = 0;

                        this.entries.forEach(e => {
                            if (e.hh == 1) {
                                this.higHandPot += 5;
                            }
                        });
                    });
                }

                this.game = lr.game;
                this.displayDate = new Date(this.game.y, this.game.m, this.game.d);
            }
        });
    }
}
