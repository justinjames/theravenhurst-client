import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeLatestResultsComponent } from './home-latest-results.component';

describe('HomeLatestResultsComponent', () => {
  let component: HomeLatestResultsComponent;
  let fixture: ComponentFixture<HomeLatestResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeLatestResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeLatestResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
