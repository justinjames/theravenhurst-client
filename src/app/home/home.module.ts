import { NgModule } from '@angular/core';

import { CoreModule } from '../core';
import { HomeAnnouncementComponent } from './home-announcement/home-announcement.component';
// import { HomeBigWinnerComponent } from './home-latest-results/home-big-winner/home-big-winner.component'
import { HomeLatestResultsComponent } from './home-latest-results/home-latest-results.component';
import { HomeComponent } from './home.component';
import { HomeRouting } from './home.routing';

@NgModule({
    imports: [CoreModule, HomeRouting],
    declarations: [
        HomeAnnouncementComponent,
        // HomeBigWinnerComponent,
        HomeLatestResultsComponent,
        HomeComponent,
    ],
})
export class HomeModule {}
