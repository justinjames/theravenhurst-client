import { Component, OnInit } from "@angular/core";

import { Announcement, AnnouncementService } from "../../core";

@Component({
    selector: "raven-home-announcement",
    templateUrl: "./home-announcement.component.html",
    styleUrls: ["./home-announcement.component.scss"]
})
export class HomeAnnouncementComponent implements OnInit {
    announcement: Announcement;
    private cutOffWeeks = 2;

    constructor(private announcementService: AnnouncementService) {}

    ngOnInit() {
        this.subscribeToAnnouncements();
    }

    private subscribeToAnnouncements() {
        const cutOffAsMiliseconds = 1000 * 60 * 60 * 24 * 7 * this.cutOffWeeks;
        const now = new Date();

        this.announcementService.announcementsSubject.subscribe(
            announcements => {
                const mostRecent =
                    announcements && announcements.length
                        ? announcements[0]
                        : null;

                if (mostRecent) {
                    const mrd = new Date(mostRecent.date);
                    const diff = Math.abs(mrd.getTime() - now.getTime());
                    const tooOld = diff >= cutOffAsMiliseconds;

                    if (!tooOld) {
                        this.announcement = mostRecent;
                    } else {
                        this.announcement = null;
                    }
                }
            }
        );
    }
}
