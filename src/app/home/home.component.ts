import { Component, OnInit } from '@angular/core'

import { BrowserTitleService } from '../core'

@Component({
    selector: 'raven-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    constructor(
        private browserTitleService : BrowserTitleService,
        // private navigationService : NavigationService,
        // private playerService : PlayerService
    ) {}

    ngOnInit() {
        this.browserTitleService.title = 'Home'
    }
}