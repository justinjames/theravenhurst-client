import { RouterModule } from '@angular/router'

import { AuthenticatedGuard } from '../core'
import { SmackComponent } from './smack.component'

export const SmackRouting = RouterModule.forChild([
    { path: 'smack', component: SmackComponent, canActivate: [ AuthenticatedGuard ] }
])