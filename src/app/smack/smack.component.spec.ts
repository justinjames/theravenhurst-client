import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmackComponent } from './smack.component';

describe('SmackComponent', () => {
  let component: SmackComponent;
  let fixture: ComponentFixture<SmackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
