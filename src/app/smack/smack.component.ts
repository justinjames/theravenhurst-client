import { Component, OnInit } from '@angular/core';

import {
    BrowserTitleService,
    Message,
    MessageService,
    Player,
    PlayerService,
    SocketService,
} from '../core';

@Component({
    selector: 'raven-smack',
    templateUrl: './smack.component.html',
    styleUrls: ['./smack.component.scss'],
})
export class SmackComponent implements OnInit {
    private allMessages: Message[] = [];
    displayedMessages: Message[] = [];
    private numbMessages = 20;
    loading = true;
    sendingMessage = false;
    initialScrollDone = false;
    newMessage = '';
    filesToUpload: Array<File> = [];
    imageToUpload: File;
    uploadedImagePath: string;

    private players: Player[] = [];

    constructor(
        private browserTitleService: BrowserTitleService,
        private messageService: MessageService,
        private playerService: PlayerService,
        private socketService: SocketService
    ) {}

    ngOnInit() {
        this.browserTitleService.title = 'Smack';
        this.subscribeToPlayers();
    }

    private subscribeToPlayers() {
        this.playerService.playersSubject.subscribe(ps => {
            if (ps.length > 0) {
                this.subscribeToStoredMessages();
                this.subscribeToNewMessage();
                this.subscribeToDeletedMessages();
                this.loading = false;
            }
        });
    }

    private subscribeToStoredMessages() {
        this.messageService.storedMessagesSubject.subscribe((ms: Message[]) => {
            if (ms.length > 0) {
                ms = ms.filter(m => {
                    return this.allMessages.findIndex(am => am.id == m.id) == -1;
                });

                ms.forEach(m => {
                    m.player = this.playerService.getPlayerFromId(m.user);
                });

                this.allMessages = this.allMessages.concat(ms);

                this.scrollToBottom();
                this.sortMessages();
            }
        });
    }

    private subscribeToNewMessage() {
        this.messageService.liveMessageSubject.subscribe((m: Message) => {
            const idx = this.allMessages.findIndex(am => am.id == m.id);

            if (idx == -1) {
                m.player = this.playerService.getPlayerFromId(m.user);

                this.allMessages.push(m);
                // this.displayedMessages.push(m)
                this.scrollToBottom();
                this.sortMessages();
            }
        });
    }

    private subscribeToDeletedMessages() {
        this.messageService.deletedMessageSubject.subscribe((mes: Message) => {
            const idx = this.allMessages.findIndex(m => m.id == mes.id);
            // const idx2 = this.displayedMessages.findIndex(m => m.id == mes.id)

            if (idx != -1) {
                this.allMessages.splice(idx, 1);
            }

            // if(idx2 != -1) {
            //     this.displayedMessages.splice(idx2, 1)
            // }
        });
    }

    private sortMessages() {
        this.allMessages.sort((a, b) => {
            const ad = <Date>a.date;
            const bd = <Date>b.date;
            return ad.getTime() - bd.getTime();
        });

        this.displayedMessages = this.allMessages.slice(
            this.allMessages.length - this.numbMessages,
            this.allMessages.length
        );

        // console.log(this.allMessages);
        // console.log(this.displayedMessages);
    }

    private scrollToBottom() {
        setTimeout(() => {
            const allMessagesDiv = document.getElementById('displayedMessages');

            // console.log('allMessagesDiv', allMessagesDiv);

            if (allMessagesDiv) {
                allMessagesDiv.scrollTop = allMessagesDiv.scrollHeight;

                if (!this.initialScrollDone) {
                    setTimeout(() => {
                        this.initialScrollDone = true;
                    }, 10);
                }
            }
        });
    }

    fileChangeEvent(fileInput: any) {
        this.filesToUpload = <Array<File>>fileInput.target.files;

        if (this.filesToUpload[0].type.indexOf('image') != -1) {
            this.imageToUpload = this.filesToUpload[0];
        }
    }

    sendHit() {
        if (this.imageToUpload) {
            this.uploadImage();
        } else {
            this.addNewMessage();
        }
    }

    cancelImage() {
        this.imageToUpload = null;
        this.filesToUpload = [];
    }

    private uploadImage() {
        this.sendingMessage = true;

        this.messageService
            .uploadImage(this.imageToUpload)
            .then(response => {
                this.uploadedImagePath = response;
                this.imageToUpload = null;
                this.addNewMessage();
            })
            .catch(err => {
                console.log(err);
            });
    }

    private addNewMessage() {
        this.sendingMessage = true;

        this.messageService
            .createNewMessage(this.newMessage, this.uploadedImagePath)
            .then(response => {
                this.sendingMessage = false;
                this.newMessage = '';
                this.uploadedImagePath = null;
            })
            .catch(err => {
                console.log(err);
            });
    }
}
