import { Component, OnInit, Input } from '@angular/core'

import { DateService, Message, MessageService, PlayerService, StringService } from '../../core'

@Component({
    selector: 'raven-smack-message',
    templateUrl: './smack-message.component.html',
    styleUrls: ['./smack-message.component.scss']
})
export class SmackMessageComponent implements OnInit {

    @Input() message : Message

    today = false
    myMessage = false
    deleting = false

    constructor(
        private dateService : DateService,
        private messageService : MessageService,
        private playerService : PlayerService,
        private stringService : StringService
    ) {}

    ngOnInit() {

        this.today = this.dateService.isToday(this.message.date)
        this.myMessage = this.playerService.player._id == this.message.player._id

        if(!this.message.adjusted) {
            this.message.text = this.stringService.replaceUrl(this.message.text)
            this.message.adjusted = true
        }
    }

    deleteMe() {
        this.deleting = true
        this.messageService.deleteMessage(this.message.file)
    }
}