import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmackMessageComponent } from './smack-message.component';

describe('SmackMessageComponent', () => {
  let component: SmackMessageComponent;
  let fixture: ComponentFixture<SmackMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmackMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmackMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
