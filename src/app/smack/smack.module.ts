import { NgModule } from '@angular/core'

import { CoreModule } from '../core'
import { SmackMessageComponent } from './smack-message/smack-message.component'
import { SmackComponent } from './smack.component'
import { SmackRouting } from './smack.routing'

@NgModule({
    imports: [
        CoreModule,
        SmackRouting
    ],
    declarations: [
        SmackMessageComponent,
        SmackComponent
    ]
})
export class SmackModule { }
