import { NgModule } from '@angular/core'

import { CoreModule } from '../core'
import { ProfileComponent } from './profile.component'
import { ProfileRouting } from './profile.routing'

@NgModule({
    imports: [
        CoreModule,
        ProfileRouting
    ],
    declarations: [
        ProfileComponent
    ]
})
export class ProfileModule {}