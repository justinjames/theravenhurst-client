import { RouterModule } from '@angular/router'

import { AuthenticatedGuard } from '../core'
import { ProfileComponent } from './profile.component'

export const ProfileRouting = RouterModule.forChild([
    { path: 'profile', component: ProfileComponent, canActivate: [ AuthenticatedGuard ] }
])