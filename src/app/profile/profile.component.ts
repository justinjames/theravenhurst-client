import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { RequestOptions } from '@angular/http'

import { BrowserTitleService, ButtonTexts, FormInput, Player, PlayerService } from '../core'

@Component({
    selector: 'raven-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    player : Player
    customPic = false

    private buttonTexts : ButtonTexts = {
        initial: 'Save',
        processing: 'Processing...'
    }

    private buttonTexts2 : ButtonTexts = {
        initial: 'Update Password',
        processing: 'Processing...'
    }

    private originalPlayer : Player = {}

    private props : string[] = [
        'first',
        'last',
        'email',
        'nick',
        'say',
        'phone'
    ]

    items : FormInput[] = [
        new FormInput({
            key: 'first',
            label: 'First name',
            value: '',
            required: true,
            type: 'text'
        }),
        new FormInput({
            key: 'last',
            label: 'Last name',
            required: true,
            type: 'text'
        }),
        new FormInput({
            key: 'nick',
            label: 'Nickname (optional)',
            type: 'text'
        }),
        new FormInput({
            key: 'say',
            label: 'Saying (optional)',
            type: 'text'
        }),
        new FormInput({
            key: 'phone',
            label: 'Phone number (optional)',
            type: 'text'
        }),
        new FormInput({
            key: 'email',
            label: 'Email (optional)',
            required: false,
            type: 'email'
        })
    ]
    items2 : FormInput[] = [
        new FormInput({
            key: 'password',
            label: 'New password',
            required: true,
            type: 'text'
        })
    ]
    form : FormGroup = null
    form2 : FormGroup = null
    formSubmitting = false
    buttonText : string = this.buttonTexts.initial
    errorMessage = ''
    formSubmitting2 : boolean = false
    buttonText2 : string = this.buttonTexts2.initial
    errorMessage2 = ''
    matchesOriginal = true

    uploadingImage = false
    filesToUpload : Array<File> = []

    constructor(
        private browserTitleService : BrowserTitleService,
        // private navigationService : NavigationService,
        private playerService : PlayerService
    ) {}

    ngOnInit() {
        this.browserTitleService.title = 'Profile'
        this.loadMe()
    }

    toFormGroup(items: FormInput[] ) {

        let group : any = {}

        items.forEach(input => {
            group[input.key] = input.required ? new FormControl(input.value || '', Validators.required) : new FormControl(input.value || '')
        })

        return new FormGroup(group)
    }

    private loadMe() {
        this.playerService.getPlayer(this.playerService.player._id)
            .then(response => {

                this.setOriginalPlayer(response.player)
                this.player = response.player
                this.form = this.toFormGroup(this.items)
                this.form2 = this.toFormGroup(this.items2)
                this.form.patchValue(response.player)
                this.customPic = this.player.afile.search('/donkeys') == -1

            })
            .catch(err => {
                console.log(err)
            })
    }

    private setOriginalPlayer(pl : Player) {
        this.props.forEach(p => {
            this.originalPlayer[p] = pl[p]
        })
    }

    formValueChanged() {

        let matches = true

        this.props.forEach(p => {
            if(this.form.value[p] != this.originalPlayer[p]) {
                matches = false
            }
        })

        this.matchesOriginal = matches

    }

    onSubmit() {

        let payloadPlayer : Player = {}

        for(const p in this.originalPlayer) {
            if(this.form.value[p] != this.originalPlayer[p]) {
                payloadPlayer[p] = this.form.value[p]
            }
        }

        if(payloadPlayer.email == '' ) {
            delete payloadPlayer.email
        }

        const payload : any = JSON.stringify(payloadPlayer)

        this.formSubmitting = true
        this.buttonText = this.buttonTexts.processing
        this.errorMessage = ''

        this.playerService.updatePlayer(this.playerService.player._id, payload)
            .then(response => {
                this.formSubmitting = false
                this.buttonText = this.buttonTexts.initial
                this.setOriginalPlayer(response)
                this.matchesOriginal = true
            })
            .catch(error => {
                this.formSubmitting = false
                this.buttonText = this.buttonTexts.initial
                this.errorMessage = `There was an error creating new player: ${error}`
            })
    }

    onSubmitPassword() {

        const payload : any = JSON.stringify(this.form2.value)

        this.formSubmitting2 = true
        this.buttonText2 = this.buttonTexts2.processing
        this.errorMessage2 = ''

        this.playerService.udpdatePassword(this.playerService.player._id, payload)
        .then(response => {
            this.formSubmitting2 = false
            this.buttonText2 = this.buttonTexts2.initial
            this.form2.patchValue({password: ''})
        })
        .catch(error => {
            this.formSubmitting2 = false
            this.buttonText2 = this.buttonTexts2.initial
            this.errorMessage2 = `There was an error creating new player: ${error}`
        })
    }

    newDonkey() {
        this.playerService.newDonkey(this.playerService.player._id)
            .then(response => {
                this.player.afile = response.afile + '?rand=' + Math.random()
            })
            .catch(err => {
                console.log(err)
            })
    }

    fileChangeEvent(fileInput : any) {
        this.filesToUpload = <Array<File>>fileInput.target.files
        if(this.filesToUpload[0].type.indexOf('image') != -1) {
            this.onReceiveImage(this.filesToUpload[0])
        }
    }

    onReceiveImage(f : File) {

        this.uploadingImage = true

        this.playerService.uploadImage(this.playerService.player._id, f)
            .then(response => {
                this.uploadingImage = false
                this.player.afile = response.afile + '?rand=' + Math.random()
            })
            .catch(err => {
                console.log(err)
            })
    }
}