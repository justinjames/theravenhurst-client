import { RouterModule } from '@angular/router'

import { AuthenticatedGuard } from '../core'

import { YearInReviewComponent } from './year-in-review.component';

export const YearInReviewRouting = RouterModule.forChild([
    { path: 'year-in-review', component: YearInReviewComponent, canActivate: [ AuthenticatedGuard ] },
])