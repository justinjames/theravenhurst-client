import { Component, OnInit } from '@angular/core';

import { BrowserTitleService, DateService, GameService, YearInReviewGame } from '../core';

@Component({
    selector: 'raven-year-in-review',
    templateUrl: './year-in-review.component.html',
    styleUrls: ['./year-in-review.component.scss'],
})
export class YearInReviewComponent implements OnInit {
    reviews: YearInReviewGame[] = [];

    years: number[] = [];
    year: number;
    firstYear = 2017;
    loading = true;

    constructor(
        private browserTitleService: BrowserTitleService,
        private dateService: DateService,
        private gameService: GameService,
    ) {}

    ngOnInit() {
        this.browserTitleService.title = 'Games';
        this.setupYears();
    }

    private setupYears() {
        const now = new Date();
        let cur = now.getFullYear();

        while (cur >= this.firstYear) {
            this.years.push(cur--);
        }

        if (this.dateService.selectedYear) {
            this.year = this.dateService.selectedYear;
        } else {
            this.year = this.years[0];
        }

        this.onYearChanged();
    }

    onYearChanged() {
        this.dateService.selectedYear = this.year;
        this.loadReview();
    }

    private loadReview() {
        this.loading = true;
        this.reviews.length = 0;

        this.gameService
            .loadYearReview(this.year)
            .then(rev => {
                this.reviews = rev;
                this.loading = false;
            })
            .catch(err => {
                console.log(err);
            });
    }
}
