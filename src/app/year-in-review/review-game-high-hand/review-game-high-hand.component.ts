import { Component, OnInit, Input } from '@angular/core';

import { Hand, SortableHand, HandService } from '../../core';

@Component({
    selector: 'raven-review-game-high-hand',
    templateUrl: './review-game-high-hand.component.html',
    styleUrls: ['./review-game-high-hand.component.scss'],
})
export class ReviewGameHighHandComponent implements OnInit {
    @Input()
    hand: Hand;

    highHand: SortableHand = {};

    constructor(private handService: HandService) {}

    ngOnInit() {
        this.highHand.hand = this.hand.h.split(',');
        this.highHand.type = this.handService.determineHandType(this.highHand.hand);
        this.highHand.display = this.handService.getHandDisplay(this.highHand);

        this.highHand.g = Object.assign(this.hand.g, {});
        this.highHand.p = this.hand.p;
    }
}
