import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewGameHighHandComponent } from './review-game-high-hand.component';

describe('ReviewGameHighHandComponent', () => {
  let component: ReviewGameHighHandComponent;
  let fixture: ComponentFixture<ReviewGameHighHandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewGameHighHandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewGameHighHandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
