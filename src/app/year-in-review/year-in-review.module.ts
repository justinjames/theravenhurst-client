import { NgModule } from '@angular/core'

import { CoreModule } from '../core'
import { YearInReviewComponent } from './year-in-review.component';
import { YearInReviewRouting } from './year-in-review.routing';
import { ReviewGameHighHandComponent } from './review-game-high-hand/review-game-high-hand.component'

@NgModule({
    imports: [
        CoreModule,
        YearInReviewRouting
    ],
    declarations: [
        YearInReviewComponent,
        ReviewGameHighHandComponent
    ]
})
export class YearInReviewModule { }
