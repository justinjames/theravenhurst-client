import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'

import { BrowserTitleService, ButtonTexts, FormInput, NavigationService, Player, PlayerService } from '../../core'

@Component({
    selector: 'raven-new-player',
    templateUrl: './new-player.component.html',
    styleUrls: ['./new-player.component.scss']
})
export class NewPlayerComponent implements OnInit {

    private buttonTexts : ButtonTexts = {
        initial: "Create",
        processing: 'Processing...'
    }

    items : FormInput[] = [
        new FormInput({
            key: 'first',
            label: 'First name',
            value: '',
            required: true,
            type: 'text'
        }),
        new FormInput({
            key: 'last',
            label: 'Last name',
            value: '',
            required: true,
            type: 'text'
        }),
        new FormInput({
            key: 'nick',
            label: 'Nickname (optional)',
            type: 'text'
        }),
        new FormInput({
            key: 'say',
            label: 'Saying (optional)',
            type: 'text'
        }),
        new FormInput({
            key: 'phone',
            label: 'Phone number (optional)',
            type: 'text'
        }),
        new FormInput({
            key: 'email',
            label: 'Email (optional)',
            value: '',
            required: false,
            type: 'email'
        }),
        new FormInput({
            key: 'password',
            label: 'Password (optional)',
            value: '',
            required: false,
            type: 'text'
        })
    ]
    form : FormGroup = null
    formSubmitting : boolean = false
    buttonText : string = this.buttonTexts.initial
    errorMessage : string = ""

    constructor(
        private browserTitleService : BrowserTitleService,
        private navigationService : NavigationService,
        private playerService : PlayerService
    ) {}

    ngOnInit() {
        this.browserTitleService.title = 'New Player'
        this.form = this.toFormGroup(this.items)
    }

    toFormGroup(items: FormInput[] ) {

        let group: any = {}

        items.forEach(input => {
            group[input.key] = input.required ? new FormControl(input.value || '', Validators.required) : new FormControl(input.value || '')
        })

        return new FormGroup(group)
    }

    onSubmit() {

        let payload : any = JSON.stringify(this.form.value)

        this.formSubmitting = true
        this.buttonText = this.buttonTexts.processing
        this.errorMessage = ""

        this.playerService.createPlayer(payload)
            .then(response => {
                this.navigationService.goto('players')
            })
            .catch(error => {
                this.formSubmitting = false
                this.buttonText = this.buttonTexts.initial
                this.errorMessage = `There was an error creating new player: ${error}`
            })
    }

}
