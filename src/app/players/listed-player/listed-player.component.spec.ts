import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListedPlayerComponent } from './listed-player.component';

describe('ListedPlayerComponent', () => {
  let component: ListedPlayerComponent;
  let fixture: ComponentFixture<ListedPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListedPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListedPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
