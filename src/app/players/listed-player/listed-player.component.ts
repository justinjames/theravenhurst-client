import { Component, Input, OnInit } from '@angular/core'

import { Player, PlayerService } from '../../core'

@Component({
    selector: 'raven-listed-player',
    templateUrl: './listed-player.component.html',
    styleUrls: ['./listed-player.component.scss']
})
export class ListedPlayerComponent implements OnInit {

    @Input() player : Player

    admin = false
    extraName : string

    constructor(
        private playerService : PlayerService
    ) {}

    ngOnInit() {
        this.admin = this.playerService.player.admin
        this.extraName = this.playerService.getExtraName(this.player)
    }
}