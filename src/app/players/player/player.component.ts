import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {
    BrowserTitleService,
    DateService,
    Game,
    HandService,
    NavigationService,
    NgStyle,
    Player,
    PlayerService,
    PlayerStats,
    SortableHand,
} from '../../core';

@Component({
    selector: 'raven-player',
    templateUrl: './player.component.html',
    styleUrls: ['./player.component.scss'],
})
export class PlayerComponent implements OnInit {
    playerId = '';
    player: Player = null;
    admin = false;
    extraName: string;
    noPlayer = false;
    phoneNumber: string;

    loadingStats = true;
    stats: PlayerStats;
    highHands: SortableHand[] = [];
    years: number[] = [];
    year: number;
    firstYear = 2017;

    isNotMe = false;

    headerStyle: NgStyle = {};

    constructor(
        private activatedRoute: ActivatedRoute,
        private browserTitleService: BrowserTitleService,
        private dateService: DateService,
        private handService: HandService,
        private navigationService: NavigationService,
        private playerService: PlayerService,
    ) {}

    ngOnInit() {
        this.browserTitleService.title = '';
        this.admin = this.playerService.player.admin;
        this.subscribeToRoute();
    }

    private subscribeToRoute() {
        this.activatedRoute.params.subscribe(params => {
            this.playerId = params['playerId'];
            this.loadMyPlayer();
        });
    }

    private loadMyPlayer() {
        this.playerService
            .getPlayer(this.playerId)
            .then(response => {
                this.player = response.player;
                this.browserTitleService.title = this.player.first + ' ' + this.player.last;

                this.headerStyle = {
                    'background-image': `url("${this.player.afile}")`,
                };

                this.checkIfIsNotMe();
                this.setExtraName();
                this.setPhoneNumber();
                this.setupYears();
            })
            .catch(err => {
                console.log('err >> ', err);
                this.noPlayer = true;
            });
    }

    private checkIfIsNotMe() {
        this.isNotMe = this.playerId != this.playerService.player._id;
    }

    private setupYears() {
        const now = new Date();
        let cur = now.getFullYear();

        while (cur >= this.firstYear) {
            this.years.push(cur--);
        }

        if (this.dateService.selectedYear) {
            this.year = this.dateService.selectedYear;
        } else {
            this.year = this.years[0];
        }

        this.onYearChanged();
    }

    onYearChanged() {
        this.dateService.selectedYear = this.year;
        this.loadPlayerStats();
    }

    private loadPlayerStats() {
        this.loadingStats = true;
        this.stats = null;
        this.highHands.length = 0;

        this.playerService
            .readPlayerStats(this.playerId, this.year)
            .then(stats => {
                this.stats = stats;

                if (this.stats.highHands.length > 0) {
                    this.highHands = this.handService.sortHighHands(this.stats.highHands);
                }

                this.loadingStats = false;
            })
            .catch(err => {
                console.log('err >> ', err);
                this.noPlayer = true;
            });
    }

    compare() {
        this.navigationService.goto([
            'players',
            'compare',
            this.playerService.player._id,
            this.playerId,
        ]);
    }

    // private convertValueToString(n : number) : string {
    //     if(n < 0) {
    //         return String(n).replace('-', '-$')
    //     }else{
    //         return `$${String(n)}`
    //     }
    // }

    private setExtraName() {
        this.extraName = this.playerService.getExtraName(this.player);
    }

    private setPhoneNumber() {
        if (this.player.phone) {
            this.phoneNumber =
                this.player.phone.slice(0, 3) +
                '.' +
                this.player.phone.slice(3, 6) +
                '.' +
                this.player.phone.slice(6, 10);
        }
    }

    getHighHandId(g: Game): string {
        return g._id;
    }

    getHighHandGameDate(g: Game): Date {
        return new Date(g.y, g.m, g.d);
    }
}
