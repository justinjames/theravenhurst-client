import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';

import {
    ArrayService,
    BrowserTitleService,
    Player,
    PlayerService,
} from '../core';

@Component({
    selector: 'raven-players',
    templateUrl: './players.component.html',
    styleUrls: ['./players.component.scss'],
})
export class PlayersComponent implements OnInit {
    fullPlayers: Player[];
    players: Player[];
    inactivePlayers: Player[];
    admin = false;
    find = '';

    private weHaveTyped: Subject<string> = new Subject<string>();
    private debounceTypedTime = 400;

    constructor(
        private arrayService: ArrayService,
        private browserTitleService: BrowserTitleService,
        private playerService: PlayerService
    ) {}

    ngOnInit() {
        this.browserTitleService.title = 'Players';
        this.admin = this.playerService.player.admin;
        this.subscribeToPlayers();
        this.subscribeToTyped();
    }

    private subscribeToPlayers() {
        this.playerService.playersSubject.subscribe(players => {
            this.fullPlayers = players;
            this.updateFiltered();
        });
    }

    private subscribeToTyped() {
        this.weHaveTyped.debounceTime(this.debounceTypedTime).subscribe(m => {
            this.updateFiltered();
        });
    }

    private updateFiltered() {
        const active = this.fullPlayers.filter(p => p.active);
        const inactive = this.fullPlayers.filter(p => !p.active);

        if (this.find == '') {
            this.players = active;
            this.inactivePlayers = inactive;
        } else {
            this.players = active.filter(p => {
                const fn = p.first.toLowerCase();
                const ln = p.last.toLowerCase();
                const reg = new RegExp(this.find, 'gi');

                return fn.search(reg) != -1 || ln.search(reg) != -1;
            });

            this.inactivePlayers = inactive.filter(p => {
                const fn = p.first.toLowerCase();
                const ln = p.last.toLowerCase();
                const reg = new RegExp(this.find, 'gi');

                return fn.search(reg) != -1 || ln.search(reg) != -1;
            });
        }
    }

    findChanged() {
        this.weHaveTyped.next('a');
    }
}
