import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Params } from '@angular/router'

import { BrowserTitleService, DateService, LocationService, Player, PlayerService } from '../../core'

// interface params

@Component({
    selector: 'raven-compare',
    templateUrl: './compare.component.html',
    styleUrls: ['./compare.component.scss']
})
export class CompareComponent implements OnInit {

    years : number[] = []
    year : number
    firstYear = 2017

    selectPlayerOption : Player = {_id: null, first: 'Select', last: 'Player'}
    allPlayers : Player[] = []
    playerA : Player
    playerB : Player

    parms : Params = null

    constructor(
        private activatedRoute : ActivatedRoute,
        private browserTitleService : BrowserTitleService,
        private dateService : DateService,
        private locationService : LocationService,
        private playerService : PlayerService
    ) { }

    ngOnInit() {
        this.browserTitleService.title = 'Compare'
        this.subscribeToRoute()
        this.subscribeToPlayers()
        this.setupYears()
    }

    private subscribeToRoute() {
        this.activatedRoute.params.subscribe(params => {
            this.parms = params
            this.checkParamsToSetPlayer()
        })
    }

    private subscribeToPlayers() {
        this.playerService.playersSubject.subscribe(players => {
            // this.allPlayers = [this.selectPlayerOption, ...players]
            this.allPlayers = players
            this.checkParamsToSetPlayer()
        })
    }

    private checkParamsToSetPlayer() {

        if(this.parms) {

            if(this.parms.playerId1) {

                const idx1 = this.allPlayers.findIndex(p => {
                    return p._id == this.parms.playerId1
                })

                if(idx1 != -1) {
                    this.playerA = this.allPlayers[idx1]
                }
            }else{
                // this.playerA = this.allPlayers[0]
            }

            if(this.parms.playerId2) {

                const idx2 = this.allPlayers.findIndex(p => {
                    return p._id == this.parms.playerId2
                })

                if(idx2 != -1) {
                    this.playerB = this.allPlayers[idx2]
                }else{
                    // this.playerB = this.allPlayers[0]
                }
            }
        }
    }

    private setupYears() {

        const now = new Date()
        let cur = now.getFullYear()

        while(cur >= this.firstYear) {
            this.years.push(cur--)
        }

        if(this.dateService.selectedYear) {
            this.year = this.dateService.selectedYear
        }else{
            this.year = this.years[0]
        }
    }

    onPlayerChanged(player : string) {

        let newLoc = 'players/compare'

        if(this.playerA) {

            newLoc += `/${this.playerA._id}`

            if(this.playerB) {
                newLoc += `/${this.playerB._id}`
            }
        }

        this.locationService.location = newLoc
    }
}