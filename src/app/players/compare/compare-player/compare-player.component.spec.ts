import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComparePlayerComponent } from './compare-player.component';

describe('ComparePlayerComponent', () => {
  let component: ComparePlayerComponent;
  let fixture: ComponentFixture<ComparePlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComparePlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComparePlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
