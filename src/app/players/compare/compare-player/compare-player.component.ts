import { Component, Input, OnChanges, SimpleChanges } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import {
    BrowserTitleService,
    DateService,
    Game,
    Knockout,
    KnockoutService,
    Player,
    PlayerService,
    PlayerStats,
} from "../../../core";

@Component({
    selector: "raven-compare-player",
    templateUrl: "./compare-player.component.html",
    styleUrls: ["./compare-player.component.scss"],
})
export class ComparePlayerComponent implements OnChanges {
    @Input() player: Player;
    @Input() otherPlayer: Player;
    @Input() year: number;

    loadingStats = false;
    stats: PlayerStats;
    eliminatedOponentKnockouts: Knockout[] = [];

    private prevId: string;
    private prevOtherId: string;
    private prevYear: number;

    constructor(
        private activatedRoute: ActivatedRoute,
        private browserTitleService: BrowserTitleService,
        private dateService: DateService,
        private knockoutService: KnockoutService,
        private playerService: PlayerService
    ) {}

    ngOnChanges(ch: SimpleChanges) {
        // console.log(this.year, this.prevYear);
        if (this.player && this.player._id) {
            // console.log(this.player);
            if (this.player._id != this.prevId || this.year != this.prevYear) {
                this.loadPlayerStats();

                this.prevId = this.player._id;
            }

            if (
                (this.otherPlayer &&
                    this.prevOtherId != this.otherPlayer._id) ||
                this.year != this.prevYear
            ) {
                // console.log("laod");
                this.prevOtherId = this.otherPlayer._id;
                this.loadTimesEliminatedOther();
            }

            this.prevYear = this.year;
        }
    }

    private loadPlayerStats() {
        this.loadingStats = true;
        this.stats = null;

        this.playerService
            .readPlayerStats(this.player._id, this.year)
            .then((stats) => {
                // console.log(this.player, stats);
                this.stats = stats;
                this.loadingStats = false;
            })
            .catch((err) => {
                console.log("err >> ", err);
            });
    }

    private loadTimesEliminatedOther() {
        // console.log(this.year);

        this.knockoutService
            .loadTimesEliminatedOther(
                this.player._id,
                this.otherPlayer._id,
                this.year
            )
            .then((res) => {
                this.eliminatedOponentKnockouts = res.map((e) => {
                    // console.log(e);

                    const g: Game = e.g;

                    return {
                        gid: g._id,
                        date: new Date(g.y, g.m, g.d),
                    };
                });
            })
            .catch((err) => {
                console.log("err >> ", err);
            });
    }
}
