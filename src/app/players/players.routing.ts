import { RouterModule } from '@angular/router'

import { AdminGuard, AuthenticatedGuard } from '../core'

import { CompareComponent } from './compare/compare.component'
import { EditPlayerComponent } from './edit-player/edit-player.component'
import { NewPlayerComponent } from './new-player/new-player.component'
import { PlayersComponent } from './players.component'
import { PlayerComponent } from './player/player.component'

export const PlayersRouting = RouterModule.forChild([
    { path: 'players', component: PlayersComponent, canActivate: [ AuthenticatedGuard ] },
    { path: 'players/new', component: NewPlayerComponent, canActivate: [ AuthenticatedGuard, AdminGuard ] },
    { path: 'players/compare', component: CompareComponent, canActivate: [ AuthenticatedGuard ] },
    { path: 'players/compare/:playerId1', component: CompareComponent, canActivate: [ AuthenticatedGuard ] },
    { path: 'players/compare/:playerId1/:playerId2', component: CompareComponent, canActivate: [ AuthenticatedGuard ] },
    { path: 'players/:playerId', component: PlayerComponent, canActivate: [ AuthenticatedGuard ] },
    { path: 'players/:playerId/edit', component: EditPlayerComponent, canActivate: [ AuthenticatedGuard, AdminGuard ] }
])