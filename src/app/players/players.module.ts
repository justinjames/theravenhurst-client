import { NgModule } from '@angular/core'

import { CoreModule } from '../core'

import { PlayersRouting } from './players.routing'

import { CompareComponent } from './compare/compare.component'
import { ComparePlayerComponent } from './compare/compare-player/compare-player.component'
import { EditPlayerComponent } from './edit-player/edit-player.component'
import { ListedPlayerComponent } from './listed-player/listed-player.component'
import { NewPlayerComponent } from './new-player/new-player.component'
import { PlayersComponent } from './players.component'
import { PlayerComponent } from './player/player.component'

@NgModule({
    imports: [
        CoreModule,
        PlayersRouting
    ],
    declarations: [
        CompareComponent,
        ComparePlayerComponent,
        EditPlayerComponent,
        ListedPlayerComponent,
        NewPlayerComponent,
        PlayersComponent,
        PlayerComponent
    ]
})
export class PlayersModule { }