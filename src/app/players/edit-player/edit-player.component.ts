import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { FormControl, FormGroup, Validators } from '@angular/forms'

import { BrowserTitleService, ButtonTexts, FormInput, NavigationService, Player, PlayerService } from '../../core'

@Component({
    selector: 'raven-edit-player',
    templateUrl: './edit-player.component.html',
    styleUrls: ['./edit-player.component.scss']
})
export class EditPlayerComponent implements OnInit {

    private buttonTexts : ButtonTexts = {
        initial: 'Save',
        processing: 'Processing...'
    }

    private buttonTexts2 : ButtonTexts = {
        initial: 'Update Password',
        processing: 'Processing...'
    }

    private originalPlayer : Player = {}

    private props : string[] = [
        'first',
        'last',
        'email',
        'nick',
        'say',
        'phone'
    ]

    items : FormInput[] = [
        new FormInput({
            key: 'first',
            label: 'First name',
            value: '',
            required: true,
            type: 'text'
        }),
        new FormInput({
            key: 'last',
            label: 'Last name',
            required: true,
            type: 'text'
        }),
        new FormInput({
            key: 'nick',
            label: 'Nickname (optional)',
            type: 'text'
        }),
        new FormInput({
            key: 'say',
            label: 'Saying (optional)',
            type: 'text'
        }),
        new FormInput({
            key: 'phone',
            label: 'Phone number (optional)',
            type: 'text'
        }),
        new FormInput({
            key: 'email',
            label: 'Email (optional)',
            required: false,
            type: 'email'
        }),
        new FormInput({
            key: 'password',
            label: 'Password (optional)',
            required: false,
            type: 'text'
        })
    ]
    items2 : FormInput[] = [
        new FormInput({
            key: 'password',
            label: 'New password',
            required: true,
            type: 'text'
        })
    ]
    form : FormGroup = null
    form2 : FormGroup = null
    formSubmitting : boolean = false
    buttonText : string = this.buttonTexts.initial
    errorMessage : string = ""
    formSubmitting2 : boolean = false
    buttonText2 : string = this.buttonTexts2.initial
    errorMessage2 : string = ""
    playerId : string
    hasPass = false
    matchesOriginal = true

    constructor(
        private activatedRoute : ActivatedRoute,
        private browserTitleService : BrowserTitleService,
        private navigationService : NavigationService,
        private playerService : PlayerService
    ) {}

    ngOnInit() {
        this.browserTitleService.title = 'Edit Player'
        this.subscribeToRoute()
    }

    toFormGroup(items: FormInput[] ) {

        let group: any = {}

        items.forEach(input => {
            group[input.key] = input.required ? new FormControl(input.value || '', Validators.required) : new FormControl(input.value || '')
        })

        return new FormGroup(group)
    }

    private subscribeToRoute() {
        this.activatedRoute.params.subscribe(params => {
            this.playerId = params['playerId']
            this.loadMyPlayer()
        })
    }

    private loadMyPlayer() {
        this.playerService.getPlayer(this.playerId)
            .then(response => {

                this.setOriginalPlayer(response.player)
                this.hasPass = response.hasPass

                if(this.hasPass) {

                    const last = this.items.length - 1

                    this.items = this.items.slice(0, last)
                    this.form2 = this.toFormGroup(this.items2)
                }

                this.form = this.toFormGroup(this.items)
                this.form.patchValue(response.player)
            })
            .catch(err => {
                console.log('err', err)
            })
    }

    private setOriginalPlayer(pl : Player) {
        this.props.forEach(p => {
            this.originalPlayer[p] = pl[p]
        })
    }

    formValueChanged() {

        let matches = true

        this.props.forEach(p => {
            if(this.form.value[p] != this.originalPlayer[p]) {
                matches = false
            }
        })

        this.matchesOriginal = matches

    }

    onSubmit() {

        let payloadPlayer : Player = {}

        for(const p in this.originalPlayer) {
            if(this.form.value[p] != this.originalPlayer[p]) {
                payloadPlayer[p] = this.form.value[p]
            }
        }

        if(payloadPlayer.email == '' ) {
            delete payloadPlayer.email
        }

        const payload : any = JSON.stringify(payloadPlayer)

        this.formSubmitting = true
        this.buttonText = this.buttonTexts.processing
        this.errorMessage = ''

        this.playerService.updatePlayer(this.playerId, payload)
            .then(response => {
                this.formSubmitting = false
                this.buttonText = this.buttonTexts.initial
                this.setOriginalPlayer(response)
                this.matchesOriginal = true
            })
            .catch(error => {
                this.formSubmitting = false
                this.buttonText = this.buttonTexts.initial
                this.errorMessage = `There was an error creating new player: ${error}`
            })
    }

    onSubmitPassword() {

        const payload : any = JSON.stringify(this.form2.value)

        this.formSubmitting2 = true
        this.buttonText2 = this.buttonTexts2.processing
        this.errorMessage2 = ''

        this.playerService.udpdatePassword(this.playerId, payload)
        .then(response => {
            this.formSubmitting2 = false
            this.buttonText2 = this.buttonTexts2.initial
            this.form2.patchValue({password: ''})
        })
        .catch(error => {
            this.formSubmitting2 = false
            this.buttonText2 = this.buttonTexts2.initial
            this.errorMessage2 = `There was an error creating new player: ${error}`
        })
    }
}