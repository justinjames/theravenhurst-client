import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { APP_CONFIG, AppConfig } from './config'
import { AppRoutes } from './app.routing'

import { CoreModule } from './core'
import { AnnouncementsModule } from './announcements'
import { GamesModule } from './games'
import { HighHandsModule } from './high-hands'
import { HomeModule } from './home'
import { LeaderboardModule } from './leaderboard'
import { LoginModule } from './login'
import { PlayersModule } from './players'
import { ProfileModule } from './profile'
import { SmackModule } from './smack'
import { YearInReviewModule } from './year-in-review'

import { AppComponent } from './app.component'
import { NotFoundComponent } from './not-found'

@NgModule({
    declarations: [
        AppComponent,
        NotFoundComponent
    ],
    imports: [
        AppRoutes,
        BrowserModule,
        CoreModule,
        AnnouncementsModule,
        GamesModule,
        HighHandsModule,
        HomeModule,
        LeaderboardModule,
        LoginModule,
        PlayersModule,
        ProfileModule,
        SmackModule,
        YearInReviewModule,
    ],
    exports: [],
    providers: [
        { provide: APP_CONFIG, useValue: AppConfig }
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {}