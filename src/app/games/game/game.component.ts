import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {
    ArrayService,
    BrowserTitleService,
    Entry,
    EntryService,
    Game,
    GameService,
    Hand,
    HandService,
    Knockout,
    KnockoutService,
    Player,
    PlayerService,
    SortableHand,
} from '../../core';

@Component({
    selector: 'raven-game',
    templateUrl: './game.component.html',
    styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit {
    admin = false;
    gameId = '';
    myGame: Game;
    // highHand: Hand;
    highHands: Hand[];
    // entries : Entry[] = []
    players: Player[] = [];
    gamePlayers: Player[] = [];
    playersAlive: Player[] = [];
    knockouts: Knockout[] = [];
    itemsLoaded = 0;
    totalItems = 3; // game, knockouts, high hand
    updates = 0;
    highHandTotalEntries = 0;
    gamePot = '-';
    loading = true;
    // highHandWinner: string = null;
    highHandWinners: string[] = null;
    prevGameId: string;
    nextGameId: string;
    year: number;
    totalChips: number;

    private playersLoadedOnce = false;

    constructor(
        private activatedRoute: ActivatedRoute,
        private arrayService: ArrayService,
        private browserTitleService: BrowserTitleService,
        private entryService: EntryService,
        private gameService: GameService,
        private handService: HandService,
        private knockoutService: KnockoutService,
        private playerService: PlayerService
    ) { }

    ngOnInit() {
        this.browserTitleService.title = 'Game';
        this.admin = this.playerService.player.admin;
        this.subscribeToRoute();
        this.subscribeToPlayers();
    }

    private subscribeToRoute() {
        this.activatedRoute.params.subscribe(params => {
            this.gameId = params['gameId'];

            if (this.playersLoadedOnce) {
                this.initLoads();
            }
        });
    }

    private subscribeToPlayers() {
        this.playerService.playersSubject.subscribe(players => {
            if (players.length > 0) {
                this.players = players;

                if (!this.playersLoadedOnce) {
                    this.initLoads();
                    this.playersLoadedOnce = true;
                }
            }
        });
    }

    private initLoads() {
        this.loading = true;
        this.myGame = null;
        this.highHands = [];
        this.highHandWinners = [];
        this.knockouts.length = 0;
        this.year = null;
        this.itemsLoaded = 0;

        this.loadHighHand();
        this.loadGame();
        this.loadKnockouts();
    }

    private loadHighHand() {
        this.handService
            .loadGameHand(this.gameId)
            .then(hands => {
                // if(hands.length == 1) {
                const hhs: SortableHand[] = <SortableHand[]>hands;
                hhs.forEach((hand: SortableHand) => {
                    // highHands
                    const tempHand = <SortableHand>hand;

                    if (tempHand.h) {
                        tempHand.hand = hand.h.split(',');
                        tempHand.type = this.handService.determineHandType(tempHand.hand);
                        tempHand.display = this.handService.getHandDisplay(tempHand);
                        this.highHands.push(tempHand);
                        const plr: Player = <Player>tempHand.p;
                        this.highHandWinners.push(plr._id);
                    }
                });

                // }

                // if(hands.length > 1) {

                // }

                this.itemHasLoaded();
            })
            .catch(err => {
                console.log(err);
            });
    }

    private loadGame() {
        this.gameService
            .loadGame(this.gameId, false)
            .then(response => {
                this.highHandTotalEntries = 0;

                this.myGame = response;
                this.year = new Date(this.myGame.date).getFullYear();
                this.getNeighborGameIds();

                let totEntries = 0;
                let secondEntries = 0;
                let thirdEntries = 0;

                this.myGame.entries.forEach(e => {
                    totEntries += e.b;
                    secondEntries += e.b > 1 ? 1 : 0;
                    thirdEntries += e.b > 2 ? e.b - 2 : 0;

                    if (e.hh == 1) {
                        ++this.highHandTotalEntries;
                    }
                });

                this.totalChips = totEntries * 10000 + secondEntries * 2500 + thirdEntries * 5000;

                this.itemHasLoaded();
            })
            .catch(err => {
                console.log(err);
            });
    }

    private getNeighborGameIds() {
        this.gameService.gameDatesAndIdsSubject.subscribe(ids => {
            const idx = ids.findIndex(g => g._id == this.gameId);

            if (idx != -1) {
                const prev = idx + 1;
                const next = idx - 1;

                this.prevGameId = ids[prev] ? ids[prev]._id : null;
                this.nextGameId = ids[next] ? ids[next]._id : null;
            }
        });

        // const games = this.gameService.getNeighborGameIds(this.gameId, this.myGame.date.getFullYear())

        //  = games[0]._id ? games[0]._id : null
        //  = games[1]._id ? games[1]._id : null

        // this.gameService.getNeighborGameId(this.gameId, true)
        //     .then(id => {
        //         // console.log('prev', id)
        //         this.prevGameId = id
        //     })
        //     .catch(err => {
        //         console.log(err)
        //     })

        // this.gameService.getNeighborGameId(this.gameId, false)
        //     .then(id => {
        //         // console.log('next', id)
        //         this.nextGameId = id
        //     })
        //     .catch(err => {
        //         console.log(err)
        //     })
    }

    // private loadEntries() {
    //     this.entryService.loadGameEntries(this.gameId)
    //         .then(entries => {
    //             this.entries = entries
    //             this.itemHasLoaded()
    //         })
    //         .catch(err => {
    //             console.log(err)
    //         })
    // }

    private loadKnockouts() {
        this.knockoutService
            .loadGameKnockouts(this.gameId)
            .then(response => {
                this.knockouts = this.arrayService.sortByObjectProperty(response, 's');
                this.itemHasLoaded();
            })
            .catch(err => {
                console.log(err);
            });
    }

    private itemHasLoaded() {
        ++this.itemsLoaded;

        // console.log(this.itemsLoaded, this.totalItems)

        if (this.itemsLoaded >= this.totalItems) {
            const counts = {};

            this.knockouts.forEach(k => {
                const loserId = k.l._id;
                const loserIdx = this.myGame.entries.findIndex(e => e.p._id == loserId);
                const rebuys = loserIdx == -1 ? 1 : this.myGame.entries[loserIdx].b - 1;

                if (!counts[loserId]) {
                    counts[loserId] = {
                        rebuys,
                    };
                }
            });

            this.knockouts.forEach(k => {
                const loserId = k.l._id;

                if (counts[loserId].rebuys > 0) {
                    k.r = true;
                }

                --counts[loserId].rebuys;
            });

            this.updatePlayers();
            this.loading = false;
        }
    }

    private updatePlayers() {
        this.updatePlayersSelected();
        this.updateGamePlayers();
        this.updatePlayersAlive();
        this.updateGamePot();

        ++this.updates;
    }

    private updatePlayersSelected() {
        this.players.forEach(p => (p.selected = false));

        this.myGame.entries.forEach(e => {
            let idx = this.players.findIndex(p => e.p._id == p._id);

            if (idx != -1) {
                this.players[idx].selected = true;
            }
        });
    }

    private updateGamePlayers() {
        this.gamePlayers = this.players.filter(p => p.selected);
    }

    private updatePlayersAlive() {
        this.playersAlive.length = 0;

        this.gamePlayers.forEach(gp => {
            const myEntry = this.myGame.entries.filter(e => e.p._id == gp._id)[0];
            const myKnockouts = this.knockouts.filter(k => k.l._id == gp._id);

            gp.alive = myEntry.b > myKnockouts.length;
        });

        this.playersAlive = this.gamePlayers.filter(p => p.alive);
    }

    private updateGamePot() {
        // console.log(this.myGame)
        this.gamePot = this.gameService.determineGamePot(this.myGame);
    }
}
