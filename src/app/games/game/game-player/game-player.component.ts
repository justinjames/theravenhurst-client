import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';

import { Entry, Knockout, Player, PlayerService } from '../../../core';

@Component({
    selector: 'raven-game-player',
    templateUrl: './game-player.component.html',
    styleUrls: ['./game-player.component.scss'],
})
export class GamePlayerComponent implements OnChanges {
    @Input() player: Player;
    @Input() entries: Entry[] = [];
    @Input() knockouts: Knockout[] = [];
    @Input() highHandWinners: string[];
    @Input() highHandTotalEntries: number;
    @Input() updates: number;

    myEntry: Entry;
    bountyMoney = '';
    net = '';
    private entryValue = 0;
    entryCost = '$0';
    highHandValue = '';
    highHandCost = 5;

    constructor(private playerService: PlayerService) {}

    ngOnChanges() {
        this.updateMyEntry();
    }

    private updateMyEntry() {
        let idx = this.entries.findIndex(e => e.p._id == this.player._id);

        if (idx != -1) {
            this.myEntry = this.entries[idx];

            this.entryValue = this.myEntry.b * -20;
            this.entryCost =
                this.entryValue >= 0
                    ? '$' + this.entryValue
                    : String(this.entryValue).replace('-', '-$');

            const pid = this.player._id;
            const wins = this.knockouts.filter(k => {
                if (k.w) {
                    return k.w._id == pid && k.b == 1;
                }
                return false;
            });
            const losses = this.knockouts.filter(k => k.l._id == pid && k.b == 1);
            const bountyValue = (wins.length - losses.length) * 5;

            let netValue = (wins.length - losses.length) * 5 + this.myEntry.c + this.entryValue;

            if (this.highHandWinners && this.highHandWinners.length) {
                if (this.highHandWinners.includes(this.player._id)) {
                    const hhPot =
                        (this.highHandTotalEntries * this.highHandCost) /
                            this.highHandWinners.length -
                        this.highHandCost;
                    this.highHandValue = '$' + hhPot;
                    netValue += hhPot;
                } else {
                    this.highHandValue = this.myEntry.hh == 1 ? `-$${this.highHandCost}` : '';
                    netValue += this.myEntry.hh == 1 ? this.highHandCost * -1 : 0;
                }
            }

            this.bountyMoney =
                bountyValue >= 0 ? '$' + bountyValue : String(bountyValue).replace('-', '-$');
            this.net = netValue >= 0 ? '$' + netValue : String(netValue).replace('-', '-$');
        }
    }
}
