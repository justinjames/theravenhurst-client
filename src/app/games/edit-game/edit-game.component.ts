import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Subject } from "rxjs/Subject";
import "rxjs/add/operator/debounceTime";
import { MatSlideToggle } from "@angular/material";

import {
    ArrayService,
    BrowserTitleService,
    DateService,
    Entry,
    EntryService,
    Game,
    GameService,
    Knockout,
    KnockoutRequestMove,
    KnockoutService,
    NavigationService,
    Player,
    PlayerService
} from "../../core";

@Component({
    selector: "raven-edit-game",
    templateUrl: "./edit-game.component.html",
    styleUrls: ["./edit-game.component.scss"]
})
export class EditGameComponent implements OnInit {
    gameId = "";
    myGame: Game;
    // entries : Entry[] = []
    players: Player[] = [];
    playersToChoose: Player[] = [];
    gamePlayers: Player[] = [];
    playersAlive: Player[] = [];
    knockouts: Knockout[] = [];
    gameIsTodayOrOlder = false;
    itemsLoaded = 0;
    totalItems = 3; // game, players, knockouts
    updates = 0;
    gamePot = "-";
    notesSaved: Date = null;
    showInactive = false;
    adjustingKnockouts = false;
    totalChips: number;

    private notesChanged: Subject<string> = new Subject<string>();
    private notesChangedDebounceTime = 400;

    constructor(
        private activatedRoute: ActivatedRoute,
        private arrayService: ArrayService,
        private browserTitleService: BrowserTitleService,
        private dateService: DateService,
        private entryService: EntryService,
        private gameService: GameService,
        private knockoutService: KnockoutService,
        private navigationService: NavigationService,
        private playerService: PlayerService
    ) { }

    ngOnInit() {
        // console.log(">", this.myGame);
        this.browserTitleService.title = "Edit Game";
        this.subscribeToRoute();
        this.subscribeToPlayers();
        this.subscribeToNotesChanged();
    }

    private subscribeToRoute() {
        this.activatedRoute.params.subscribe(params => {
            this.gameId = params["gameId"];
            this.loadGame();
            // this.loadEntries()
            this.loadKnockouts();
        });
    }

    private subscribeToPlayers() {
        this.playerService.playersSubject.subscribe(players => {
            /// FIND OUT WHY THIS IS GETTING CALLED MORE THAN ONCE ON INITIAL LOAD WITH MORE THAN 0
            // console.log('PLAYERS LOADED 1', players.length)
            if (players.length > 0 && players.length != this.players.length) {
                // console.log('PLAYERS LOADED 2', players.length)
                this.players = players;
                this.itemHasLoaded();
            }
        });
    }

    private subscribeToNotesChanged() {
        this.notesChanged
            .debounceTime(this.notesChangedDebounceTime)
            .subscribe(m => {
                this.saveNotes();
            });
    }

    private loadGame() {
        this.gameService
            .loadGame(this.gameId, true)
            .then(response => {
                this.myGame = response;
                // console.log(response);
                this.gameIsTodayOrOlder = this.dateService.isPastDate(
                    this.myGame.date
                );
                this.itemHasLoaded();
            })
            .catch(err => {
                console.log(err);
            });
    }

    // private loadEntries() {
    //     this.entryService.loadGameEntries(this.gameId)
    //         .then(entries => {
    //             this.entries = entries
    //             this.itemHasLoaded()
    //         })
    //         .catch(err => {
    //             console.log(err)
    //         })
    // }

    private loadKnockouts() {
        this.knockoutService
            .loadGameKnockouts(this.gameId)
            .then(response => {
                // console.log(response)
                this.knockouts = response;
                this.sortKnockouts();
                this.itemHasLoaded();
            })
            .catch(err => {
                console.log(err);
            });
    }

    onKnockoutRemoved(id: string) {
        const ko = this.knockouts.find(k => k._id == id);
        this.adjustingKnockouts = true;

        if (ko) {
            const playReturn = <Player>ko.l;
            const playerReturnedId = playReturn._id;
            const entry = this.myGame.entries.find(e => {
                const p = <Player>e.p;
                return p._id == playerReturnedId;
            });

            let timesEliminated = 0;

            this.knockouts = this.knockouts.filter(k => k._id != id);
            this.knockouts.forEach(k => {
                const p = <Player>k.l;
                if (p._id == playerReturnedId) {
                    ++timesEliminated;
                }
            });

            if (entry && entry.b - 1 > timesEliminated) {
                entry.b = timesEliminated + 1;

                this.entryService
                    .updateEntry(entry)
                    .then(res => {
                        this.sortKnockouts();
                        this.updatePlayers();
                        this.adjustingKnockouts = false;
                    })
                    .catch(err => {
                        console.log(err);
                    });
            } else {
                this.sortKnockouts();
                this.updatePlayers();
                this.adjustingKnockouts = false;
            }
        }
    }

    onKnockoutMoveRequested(evt: KnockoutRequestMove) {
        const { index, up } = evt;

        if (index == 0 && up) {
            // do nothing
        } else if (index == this.knockouts.length - 1 && !up) {
            // do nothing
        } else {
            this.adjustingKnockouts = true;

            const neighborIndex = up ? index - 1 : index + 1;
            const neighbor = this.knockouts[neighborIndex];
            const thisK = this.knockouts[index];

            if (neighbor && thisK) {
                const neighborS = new Date(neighbor.s);
                const thisKS = new Date(thisK.s);

                this.knockoutService
                    .updateKnockoutSort(neighbor._id, thisKS)
                    .then(newNeighbor => {
                        this.knockoutService
                            .updateKnockoutSort(thisK._id, neighborS)
                            .then(newThisK => {
                                neighbor.s = newNeighbor.s;
                                thisK.s = newThisK.s;

                                this.adjustingKnockouts = false;
                                this.sortKnockouts();
                            })
                            .catch(err => {
                                console.log(err);
                            });
                    })
                    .catch(err => {
                        console.log(err);
                    });
            } else {
                this.adjustingKnockouts = false;
            }
        }
    }

    private itemHasLoaded() {
        ++this.itemsLoaded;

        if (this.itemsLoaded >= this.totalItems) {
            this.updatePlayers();
        }
    }

    private updatePlayers() {
        this.updatePlayersSelected();
        this.updatePlayersToChoose();
        this.updateGamePlayers();
        this.updatePlayersAlive();
        this.updateGamePot();

        ++this.updates;
    }

    private updatePlayersSelected() {
        this.players.forEach(p => (p.selected = false));

        this.myGame.entries.forEach(e => {
            const idx = this.players.findIndex(p => e.p._id == p._id);

            if (idx != -1) {
                this.players[idx].selected = true;
            }
        });
    }

    onToggleChange(e: MatSlideToggle) {
        // console.log(this.showInactive, e.checked)
        this.showInactive = e.checked;
        this.updatePlayersToChoose();
    }

    private updatePlayersToChoose() {
        this.playersToChoose = this.players.filter(p => {
            if (this.showInactive) {
                return true;
            } else {
                return p.active || p.selected;
            }
        });
    }

    private updateGamePlayers() {
        this.gamePlayers = this.players.filter(p => p.selected);
    }

    private updatePlayersAlive() {
        this.playersAlive.length = 0;

        this.gamePlayers.forEach(gp => {
            const myEntry = this.myGame.entries.filter(
                e => e.p._id == gp._id
            )[0];
            const myKnockouts = this.knockouts.filter(k => k.l._id == gp._id);

            gp.alive = myEntry.b > myKnockouts.length;
        });

        this.playersAlive = this.gamePlayers.filter(p => p.alive);
    }

    private updateGamePot() {
        this.gamePot = this.gameService.determineGamePot(this.myGame);

        let totEntries = 0;
        let secondEntries = 0;
        let thirdEntries = 0;

        this.myGame.entries.forEach(e => {
            totEntries += e.b;
            secondEntries += e.b > 1 ? 1 : 0;
            thirdEntries += e.b > 2 ? e.b - 2 : 0;
        });

        this.totalChips =
            totEntries * 10000 + secondEntries * 2500 + thirdEntries * 5000;
    }

    onKnockout(k: Knockout) {
        if (!k.s) {
            k.s = k.createdAt;
        }
        this.knockouts.push(k);
        this.sortKnockouts();
        this.updatePlayers();
    }

    private sortKnockouts() {
        this.knockouts = this.arrayService.sortByObjectProperty(
            this.knockouts,
            "s"
        );
    }

    onTogglePlayer(p: Player) {
        // console.log(p);
        if (!p.saving) {
            p.saving = true;

            if (p.selected) {
                const idx = this.myGame.entries.findIndex(
                    e => e.p._id == p._id
                );

                // console.log(idx);

                if (idx != -1) {
                    this.entryService
                        .removePlayerFromGame(this.myGame.entries[idx]._id)
                        .then(response => {
                            p.selected = false;
                            p.saving = false;
                            // console.log(p);
                            this.myGame.entries.splice(idx, 1);
                            this.updateGamePot();
                            this.updatePlayersToChoose();
                        })
                        .catch(err => {
                            console.log(err);
                        });
                }

                return;
            }

            // console.log(this.myGame.entries);

            this.entryService
                .addPlayerToGame(this.gameId, this.myGame.date, p._id)
                .then(response => {
                    // console.log(response);

                    const { _id, first, last } = p;
                    const copy = { ...response, p: { _id, first, last } };

                    p.selected = true;
                    p.saving = false;
                    this.playerService.activatePlayerIfNotActive(p._id);
                    this.myGame.entries.push(copy);
                    this.updateGamePot();
                })
                .catch(err => {
                    console.log(err);
                });
        }
    }

    onRebuyPlayer(e: Entry) {
        this.updatePlayers();
    }

    onUpdatePlayerPoints(e: Entry) {
        // this.updatePlayers()
    }

    advanceGameState() {
        this.gameService
            .advanceGameState(this.gameId)
            .then(response => {
                this.myGame = response;
                this.updatePlayers();
            })
            .catch(err => {
                console.log(err);
            });
    }

    notesChanges() {
        this.notesChanged.next("a");
    }

    private saveNotes() {
        const payload: Game = {
            _id: this.myGame._id,
            n: this.myGame.n
        };

        if (this.myGame.name) {
            payload.name = this.myGame.name;
        }

        this.gameService
            .saveGame(payload)
            .then(response => {
                this.notesSaved = new Date();
            })
            .catch(err => {
                console.log(err);
            });
    }

    managePlayers() {
        this.navigationService.goto([
            "games",
            this.myGame._id,
            "edit",
            "players"
        ]);
    }
}
