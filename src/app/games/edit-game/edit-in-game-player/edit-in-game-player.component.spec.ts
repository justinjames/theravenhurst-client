import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditInGamePlayerComponent } from './edit-in-game-player.component';

describe('EditInGamePlayerComponent', () => {
  let component: EditInGamePlayerComponent;
  let fixture: ComponentFixture<EditInGamePlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditInGamePlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditInGamePlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
