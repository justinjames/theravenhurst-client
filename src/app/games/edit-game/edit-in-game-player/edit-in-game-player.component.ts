import { Component, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core'

import { Entry, Knockout, Player, EntryService } from '../../../core'

@Component({
    selector: 'raven-edit-in-game-player',
    templateUrl: './edit-in-game-player.component.html',
    styleUrls: ['./edit-in-game-player.component.scss']
})
export class EditInGamePlayerComponent implements OnChanges {

    @Input() player : Player
    @Input() entries : Entry[] = []
    @Input() knockouts : Knockout[] = []
    @Input() updates : number
    @Output() rebuyPlayer = new EventEmitter<Player>()

    myEntry : Entry = {}
    saving = false
    isHighHand = false

    constructor(
        private entryService : EntryService
    ) {}

    ngOnChanges(ch : SimpleChanges) {
        this.updateMyEntry()
    }

    private updateMyEntry() {

        let idx = this.entries.findIndex(e => e.p._id == this.player._id)

        if(idx != -1) {
            this.myEntry = this.entries[idx]
            this.isHighHand = this.myEntry.hh == 1
        }else{
            this.myEntry = {}
        }
    }

    rebuy() {

        this.saving = true
        ++this.myEntry.b

        this.entryService.updateEntry(this.myEntry)
            .then(response => {
                this.saving = false
                this.rebuyPlayer.emit(response)
            })
            .catch(err => {
                console.log(err)
            })
    }

    unrebuy() {

        this.saving = true
        --this.myEntry.b

        this.entryService.updateEntry(this.myEntry)
            .then(response => {
                this.saving = false
                this.rebuyPlayer.emit(response)
            })
            .catch(err => {
                console.log(err)
            })
    }

    isUndoRebyShowing() : boolean {

        if(!this.player.alive) {
            return false
        }

        const numberRebuys = this.myEntry.b - 1
        const myKnockedOuts = this.knockouts.filter(k => k.l._id == this.player._id)
        const numbMyKnockouts = myKnockedOuts.length

        return numbMyKnockouts == numberRebuys && numberRebuys > 0
    }

    highHandChanged() {

        this.saving = true
        this.myEntry.hh = this.isHighHand ? 1 : 0

        this.entryService.updateEntry(this.myEntry)
            .then(response => {
                this.saving = false
            })
            .catch(err => {
                console.log(err)
            })
    }
}