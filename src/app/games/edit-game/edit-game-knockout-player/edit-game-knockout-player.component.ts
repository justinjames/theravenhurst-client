import { Component, Input, Output, OnChanges, SimpleChanges, EventEmitter } from '@angular/core'

import { Knockout, KnockoutService, Player, PlayerService } from '../../../core'

@Component({
    selector: 'raven-edit-game-knockout-player',
    templateUrl: './edit-game-knockout-player.component.html',
    styleUrls: ['./edit-game-knockout-player.component.scss']
})
export class EditGameKnockoutPlayerComponent implements OnChanges {

    @Input() playersAlive : Player[]
    @Input() gameId : string
    @Output() registerKnockout = new EventEmitter<Knockout>()

    selectedWinner = ''
    selectedLoser = ''
    losers : Player[] = []
    working = false
    bounty = false

    constructor(
        private knockoutService : KnockoutService
    ) {}

    ngOnChanges(ch : SimpleChanges) {
        this.updateLosers()
    }

    winnerUpdated() {
        this.updateLosers()
    }

    private updateLosers() {
        this.losers = this.playersAlive.filter(p => p._id != this.selectedWinner)
        this.selectedLoser = ''
    }

    createKnockout() {

        this.working = true

        this.knockoutService.createKnockout(this.gameId, this.selectedWinner, this.selectedLoser, this.bounty)
            .then(response => {
                this.registerKnockout.emit(response)
                this.selectedWinner = ''
                this.selectedLoser = ''
                this.bounty = false
                this.working = false
            })
            .catch(err => {
                console.log(err)
            })
    }

}