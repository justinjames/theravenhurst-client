import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGameKnockoutPlayerComponent } from './edit-game-knockout-player.component';

describe('EditGameKnockoutPlayerComponent', () => {
  let component: EditGameKnockoutPlayerComponent;
  let fixture: ComponentFixture<EditGameKnockoutPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditGameKnockoutPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGameKnockoutPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
