import { Component, Input, Output, EventEmitter } from '@angular/core'

import { Knockout, KnockoutRequestMove, KnockoutService } from '../../../core'

@Component({
    selector: 'raven-edit-game-knockout',
    templateUrl: './edit-game-knockout.component.html',
    styleUrls: ['./edit-game-knockout.component.scss']
})
export class EditGameKnockoutComponent {

    @Input() knockout : Knockout
    @Input() gameId : string
    @Input() index : number
    @Input() adjustingKnockouts : boolean
    @Output() knockoutRemoved = new EventEmitter<string>()
    @Output() requestMove = new EventEmitter<KnockoutRequestMove>()

    working = false

    constructor(
        private knockoutService : KnockoutService
    ) {}

    adjustBounty() {

        if(!this.working && !this.adjustingKnockouts) {

            this.working = true
            const b = this.knockout.b == 0 ? 1 : 0

            this.knockoutService.updateKnockoutBounty(this.knockout._id, b)
                .then(data => {
                    this.knockout.b = data.b
                    this.working = false
                })
                .catch(err => {
                    console.log(err)
                })
        }
    }

    moveKnockout(up : boolean) {
        if(!this.working && !this.adjustingKnockouts) {
            const index = this.index
            this.requestMove.emit({
                index,
                up
            })
        }
    }

    removeKnockout() {

        if(!this.working && !this.adjustingKnockouts) {

            this.working = true
            const id = this.knockout._id

            this.knockoutService.removeKnockout(this.knockout._id)
                .then(data => {
                    if(data.deleted) {
                        this.knockoutRemoved.emit(id)
                    }else{
                        this.working = false
                    }
                })
                .catch(err => {
                    console.log(err)
                })
        }
    }

}