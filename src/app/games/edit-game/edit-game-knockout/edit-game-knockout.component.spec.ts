import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGameKnockoutComponent } from './edit-game-knockout.component';

describe('EditGameKnockoutComponent', () => {
  let component: EditGameKnockoutComponent;
  let fixture: ComponentFixture<EditGameKnockoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditGameKnockoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGameKnockoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
