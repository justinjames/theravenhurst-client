import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { MatSlideToggle } from "@angular/material";

import {
    // ArrayService,
    BrowserTitleService,
    // DateService,
    // Entry,
    EntryService,
    Game,
    GameService,
    Knockout,
    // KnockoutRequestMove,
    KnockoutService,
    NavigationService,
    Player,
    PlayerService
} from "../../../core";

@Component({
    selector: "qa-edit-game-manage-players",
    templateUrl: "./edit-game-manage-players.component.html",
    styleUrls: ["./edit-game-manage-players.component.scss"]
})
export class EditGameManagePlayersComponent implements OnInit {
    gameId = "";
    myGame: Game;
    players: Player[] = [];
    private allPlayers: Player[] = [];
    knockouts: Knockout[] = [];

    playersSelected = 0;

    itemsLoaded = 0;
    totalItems = 3; // game, players, knockouts
    showInactive = false;
    working = false;

    constructor(
        private activatedRoute: ActivatedRoute,
        private browserTitleService: BrowserTitleService,
        private entryService: EntryService,
        private gameService: GameService,
        private knockoutService: KnockoutService,
        private navigationService: NavigationService,
        private playerService: PlayerService
    ) { }

    ngOnInit() {
        this.browserTitleService.title = "Manage Game's Players";
        this.subscribeToRoute();
        this.subscribeToPlayers();
    }

    private subscribeToRoute() {
        this.activatedRoute.params.subscribe(params => {
            this.gameId = params["gameId"];
            this.loadGame();
            this.loadKnockouts();
        });
    }

    private subscribeToPlayers() {
        this.playerService.playersSubject.subscribe(players => {
            /// FIND OUT WHY THIS IS GETTING CALLED MORE THAN ONCE ON INITIAL LOAD WITH MORE THAN 0
            // console.log('PLAYERS LOADED 1', players.length)
            if (
                players.length > 0 &&
                players.length != this.allPlayers.length
            ) {
                this.allPlayers = players;
                this.itemHasLoaded();
            }
        });
    }

    private loadGame() {
        this.gameService
            .loadGame(this.gameId, false)
            .then(response => {
                this.myGame = response;
                this.itemHasLoaded();
            })
            .catch(err => {
                console.log(err);
            });
    }

    private loadKnockouts() {
        this.knockoutService
            .loadGameKnockouts(this.gameId)
            .then(response => {
                this.knockouts = response;
                this.itemHasLoaded();
            })
            .catch(err => {
                console.log(err);
            });
    }

    private itemHasLoaded() {
        ++this.itemsLoaded;

        if (this.itemsLoaded >= this.totalItems) {
            this.updatePlayers();
        }
    }

    private updatePlayers() {
        const playerIdsInGame = this.myGame.entries.map(e => e.p._id);
        const winningKnockout = this.knockouts.map(k => k.w._id);
        const losingKnockout = this.knockouts.map(k => k.l._id);
        const playersInvolvedInAction = [...winningKnockout, ...losingKnockout];

        this.players = this.allPlayers
            .map(p => {
                const selected = playerIdsInGame.includes(p._id);
                const cannotRemove = playersInvolvedInAction.includes(p._id);
                return { ...p, selected, cannotRemove };
            })
            .filter(p => {
                if (this.showInactive) {
                    return true;
                }

                return p.selected || p.active;
            });

        this.updateplayersSelected();
    }

    onToggleChange(e: MatSlideToggle) {
        this.showInactive = e.checked;
        this.updatePlayers();
    }

    adjustPlayer(plyr: Player) {
        this.working = true;
        const foundEntry = this.myGame.entries.find(e => e.p._id == plyr._id);

        if (plyr.selected && !plyr.cannotRemove && foundEntry) {
            this.entryService
                .removePlayerFromGame(foundEntry._id)
                .then(resp => {
                    if (resp.deleted) {
                        this.working = false;
                        plyr.selected = false;

                        this.myGame.entries = this.myGame.entries.filter(
                            e => e.p._id != plyr._id
                        );

                        this.updateplayersSelected();
                    }
                });
        } else if (!plyr.selected && !foundEntry) {
            this.entryService
                .addPlayerToGame(this.myGame._id, this.myGame.date, plyr._id)
                .then(resp => {
                    this.working = false;
                    plyr.selected = true;

                    const p = {
                        _id: plyr._id,
                        first: plyr.first,
                        last: plyr.last
                    };
                    const entryCopy = { ...resp, p };

                    this.myGame.entries = [...this.myGame.entries, entryCopy];

                    this.updateplayersSelected();
                });
        } else {
            this.working = false;
        }
    }

    private updateplayersSelected() {
        this.playersSelected = this.players.reduce((a, b) => {
            return (a += b.selected ? 1 : 0);
        }, 0);
    }

    backToGame() {
        this.navigationService.goto(["games", this.myGame._id, "edit"]);
    }
}
