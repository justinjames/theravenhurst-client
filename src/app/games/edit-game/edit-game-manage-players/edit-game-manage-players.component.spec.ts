import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGameManagePlayersComponent } from './edit-game-manage-players.component';

describe('EditGameManagePlayersComponent', () => {
  let component: EditGameManagePlayersComponent;
  let fixture: ComponentFixture<EditGameManagePlayersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditGameManagePlayersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGameManagePlayersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
