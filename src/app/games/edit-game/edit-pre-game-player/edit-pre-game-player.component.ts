import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'

import { Player, PlayerService } from '../../../core'

@Component({
    selector: 'raven-edit-pre-game-player',
    templateUrl: './edit-pre-game-player.component.html',
    styleUrls: ['./edit-pre-game-player.component.scss']
})
export class EditPreGamePlayerComponent implements OnInit {

    @Input() player : Player
    @Output() togglePlayer = new EventEmitter<Player>()

    constructor(
        private playerService : PlayerService
    ) { }

    ngOnInit() {
        // console.log(this.player)
    }

    onClick() {
        this.togglePlayer.emit(this.player)
    }
}