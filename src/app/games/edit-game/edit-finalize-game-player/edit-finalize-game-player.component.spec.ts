import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPreGamePlayerComponent } from './edit-finalize-game-player.component';

describe('EditPreGamePlayerComponent', () => {
  let component: EditPreGamePlayerComponent;
  let fixture: ComponentFixture<EditPreGamePlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPreGamePlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPreGamePlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
