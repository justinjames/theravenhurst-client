import {
    Component,
    OnChanges,
    Input,
    Output,
    EventEmitter,
} from '@angular/core';
import { MatSlideToggleChange } from '@angular/material';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';

import {
    Entry,
    EntryService,
    Knockout,
    Player,
    PlayerService,
} from '../../../core';

@Component({
    selector: 'raven-edit-finalize-game-player',
    templateUrl: './edit-finalize-game-player.component.html',
    styleUrls: ['./edit-finalize-game-player.component.scss'],
})
export class EditFinalizeGamePlayerComponent implements OnChanges {
    @Input() i: number;
    @Input() player: Player;
    @Input() entries: Entry[] = [];
    @Input() knockouts: Knockout[] = [];
    @Input() updates: number;
    @Output() updatePlayerPoints = new EventEmitter<Player>();

    myEntry: Entry = {};
    saving = false;
    moneyShowing = false;

    private cashHasChanged: Subject<string> = new Subject<string>();
    private cashDebounceTime = 400;

    constructor(private entryService: EntryService) {}

    ngOnChanges() {
        this.updateMyEntry();
        this.subscribeToCashChanged();
    }

    private updateMyEntry() {
        const idx = this.entries.findIndex(e => e.p._id == this.player._id);

        if (idx != -1) {
            this.myEntry = this.entries[idx];
        } else {
            this.myEntry = {};
        }
    }

    private subscribeToCashChanged() {
        this.cashHasChanged.debounceTime(this.cashDebounceTime).subscribe(m => {
            this.saveEntry(null);
        });
    }

    cashChanged() {
        this.cashHasChanged.next('a');
    }

    saveEntry(e: MatSlideToggleChange) {
        if (e) {
            this.myEntry.pt = e.checked;
        }

        this.saving = true;

        this.entryService
            .updateEntry(this.myEntry)
            .then(response => {
                this.saving = false;
                this.updatePlayerPoints.emit(response);
            })
            .catch(err => {
                console.log(err);
            });
    }
}
