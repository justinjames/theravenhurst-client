import { Component, OnInit, Input } from '@angular/core';

import { Hand, HandService, Player, SortableHand } from '../../../core';

interface CardSelector {
    [key: string]: boolean;
}

@Component({
    selector: 'raven-edit-high-hand',
    templateUrl: './edit-high-hand.component.html',
    styleUrls: ['./edit-high-hand.component.scss'],
})
export class EditHighHand implements OnInit {
    @Input()
    gameId: string;
    @Input()
    players: Player[];

    hhString: string;
    hhPlayer: Player;

    suits: string[] = ['s', 'h', 'c', 'd'];

    cards: number[] = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];

    cardsSelected: CardSelector = {};
    fiveSelected = false;
    saving = false;

    // private hand: Hand;
    private hands: Hand[];

    constructor(private handService: HandService) {}

    ngOnInit() {
        this.initCardsSelected();
        this.loadHighHand();
    }

    private initCardsSelected() {
        this.suits.forEach(s => {
            this.cards.forEach(c => {
                this.cardsSelected[s + c] = false;
            });
        });
    }

    private loadHighHand() {
        this.handService
            .loadGameHand(this.gameId)
            .then(hands => {
                this.hands = [];
                const hhs: SortableHand[] = <SortableHand[]>hands;
                if (hhs && hhs.length) {
                    hhs.forEach(hh => {
                        if (hh._id) {
                            this.hands.push(<Hand>hh);

                            this.setCardsSelected();
                            this.setHighHandPlayer();
                        }
                    });
                }
            })
            .catch(err => {
                console.log(err);
            });
    }

    getCardPath(returnNumb: boolean, suit: string, numb: number): string {
        let path: string;
        const red = suit == 'h' || suit == 'd';

        if (returnNumb) {
            path = `/assets/cards/${red ? 'red' : 'black'}/${numb}.svg`;
        } else {
            path = `/assets/cards/suits/${suit}.svg`;
        }

        return path;
    }

    private setCardsSelected() {
        const cards = this.hands[0].h.split(',');

        cards.forEach(c => {
            this.cardsSelected[c] = true;
        });
    }

    private setHighHandPlayer() {
        const idx = this.players.findIndex(p => {
            return p._id == this.hands[0].p._id;
        });

        if (idx != -1) {
            this.hhPlayer = this.players[idx];
        }
    }

    isCardSelected(suit: string, numb: number): boolean {
        const con = suit + numb;

        return this.cardsSelected[con];
    }

    cardClicked(suit: string, numb: number) {
        const con = suit + numb;
        let numberSelected = this.getNumberSelected();
        let proceed = false;

        if (!this.cardsSelected[con] && numberSelected < 5) {
            proceed = true;
            ++numberSelected;
        } else if (this.cardsSelected[con]) {
            proceed = true;
            --numberSelected;
        }

        if (proceed && !this.saving) {
            this.cardsSelected[con] = !this.cardsSelected[con];

            if (numberSelected == 5) {
                this.fiveSelected = true;

                if (this.hhPlayer) {
                    this.saveOrCreateHighHand();
                }
            } else {
                this.fiveSelected = false;
            }
        }
    }

    private getNumberSelected(): number {
        let numberSelected = 0;

        for (let prop in this.cardsSelected) {
            if (this.cardsSelected[prop] == true) {
                ++numberSelected;
            }
        }

        return numberSelected;
    }

    playerChanged() {
        const numberSelected = this.getNumberSelected();

        if (numberSelected == 5) {
            this.saveOrCreateHighHand();
        }
    }

    private saveOrCreateHighHand() {
        this.saving = true;

        let handString = '';
        let added = 0;

        for (let prop in this.cardsSelected) {
            if (this.cardsSelected[prop] == true) {
                handString += prop;
                ++added;

                if (added < 5) {
                    handString += ',';
                }
            }
        }

        if (added == 5 && this.hhPlayer != null) {
            // if (this.hand) {
            //     console.log('update');
            //     this.handService
            //         .updateHand(this.hand._id, this.gameId, this.hhPlayer._id, handString)
            //         .then(res => {
            //             this.saving = false;
            //         })
            //         .catch(err => {
            //             console.log('err updating', err);
            //         });
            // } else {
            this.handService
                .createHand(this.gameId, this.hhPlayer._id, handString)
                .then(res => {
                    this.saving = false;
                })
                .catch(err => {
                    console.log('err creating', err);
                });
            // }
        }
    }
}
