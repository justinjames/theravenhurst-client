import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditHighHand } from './edit-high-hand.component';

describe('EditHighHand', () => {
  let component: EditHighHand;
  let fixture: ComponentFixture<EditHighHand>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditHighHand ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditHighHand);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
