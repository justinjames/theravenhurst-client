import { Component, OnInit, Input } from '@angular/core'

import { DateService, Game, GameService, GameWinner, NavigationService, Player, PlayerService } from '../../core'

@Component({
    selector: 'raven-listed-game',
    templateUrl: './listed-game.component.html',
    styleUrls: ['./listed-game.component.scss']
})
export class ListedGameComponent implements OnInit {

    @Input() game : Game

    admin = false
    gamePot = '-'
    private winners : Player[] = []
    thisGamesWinners : GameWinner[] = []
    displayDateText = ''

    constructor(
        private dateService : DateService,
        private gameService : GameService,
        private playerService : PlayerService,
        private navigationService: NavigationService
    ) { }

    ngOnInit() {
        this.admin = this.playerService.player.admin
        this.setDateValue()
        this.determineGamePot()
        this.getWinnerNames()
    }

    gotoGame() {
        this.navigationService.goto(['games', this.game._id])
    }

    isWinnerMe() {

    }

    private setDateValue() {
        const myDate = new Date(this.game.date)
        this.displayDateText = this.dateService.addOrGetDateSuffix(myDate.getDate(), true)
    }

    private determineGamePot() {
        this.gamePot = this.gameService.determineGamePot(this.game)
    }

    private getWinnerNames() {
        this.winners = this.gameService.getGameWinners(this.game)
        this.thisGamesWinners.length = 0
        this.winners.forEach(w => {
            this.thisGamesWinners.push({
                first: w.first,
                last: w.last,
                name: this.playerService.getShortName(w),
                isMe: w._id == this.playerService.player._id
            })
        })

        if(this.thisGamesWinners.length == 1) {
            this.thisGamesWinners.push({})
        }
    }
}