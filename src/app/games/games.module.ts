import { NgModule } from '@angular/core'

import { CoreModule } from '../core'
import { ListedGameComponent } from './listed-game/listed-game.component'
import { GamesComponent } from './games.component'
import { GamesRouting } from './games.routing'
import { NewGameComponent } from './new-game/new-game.component'
import { GamePlayerComponent } from './game/game-player/game-player.component'
import { GameComponent } from './game/game.component'
import { EditPreGamePlayerComponent } from './edit-game/edit-pre-game-player/edit-pre-game-player.component'
import { EditInGamePlayerComponent } from './edit-game/edit-in-game-player/edit-in-game-player.component'
import { EditGameKnockoutPlayerComponent } from './edit-game/edit-game-knockout-player/edit-game-knockout-player.component'
import { EditGameKnockoutComponent } from './edit-game/edit-game-knockout/edit-game-knockout.component'
import { EditFinalizeGamePlayerComponent } from './edit-game/edit-finalize-game-player/edit-finalize-game-player.component'
import { EditHighHand } from './edit-game/edit-high-hand/edit-high-hand.component'
import { EditGameComponent } from './edit-game/edit-game.component';
import { EditGameManagePlayersComponent } from './edit-game/edit-game-manage-players/edit-game-manage-players.component'

@NgModule({
    imports: [
        CoreModule,
        GamesRouting
    ],
    declarations: [
        ListedGameComponent,
        GamesComponent,
        NewGameComponent,
        GamePlayerComponent,
        GameComponent,
        EditPreGamePlayerComponent,
        EditInGamePlayerComponent,
        EditGameKnockoutComponent,
        EditGameKnockoutPlayerComponent,
        EditFinalizeGamePlayerComponent,
        EditHighHand,
        EditGameComponent,
        EditGameManagePlayersComponent
    ]
})
export class GamesModule {}