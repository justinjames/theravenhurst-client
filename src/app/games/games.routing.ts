import { RouterModule } from "@angular/router";

import { AdminGuard, AuthenticatedGuard } from "../core";
import { GamesComponent } from "./games.component";
import { NewGameComponent } from "./new-game/new-game.component";
import { GameComponent } from "./game/game.component";
import { EditGameComponent } from "./edit-game/edit-game.component";
import { EditGameManagePlayersComponent } from "./edit-game/edit-game-manage-players/edit-game-manage-players.component";

export const GamesRouting = RouterModule.forChild([
    {
        path: "games",
        component: GamesComponent,
        canActivate: [AuthenticatedGuard]
    },
    {
        path: "games/new",
        component: NewGameComponent,
        canActivate: [AuthenticatedGuard, AdminGuard]
    },
    {
        path: "games/:gameId",
        component: GameComponent,
        canActivate: [AuthenticatedGuard]
    },
    {
        path: "games/:gameId/edit",
        component: EditGameComponent,
        canActivate: [AuthenticatedGuard, AdminGuard]
    },
    {
        path: "games/:gameId/edit/players",
        component: EditGameManagePlayersComponent,
        canActivate: [AuthenticatedGuard, AdminGuard]
    }
]);
