import { Component, OnInit } from '@angular/core'

import { BrowserTitleService, DateService, Game, GameService } from '../../core'

@Component({
    selector: 'raven-new-game',
    templateUrl: './new-game.component.html',
    styleUrls: ['./new-game.component.scss']
})
export class NewGameComponent implements OnInit {

    existingDates : Game[] = []
    existingDatesLoaded = false
    // newGameDate : Date
    // monthToShow : Date
    // curYear : number
    years : number[] = []
    year : number
    firstYear = 2017

    constructor(
        private browserTitleService : BrowserTitleService,
        private dateService : DateService,
        private gameService : GameService
    ) { }

    ngOnInit() {
        this.browserTitleService.title = 'New Game'
        // this.monthToShow = this.gameService.newGameDate
        // this.setCurYear()
        this.setupYears()
        this.getExistingDates()
    }

    private setupYears() {

        const now = new Date()
        let cur = now.getFullYear()

        while(cur >= this.firstYear) {
            this.years.push(cur--)
        }

        if(this.dateService.selectedYear) {
            this.year = this.dateService.selectedYear
        }else{
            this.year = this.years[0]
        }

        this.onYearChanged()
    }

    onYearChanged() {
        this.dateService.selectedYear = this.year
        // this.setupMonths()
        // this.loadGames()
    }

    // private setCurYear() {
    //     this.curYear = this.monthToShow.getFullYear()
    // }

    private getExistingDates() {
        this.gameService.getAllGameDates()
            .then(res => {
                this.existingDates = res
                this.existingDatesLoaded = true
            })
            .catch(err => {
                console.log(err)
            })
    }

    onDateChanged(d : Date) {
        console.log(d)
    }

}