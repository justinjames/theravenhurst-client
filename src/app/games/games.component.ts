import { Component, OnInit } from '@angular/core'

import * as XLSX from 'xlsx'

import { BrowserTitleService, DateService, Entry, Game, GamesMonth, GameService, Player, PlayerService } from '../core'

@Component({
    selector: 'raven-games',
    templateUrl: './games.component.html',
    styleUrls: ['./games.component.scss']
})
export class GamesComponent implements OnInit {

    months : GamesMonth[] = []
    // games : Game[]
    admin = false

    // now = new Date()
    // year = this.now.getFullYear()
    years : number[] = []
    year : number
    firstYear = 2017
    loading = true
    doingXlsx = false

    constructor(
        private browserTitleService : BrowserTitleService,
        private dateService : DateService,
        private gameService : GameService,
        private playerService : PlayerService
    ) {}

    ngOnInit() {
        this.browserTitleService.title = 'Games'
        this.admin = this.playerService.player.admin
        this.setupYears()
        // this.setupMonths()
        // this.subscribeToGames()
    }

    private setupYears() {

        const now = new Date()
        let cur = now.getFullYear()

        while(cur >= this.firstYear) {
            this.years.push(cur--)
        }

        if(this.dateService.selectedYear) {
            this.year = this.dateService.selectedYear
        }else{
            this.year = this.years[0]
        }

        this.onYearChanged()
    }

    // private subscribeToGames() {
    //     this.gameService.gamesSubject.subscribe(games => {
    //         this.months.forEach(m => {
    //             m.games = games.filter(g => g.m == m.m && g.y == m.y)
    //         })
    //     })
    // }

    onYearChanged() {
        this.dateService.selectedYear = this.year
        this.setupMonths()
        this.loadGames()
    }

    private setupMonths() {

        const now = new Date(this.year, 0, 1, 0, 0, 0, 0)

        this.months.length = 0

        while(now.getFullYear() == this.year) {

            this.months.push({
                month: this.dateService.getMonthText(now.getMonth()),
                monthShort: this.dateService.getMonthText(now.getMonth(), true),
                m: now.getMonth(),
                y: now.getFullYear(),
                games: []
            })

            now.setMonth(now.getMonth() + 1)
        }

        this.months.reverse()
    }

    private loadGames() {

        this.loading = true

        this.gameService.loadAllGames(this.year)
            .then(games => {

                games.sort((a : Game, b : Game) => {

                    let ad = new Date(a.date)
                    let bd = new Date(b.date)

                    return bd.getTime() - ad.getTime()
                })

                this.months.forEach(m => {
                    m.games = games.filter(g => g.m == m.m && g.y == m.y)
                })

                this.loading = false

            })
            .catch(err => {
                console.log(err)
            })
    }

    download() {

        this.doingXlsx = true

        const wb : XLSX.WorkBook = XLSX.utils.book_new()
        let data : (string|number)[][] = [
            // ['Game Date', 'Winner', 'Winner']
        ]

        this.months.forEach(m => {
            m.games.forEach(g => {

                const d = new Date(g.date)
                const gameWinners : Entry[] = g.entries.filter(e => e.pt)
                const arr : string[] = [d.toDateString()]

                gameWinners.forEach(w => {

                    const p : Player = w.p

                    arr.push(`${p.first} ${p.last}`)
                })

                data.push(arr)
            })
        })

        data = data.reverse()
        data.splice(0, 0, ['Game Date', 'Winner', 'Winner'])

        // console.log(data)

        const ws : XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(data)

        XLSX.utils.book_append_sheet(wb, ws, `${this.year}'s Games`)

        const now = new Date()
        const year = now.getFullYear()

        const month = now.getMonth() + 1
        const day = now.getDate()
        let hour = now.getHours()
        const min = now.getMinutes()

        const amPm = hour > 11 ? 'pm' : 'am'

        if(hour > 11) {
            hour -= 12
        }

        if(hour == 0) {
            hour = 12
        }

        const monthStr = month < 10 ? `0${month}` : String(month)
        const dayStr = day < 10 ? `0${day}` : String(day)
        const hourStr = hour < 10 ? `0${hour}` : String(hour)
        const minStr = min < 10 ? `0${min}` : String(min)

        const dateString = `${monthStr}-${dayStr}-${year} ${hourStr}-${minStr}-${amPm}`
        const fileName = `TheRavenhurst ${this.year}s Games-${dateString}`

        XLSX.writeFile(wb, `${fileName}.xlsx`)

        this.doingXlsx = false
    }
}