import { Component, OnInit } from '@angular/core';
import {
    style,
    state,
    animate,
    transition,
    trigger,
} from '@angular/animations';
import { Event, NavigationEnd, Router } from '@angular/router';

import { MenuItem } from './';
import { NavigationService } from '../../services/navigation.service';
import { PlayerService } from '../../services/player.service';

const menuSlideDist = '-260px';
const menuAniTime = 180;
const menuBreak = 700;

@Component({
    selector: 'raven-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
    animations: [
        trigger('menuState', [
            state(
                'inactive',
                style({
                    left: menuSlideDist,
                })
            ),
            state(
                'active',
                style({
                    left: 0,
                })
            ),
            transition(
                'inactive => active',
                animate(menuAniTime + 'ms ease-out')
            ),
            transition(
                'active => inactive',
                animate(menuAniTime + 'ms ease-in')
            ),
        ]),
    ],
})
export class MenuComponent implements OnInit {
    admin = false;
    showSlide = false;
    menuState = 'inactive';
    curPath = '';
    menuItems: MenuItem[] = [
        {
            name: 'Home',
            link: '',
            otherLink: 'announcements',
            newIcon: true,
        },
        {
            name: 'Leaderboard',
            link: 'leaderboard',
        },
        {
            name: 'Games',
            link: 'games',
            newIcon: true,
        },
        {
            name: 'High Hands',
            link: 'high-hands',
        },
        {
            name: 'Year in Review',
            link: 'year-in-review',
        },
        {
            name: 'Players',
            link: 'players',
            newIcon: true,
        },
        {
            name: 'Smack',
            link: 'smack',
        },
        {
            name: 'Profile',
            link: 'profile',
        },
        {
            name: 'Logout',
            link: 'logout',
        },
    ];

    constructor(
        private router: Router,
        private navigationService: NavigationService,
        private playerService: PlayerService
    ) {}

    ngOnInit() {
        this.checkIfAdmin();
        this.subscribeToRouter();
        this.onResize();
    }

    private checkIfAdmin() {
        this.admin = this.playerService.player.admin;
    }

    private subscribeToRouter() {
        this.router.events.subscribe((route: Event) => {
            if (route instanceof NavigationEnd) {
                const e = <NavigationEnd>route;
                const urlArray: string[] = e.url.split('/');
                this.curPath = urlArray[1];
            }
        });
    }

    onMenuClicked() {
        this.resetState();
    }

    menuTabClicked() {
        this.menuState = this.menuState == 'active' ? 'inactive' : 'active';
    }

    onResize() {
        this.showSlide = window.innerWidth <= menuBreak;
    }

    goHome() {
        this.navigationService.goto('');
        this.resetState();
    }

    private resetState() {
        if (this.menuState == 'active') {
            this.menuState = 'inactive';
        }
    }
}
