import { Component, Input, Output, EventEmitter } from '@angular/core'

import { MenuItem } from '../'
import { NavigationService } from '../../../services/navigation.service'
import { PlayerService } from '../../../services/player.service'

@Component({
    selector: 'raven-menu-item',
    templateUrl: './menu-item.component.html',
    styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent {

    @Input() menuItem : MenuItem
    @Input() curPath : string
    @Input() admin : boolean
    @Output() menuClicked = new EventEmitter<any>()

    constructor(
        private navigationService : NavigationService,
        private playerService : PlayerService
    ){}

    gotoLink(link : string) {
        if(link == 'logout') {
            return this.logout()
        }
        this.navigationService.goto(link)
        this.menuClicked.emit()
    }

    gotoNew(event : MouseEvent, link : string) {
        this.navigationService.goto([link, 'new'])
        this.menuClicked.emit()
        event.stopPropagation()
    }

    private logout() {
        this.playerService.logout()
            .then(response => {
                this.navigationService.goto('/login')
            })
            .catch(err => {
                console.log('err', err)
            })
    }
}