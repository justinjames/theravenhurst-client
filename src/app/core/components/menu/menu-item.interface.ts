export interface MenuItem {
    name : string
    link : string
    otherLink? : string
    newIcon? : boolean
}