import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighHandComponent } from './high-hand.component';

describe('HighHandComponent', () => {
  let component: HighHandComponent;
  let fixture: ComponentFixture<HighHandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighHandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighHandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
