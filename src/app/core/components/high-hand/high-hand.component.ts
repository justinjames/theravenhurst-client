import { Component, OnInit, Input } from '@angular/core';

import { Game, NgStyle, Player, SortableHand } from '../../interfaces';
import { HandService } from '../../services/hand.service';

@Component({
    selector: 'raven-high-hand',
    templateUrl: './high-hand.component.html',
    styleUrls: ['./high-hand.component.scss'],
})
export class HighHandComponent implements OnInit {
    @Input()
    hand: SortableHand;
    @Input()
    linkUpTwo: boolean;
    @Input()
    displayHand: boolean;
    @Input()
    needsSorting: boolean;
    @Input()
    hideSmallText: boolean;
    @Input()
    maxWidth: number;

    cards: string[] = [];
    handDate: Date;
    extraStyle: NgStyle = {};

    constructor(private handService: HandService) {}

    ngOnInit() {
        if (this.needsSorting) {
            this.hand.hand = this.handService.sortHand(this.hand.hand);
        }

        if (this.maxWidth) {
            this.extraStyle['max-width'] = `${this.maxWidth}px`;
        }

        this.cards = this.hand.hand;
        const handGame = <Game>this.hand.g;
        this.handDate = new Date(handGame.y, handGame.m, handGame.d);
    }

    getCardPath(numb: boolean, card: string): string {
        let path: string;
        const n = Number(card.slice(1, card.length));
        const s = card.charAt(0);
        const red = s == 'h' || s == 'd';

        if (numb) {
            path = `/assets/cards/${red ? 'red' : 'black'}/${n}.svg`;
        } else {
            path = `/assets/cards/suits/${s}.svg`;
        }

        return path;
    }

    getPlayerLink(p: Player): string {
        const id = p._id;

        if (this.linkUpTwo) {
            return `../../players/${id}`;
        } else {
            return `../players/${id}`;
        }
    }

    getGameLink(g: Game): string {
        const id = g._id;

        return `../games/${id}`;
    }
}
