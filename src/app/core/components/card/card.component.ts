import {
    Component,
    Input,
    // Output,
    // EventEmitter,
    AfterViewInit,
} from '@angular/core';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';

import { NgStyle } from '../../';

@Component({
    selector: 'raven-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss'],
})
export class CardComponent implements AfterViewInit {
    @Input() image: string;
    @Input() round: boolean;
    @Input() faded: boolean = false;

    myImageId = 'img' + Math.round(Math.random() * 2000).toString();
    myContId = 'cont' + Math.round(Math.random() * 2000).toString();
    imageSet = false;
    useWidth = false;
    imgStyle: NgStyle = {};

    private weHaveResized: Subject<string> = new Subject<string>();
    private debounceResizeTime = 400;
    private ratio = 1.34;

    ngAfterViewInit() {
        this.subscribeToResize();
    }

    private subscribeToResize() {
        this.weHaveResized
            .debounceTime(this.debounceResizeTime)
            .subscribe(m => {
                // this.checkIfResizeAffectedAnything()
                this.imageLoaded();
            });
    }

    imageLoaded() {
        const img = document.getElementById(this.myImageId);
        const width = img.clientWidth;
        const height = img.clientHeight;
        const container = document.getElementById(this.myContId);
        const containerHeight = img.clientHeight;
        const containerWidth = containerHeight / this.ratio;
        let diff: number;

        this.useWidth = height > width * this.ratio;
        this.imageSet = true;

        if (!this.useWidth) {
            diff = Math.round((containerWidth - width) / 2);
            this.imgStyle.left = diff + 'px';
        }
    }

    onResize() {
        this.weHaveResized.next('a');
    }
}
