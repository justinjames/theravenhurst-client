import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YearPotComponent } from './year-pot.component';

describe('YearPotComponent', () => {
  let component: YearPotComponent;
  let fixture: ComponentFixture<YearPotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YearPotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YearPotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
