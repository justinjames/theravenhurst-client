import {
    Component,
    // Input,
    // Output,
    // EventEmitter,
    AfterViewInit
} from "@angular/core";
import { Subject } from "rxjs/Subject";
import "rxjs/add/operator/debounceTime";

import { NgStyle } from "../../";
import { GameService } from "../../services/game.service";

@Component({
    selector: "raven-year-pot",
    templateUrl: "./year-pot.component.html",
    styleUrls: ["./year-pot.component.scss"]
})
export class YearPotComponent implements AfterViewInit {
    private weHaveResized: Subject<string> = new Subject<string>();
    private debounceResizeTime = 400;
    private ratio = 1.34;

    images = {
        a: null,
        b: null,
        c: null,
        d: null
    };

    constructor(private gameService: GameService) {}

    ngAfterViewInit() {
        this.loadCount();
    }

    private loadCount() {
        this.gameService
            .loadYearCount()
            .then(c => {
                const tot: number = c;
                const cash = tot * 20;
                const cashString = String(cash);

                if (cash == 0) {
                    this.images.a = null;
                    this.images.b = null;
                    this.images.c = null;
                    this.images.d = null;
                } else if (cashString.length == 2) {
                    this.images.a = `${cashString.charAt(1)}.svg`;
                    this.images.b = `${cashString.charAt(0)}.svg`;
                    this.images.c = null;
                    this.images.d = null;
                } else if (cashString.length == 3) {
                    this.images.a = `${cashString.charAt(2)}.svg`;
                    this.images.b = `${cashString.charAt(1)}.svg`;
                    this.images.c = `${cashString.charAt(0)}.svg`;
                    this.images.d = null;
                } else {
                    this.images.a = `${cashString.charAt(3)}.svg`;
                    this.images.b = `${cashString.charAt(2)}.svg`;
                    this.images.c = `${cashString.charAt(1)}.svg`;
                    this.images.d = `${cashString.charAt(0)}.svg`;
                }
            })
            .catch(err => {
                console.log(err);
            });
    }
}
