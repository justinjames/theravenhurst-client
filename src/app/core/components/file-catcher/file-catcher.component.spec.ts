import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileCatcherComponent } from './file-catcher.component';

describe('FileCatcherComponent', () => {
  let component: FileCatcherComponent;
  let fixture: ComponentFixture<FileCatcherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileCatcherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileCatcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
