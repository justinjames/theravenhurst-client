import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'


@Component({
    selector: 'raven-file-catcher',
    template: `<div class="box" [ngClass]="{mouseOver : mouseOver}" id="FileCatcher"></div>`,
    styles: [`
        .box{
        	position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            border-radius: 50%;
        }
        .mouseOver {
            background-color: lightblue;
            opacity: .6;
            border: 2px solid #9e0000;
        }
    `]
})
export class FileCatcherComponent implements OnInit {

    @Input() acceptedType : string
    @Input() onlyOneFile : boolean
    @Output() receiveFile = new EventEmitter<File>()

    mouseOver = false

    constructor() {}

    ngOnInit() {
        this.setupFileReader()
    }

    private setupFileReader() {
        if((<any>window).FileReader) {

            const drop = document.getElementById('FileCatcher')

            this.addEventHandler(drop, 'dragover', this.cancel)
            this.addEventHandler(drop, 'dragenter', this.cancel)
            this.addEventHandler(drop, 'dragenter', this.over)
            this.addEventHandler(drop, 'dragleave', this.out)
            this.addEventHandler(drop, 'drop', this.drop)
        }
    }

    private addEventHandler(obj : Object, evt : string, handler : any) {
        (<any>obj).addEventListener(evt, handler, false)
    }

    private cancel(e : DragEvent) {
        if (e.preventDefault) {
            e.preventDefault()
        }
        return false
    }

    private over = (e : DragEvent) => {
        this.mouseOver = true
    }

    private out = (e : DragEvent) => {
        this.mouseOver = false
    }

    private drop = (e : DragEvent) => {

        this.mouseOver = false

        if (e.preventDefault) {
            e.preventDefault()
        }

        const dataTransfer = e.dataTransfer
        const files = dataTransfer.files

        if(this.onlyOneFile) {

            if(files.length > 1) {
                return /*this.headerMessageService.message = {
                    message : 'Too many files. Please drag one file only.'
                }*/
            }

            const myFile : File = files[0]

            if(myFile.type.indexOf(this.acceptedType) == -1) {
                return /*this.headerMessageService.message = {
                    message : 'Invalid file type.'
                }*/
            }

            this.receiveFile.emit(myFile)
        }
    }
}
