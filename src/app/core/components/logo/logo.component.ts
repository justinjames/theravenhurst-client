import { Component, Input } from '@angular/core'

@Component({
    selector: 'raven-logo',
    templateUrl: './logo.component.html',
    styleUrls: ['./logo.component.scss']
})
export class LogoComponent {
    @Input() chipOnly = false
}