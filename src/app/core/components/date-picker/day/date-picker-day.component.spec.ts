import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatePickerDayComponent } from './date-picker-day.component';

describe('DatePickerDayComponent', () => {
  let component: DatePickerDayComponent;
  let fixture: ComponentFixture<DatePickerDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatePickerDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatePickerDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
