import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'

import { DatePickerDate, Game } from '../../../interfaces'
import { DateService } from '../../../services'

@Component({
    selector: 'raven-date-picker-day',
    templateUrl: './date-picker-day.component.html',
    styleUrls: ['./date-picker-day.component.scss']
})
export class DatePickerDayComponent implements OnInit {

    @Input() myDate : DatePickerDate
    @Input() existingDates : Game[]
    @Input() creatingGame : boolean
    @Output() dateChosen = new EventEmitter<DatePickerDate>()

    hasGame = false
    myText = ''
    // myDate : Date

    constructor(
        private dateService : DateService
    ) {}

    ngOnInit() {
        this.determineHasGame()
        this.setDateText()
    }

    private determineHasGame() {
        if(this.existingDates.length && this.myDate.day) {
            this.existingDates.forEach(d => {

                const tempDate = new Date(d.date)

                if(tempDate.getDate() == this.myDate.day && tempDate.getMonth() == this.myDate.month) {
                    this.hasGame = true
                }
            })
        }
    }

    private setDateText() {
        this.myText = this.dateService.addOrGetDateSuffix(this.myDate.day, false)
    }

    selectDate() {
        if(!this.hasGame && !this.creatingGame) {
            this.dateChosen.emit(this.myDate)
        }
    }
}