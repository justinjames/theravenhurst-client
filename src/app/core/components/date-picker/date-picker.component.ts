import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core'

import { DatePickerDate, Game } from '../../interfaces'
import { GameService, NavigationService } from '../../services'

@Component({
    selector: 'raven-date-picker',
    templateUrl: './date-picker.component.html',
    styleUrls: ['./date-picker.component.scss']
})
export class DatePickerComponent implements OnInit, OnChanges {

    // @Input() monthToShow : Date
    @Input() year : number
    @Input() existingDates : Game[] = []

    months : DatePickerDate[] = []
    mostSaturdays = 0
    // curYear : number
    creatingGame = false

    constructor(
        private gameService : GameService,
        private navigationService : NavigationService
    ) {}

    ngOnInit() {
        // this.setCurYear()
        // this.determineMostSatsInMonth()
        // this.loadExistingGame()
    }

    ngOnChanges(ch : SimpleChanges) {
        // console.log(ch)
        if(ch.year && ch.year.currentValue != ch.year.previousValue) {
            this.determineMostSatsInMonth()
        }
    }

    // private setCurYear() {
    //     this.curYear = this.monthToShow.getFullYear()
    // }

    private determineMostSatsInMonth() {

        let d = new Date(this.year, 0, 1, 0, 0, 0, 0)
        let count = 0
        let curMonth = 0

        if(d.getDay() < 6) {
            d.setDate(d.getDate() + (6 - d.getDay()))
        }

        while(d.getFullYear() == this.year) {

            if(d.getMonth() != curMonth) {

                if(count > this.mostSaturdays) {
                    this.mostSaturdays = count
                }

                count = 1
                ++curMonth

            }else{
                ++count
            }

            d.setDate(d.getDate() + 7)
        }

        this.initalizeCalendar()
    }

    // private loadExistingGame() {
    //     this.gameService.getAllGameDates()
    //         .then(response => {

    //             if(response.length) {
    //                 this.existingDates = response.map(ed => new Date(ed.date))
    //             }

    //             this.initalizeCalendar()

    //         })
    //         .catch(err => {
    //             console.log(err)
    //         })
    // }

    private initalizeCalendar() {

        this.months.length = 0

        let m = 0

        while(m < 12) {

            this.months.push({
                month: m++,
                year: this.year
            })
        }
    }

    onDateChosen(incomingDate : DatePickerDate) {

        this.creatingGame = true

        incomingDate.date = new Date(incomingDate.year, incomingDate.month, incomingDate.day, 12, 0, 0, 0)

        this.gameService.createNewGame(incomingDate)
            .then(response => {
                // console.log(response)
                this.navigationService.goto(['games', response._id, 'edit'])
            })
            .catch(err => {
                console.log(err)
            })
    }
}