import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'

import { DatePickerDate, Game } from '../../../interfaces'
import { DateService } from '../../../services/date.service'

@Component({
    selector: 'raven-date-picker-month',
    templateUrl: './date-picker-month.component.html',
    styleUrls: ['./date-picker-month.component.scss']
})
export class DatePickerMonthComponent implements OnInit {

    @Input() month : DatePickerDate
    @Input() existingDates : Game[]
    @Input() mostSaturdays : number
    @Input() creatingGame : boolean
    @Output() dateChosen = new EventEmitter<DatePickerDate>()

    days : DatePickerDate[] = []
    monthText : string

    constructor(
        private dateService : DateService
    ) {}

    ngOnInit() {
        this.monthText = this.dateService.getMonthText(this.month.month)
        this.initalizeMonth()
    }

    private initalizeMonth() {

        let d = new Date(this.month.year, this.month.month, 1, 0, 0, 0, 0)

        if(d.getDay() < 6) {
            d.setDate(d.getDate() + (6 - d.getDay()))
        }

        while(d.getMonth() == this.month.month) {
            this.addDay(d)
            d.setDate(d.getDate() + 7)
        }

        while(this.days.length < this.mostSaturdays) {
            this.days.push({})
        }
    }

    private addDay(d : Date) {
        this.days.push({
            day: d.getDate(),
            month: d.getMonth(),
            year: d.getFullYear()
        })
    }

    onDateChosen(incomingDate : DatePickerDate) {
        this.dateChosen.emit(incomingDate)
    }
}