import {
    Component,
    Input,
    Output,
    EventEmitter,
    AfterViewInit,
} from '@angular/core';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';

import { NgStyle } from '../../';

@Component({
    selector: 'raven-avatar',
    templateUrl: './avatar.component.html',
    styleUrls: ['./avatar.component.scss'],
})
export class AvatarComponent implements AfterViewInit {
    @Input() image: string;
    @Input() round: boolean;
    @Input() faded: boolean = false;

    @Input() acceptPhoto: boolean;
    @Input() uploadingImage: boolean;
    @Output() onReceiveImage = new EventEmitter<File>();

    myImageId = 'img' + Math.round(Math.random() * 2000).toString();
    myContId = 'cont' + Math.round(Math.random() * 2000).toString();
    imageSet = false;
    imageTaller = false;
    imgStyle: NgStyle = {};

    private weHaveResized: Subject<string> = new Subject<string>();
    private debounceResizeTime = 400;

    ngAfterViewInit() {
        this.subscribeToResize();
    }

    private subscribeToResize() {
        this.weHaveResized
            .debounceTime(this.debounceResizeTime)
            .subscribe(m => {
                // this.checkIfResizeAffectedAnything()
                this.imageLoaded();
            });
    }

    imageLoaded() {
        let img = document.getElementById(this.myImageId);
        let width = img.clientWidth;
        let height = img.clientHeight;
        let container = document.getElementById(this.myContId);
        let containerHeight = img.clientHeight;
        let diff: number;

        this.imageTaller = height > width && height != width;
        this.imageSet = true;

        if (!this.imageTaller) {
            diff = Math.round((containerHeight - width) / 2);
            this.imgStyle.left = diff + 'px';
        }
    }

    onReceiveFile(f: File) {
        this.onReceiveImage.emit(f);
    }

    onResize() {
        this.weHaveResized.next('a');
    }
}
