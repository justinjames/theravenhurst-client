import { Component, OnInit } from '@angular/core'

import { Player } from '../../'
import { PlayerService } from '../../services/player.service'

@Component({
    selector: 'raven-player-badge',
    templateUrl: './player-badge.component.html',
    styleUrls: ['./player-badge.component.scss']
})
export class PlayerBadgeComponent implements OnInit {

    player : Player

    constructor(
        private playerService : PlayerService
    ) {}

    ngOnInit() {
        this.player = this.playerService.player
    }
}