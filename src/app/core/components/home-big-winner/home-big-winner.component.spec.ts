import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeBigWinnerComponent } from './home-big-winner.component';

describe('HomeBigWinnerComponent', () => {
  let component: HomeBigWinnerComponent;
  let fixture: ComponentFixture<HomeBigWinnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeBigWinnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeBigWinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
