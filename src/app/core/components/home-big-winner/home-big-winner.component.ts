import { Component, OnInit, Input } from '@angular/core';

import { Entry } from '../../interfaces/entry.interface';
import { PlayerService } from '../../services/player.service';

@Component({
    selector: 'raven-home-big-winner',
    templateUrl: './home-big-winner.component.html',
    styleUrls: ['./home-big-winner.component.scss'],
})
export class HomeBigWinnerComponent implements OnInit {
    @Input()
    entry: Entry;

    extraName = '';

    constructor(
        // private gameService : GameService
        private playerService: PlayerService,
    ) {}

    ngOnInit() {
        this.getExtraName();
    }

    getExtraName() {
        this.extraName = this.playerService.getExtraName(this.entry.p);
    }
}
