import { Injectable } from '@angular/core';
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';

import { NavigationService } from '../services/navigation.service';
import { PlayerService } from '../services/player.service';

@Injectable()
export class AuthenticatedGuard implements CanActivate {
    constructor(
        private playerService: PlayerService,
        private navigationService: NavigationService
    ) {}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        if (this.playerService.player) {
            return true;
        } else {
            this.navigationService.loginRoute = next;
            this.navigationService.goto('/login');
            return false;
        }
    }
}
