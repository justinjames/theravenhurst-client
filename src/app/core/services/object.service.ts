import { Injectable } from '@angular/core'

@Injectable()
export class ObjectService {

    getObjectPropValue(obj : any, prop : string) : any {

        var val = obj
        var arr = prop.split('.')

        while(arr.length) {
            val = val[arr.shift()]
        }

        return val
    }

    stripProperties(obj : any, propertiesToKeep : string) : any {

        let ret = {}
        let arr = propertiesToKeep.split(',')

        while(arr.length) {
            let prop = arr.pop()
            ret[prop] = obj[prop]
        }

        return ret
    }

}