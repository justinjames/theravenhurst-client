import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/toPromise';

import { APP_CONFIG, AppConfigInterface } from '../../config';
import { ErrorService } from './error.service'; // do this to avoid circular dependency error
// import { PlayerService } from './player.service'
import { ArrayService } from './array.service';
import {
    DatePickerDate,
    Game,
    LatestResults,
    Player,
    YearCount,
    YearsHolder,
    YearInReviewGame,
} from '../';

@Injectable()
export class GameService {
    private apiPrefix = this.config.apiEndpoint + '/games';
    // private _newGameDate = new Date()

    private _gameDatesAndIds: Game[] = [];
    gameDatesAndIdsSubject = new BehaviorSubject<Game[]>(this._gameDatesAndIds);

    _latestResults: LatestResults;
    latestResultsSubject = new BehaviorSubject<LatestResults>(this._latestResults);

    private cached: YearsHolder = {};
    // private gameIdsAndDates : Game[] = []

    constructor(
        private http: Http,
        private arrayService: ArrayService,
        private errorService: ErrorService,
        // private playerService : PlayerService,
        @Inject(APP_CONFIG) private config: AppConfigInterface,
    ) { }

    init() {
        // this.initializeNewGameDate()
        // this.loadAllGames()
        this.loadLatestResults();
        this.getAllGameDatesAndIds();
    }

    private replaceGameTextSlashes(s): string {
        return s.replace(/\//g, '<br><br>')
    }

    // private initializeNewGameDate() {
    //     this._newGameDate.setDate(1)
    //     this._newGameDate.setHours(12, 0, 0, 0)
    // }

    // public get newGameDate() : Date {
    //     return this._newGameDate
    // }

    // public set newGameDate(d : Date) {
    //     this._newGameDate = d
    // }

    // loadAllGames() {

    //     let path = this.apiPrefix

    //     this.http.get(path)
    //         .toPromise()
    //         .then(response => {
    //             this._games = <Game[]>response.json()
    //             this.sortGamesAndCallNext(false)
    //         })
    //         .catch(this.errorService.handleError)
    // }

    loadAllGames(year: number): Promise<Game[]> {
        if (this.cached[year]) {
            const now = new Date();
            const then = this.cached[year].date;
            const diff = now.getTime() - then.getTime();
            const diffInMinutes = diff / 1000 / 60;

            if (diffInMinutes < 60) {
                return new Promise((resolve, reject) => {
                    resolve(this.cached[year].games);
                });
            }
        }

        const path = this.apiPrefix + '/all/' + year;

        return this.http
            .get(path)
            .toPromise()
            .then(response => {
                const games = <Game[]>response.json();

                // this.addGamesToFullListOfIds(games)

                if (!this.cached[year]) {
                    this.cached[year] = {
                        date: new Date(),
                        games,
                    };
                }

                return games;
            })
            .catch(this.errorService.handleError);
    }

    // private addGamesToFullListOfIds(games : Game[]) {

    //     let addedGames = false

    //     games.forEach(g => {

    //         const idx = this.gameIdsAndDates.findIndex(gm => gm._id == g._id)

    //         if(idx == -1) {

    //             addedGames = true

    //             this.gameIdsAndDates.push({
    //                 date: g.date,
    //                 _id: g._id
    //             })
    //         }
    //     })

    //     if(addedGames) {
    //         this.gameIdsAndDates = this.gameIdsAndDates.sort((a, b) => {

    //             const ad = new Date(a.date)
    //             const bd = new Date(b.date)

    //             return ad.getTime() - bd.getTime()
    //         })
    //     }
    // }

    // getNeighborGameId(_id : string, prev : boolean) : Promise<string> {

    //     // const path = this.apiPrefix + '/count/' + _id
    //     const path = `${this.apiPrefix}/neighbor/${_id}/${prev}`

    //     // console.log(path)

    //     return this.http.get(path)
    //         .toPromise()
    //         .then(response => {

    //             const game : Game = <Game>response.json()

    //             return game._id
    //         })
    //         .catch(this.errorService.handleError)
    // }

    // getNeighborGameIds(_id : string, yearOfIncGame : number) : Game[] {

    //     const idx = this.gameIdsAndDates.findIndex(gm => gm._id == _id)
    //     const min = idx - 1
    //     const plus = idx + 1
    //     const ret = []

    //     let missed = 0

    //     console.log(_id)
    //     console.log(idx, min, plus)
    //     console.log(this.gameIdsAndDates)

    //     if(min > 0) {
    //         ret.push(this.gameIdsAndDates[min])
    //     }else{
    //         ++missed
    //         ret.push({})
    //     }

    //     if(plus < this.gameIdsAndDates.length) {
    //         ret.push(this.gameIdsAndDates[plus])
    //     }else{
    //         ++missed
    //         ret.push({})
    //     }

    //     if(missed == 2) {
    //         this.loadAllGames(yearOfIncGame)
    //     }

    //     return ret
    // }

    loadYearCount(): Promise<number> {
        const now = new Date();
        const year = now.getFullYear();
        const path = this.apiPrefix + '/count/' + year;

        return this.http
            .get(path)
            .toPromise()
            .then(response => {
                const yc = <YearCount>response.json();
                return yc.count;
            })
            .catch(this.errorService.handleError);
    }

    loadLatestResults() {
        let path = this.apiPrefix + '/latest';

        this.http
            .get(path)
            .toPromise()
            .then(response => {
                this._latestResults = <LatestResults>response.json();
                // console.log(this._latestResults)
                if (this._latestResults.game.n) {
                    this._latestResults.game.n = this.replaceGameTextSlashes(this._latestResults.game.n)
                }

                this._latestResults.entries = this.arrayService.sortByObjectProperty(
                    this._latestResults.entries,
                    'ks,p.first',
                    false,
                );
                this.latestResultsSubject.next(this._latestResults);
            })
            .catch(this.errorService.handleError);
    }

    checkIfGameIsLatestAndTriggerUpdate(gameId: string): void {
        if (gameId == this._latestResults.game._id) {
            this.loadLatestResults();
        }
    }

    getAllGameDates(): Promise<Game[]> {
        let path = this.apiPrefix + '/dates';

        return this.http
            .get(path)
            .toPromise()
            .then(response => {
                return <Game[]>response.json();
            })
            .catch(this.errorService.handleError);
    }

    private getAllGameDatesAndIds() {
        let path = this.apiPrefix + '/datesAndIds';

        this.http
            .get(path)
            .toPromise()
            .then(response => {
                // return <Game[]>response.json()
                this._gameDatesAndIds = <Game[]>response.json();
                this.gameDatesAndIdsSubject.next(this._gameDatesAndIds);
            })
            .catch(this.errorService.handleError);
    }

    createNewGame(gameDate: DatePickerDate): Promise<Game> {
        let path = this.apiPrefix;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http
            .post(path, gameDate, options)
            .toPromise()
            .then(response => {
                let res = <Game>response.json();
                // this._games.push(res)
                // this.sortGamesAndCallNext(false)

                return res;
            })
            .catch(this.errorService.handleError);
    }

    loadGame(gameId: string, forEdit: boolean): Promise<Game> {
        let path = this.apiPrefix + '/game/' + gameId;

        return this.http
            .get(path)
            .toPromise()
            .then(response => {
                let g: Game = <Game>response.json();

                if (g.n && !forEdit) {
                    g.n = this.replaceGameTextSlashes(g.n)
                }

                if (!g.entries) {
                    g.entries = [];
                }
                return g;
            })
            .catch(this.errorService.handleError);
    }

    saveGame(game: Game): Promise<Game> {
        let path = this.apiPrefix + '/game/' + game._id;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http
            .post(path, game, options)
            .toPromise()
            .then(response => {
                const g = <Game>response.json();

                if (g.s == 3 && g._id == this._latestResults.game._id) {
                    this.loadLatestResults();
                }

                return g;
            })
            .catch(this.errorService.handleError);
    }

    advanceGameState(gameId: string): Promise<Game> {
        let path = this.apiPrefix + '/game/' + gameId + '/state';

        return this.http
            .get(path)
            .toPromise()
            .then(response => {
                const g = <Game>response.json();

                if (g.s == 3) {
                    this.loadLatestResults();
                }

                return g;
            })
            .catch(this.errorService.handleError);
    }

    loadYearReview(y: number): Promise<YearInReviewGame[]> {
        const path = this.apiPrefix + '/review/' + y;

        return this.http
            .get(path)
            .toPromise()
            .then(response => {
                const review = <YearInReviewGame[]>response.json();

                review.forEach(r => {
                    // console.log(r)
                    if (r.n) {
                        r.n = this.replaceGameTextSlashes(r.n)
                    }

                    r.date = new Date(r.y, r.m, r.d);
                });

                return review;
            })
            .catch(this.errorService.handleError);
    }

    determineGamePot(g: Game): string {
        let tot = 0;
        let cashes = 0;

        if (g.entries) {
            /*if(g.entries[0].q == 0) {

                g.entries.forEach(e => {
                    tot += e.c
                })

            }else{

                g.entries.forEach(e => {
                    tot += 20 * e.b
                })

                if(tot > 0) {
                    tot -= 20
                }
            }*/

            g.entries.forEach(e => {
                cashes += e.c;
                tot += 20 * e.b;
            });

            if (tot > 0) {
                tot -= 20;
            }
        }

        if (cashes > tot) {
            tot = cashes;
        }

        return tot == 0 ? '-' : `$${tot}`;
    }

    getGameWinners(g: Game): Player[] {
        let winners: Player[] = [];
        // let ret : string[] = []

        g.entries.forEach(e => {
            if (e.pt) {
                winners.push(e.p);
            }
        });

        // console.log(winners)

        return winners;
    }

    // private sortGamesAndCallNext(skipSort : boolean) {

    // if(!skipSort) {
    //     this._games.sort((a : Game, b : Game) => {

    //         let ad = new Date(a.date)
    //         let bd = new Date(b.date)

    //         return bd.getTime() - ad.getTime()
    //     })
    // }

    // this.gamesSubject.next(this._games)
    // }

    // private updateGameInGames(g : Game) {

    //     const idx = this._games.findIndex(gm => gm._id == g._id)

    //     if(idx != -1) {
    //         this._games[idx] = g
    //     }

    //     this.sortGamesAndCallNext(true)
    // }
}

/*
gamesRouter.post('/', auth.isAdmin, (req, res) => {
    Games.create(req.body, callback(res))
})

gamesRouter.post('/:gameId/changeDate', auth.isAdmin, (req, res) => {
    Games.changeDate(req.params.gameId, req.body, callback(res))
})

gamesRouter.post('/:gameId/addPlayer', auth.isAdmin, (req, res) => {
    Games.addPlayer(req.params.gameId, req.body._id, callback(res))
})

gamesRouter.post('/:gameId/removePlayer', auth.isAdmin, (req, res) => {
    Games.removePlayer(req.params.gameId, req.body._id, callback(res))
})

gamesRouter.get('/', auth.isLoggedIn, (req, res) => {
    Games.readAll(callback(res))
})

gamesRouter.get('/:gameId', auth.isLoggedIn, (req, res) => {
    Games.readGame(req.params.gameId, callback(res))
})
*/
