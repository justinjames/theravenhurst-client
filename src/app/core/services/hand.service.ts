import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
// import { BehaviorSubject } from 'rxjs'
import 'rxjs/add/operator/toPromise';

import { APP_CONFIG, AppConfigInterface } from '../../config';
import { ErrorService } from './error.service'; // do this to avoid circular dependency error
import { GameService } from './game.service';
import { Game, Hand, SortableHand, Player } from '../interfaces';

@Injectable()
export class HandService {
    private apiPrefix = this.config.apiEndpoint + '/hands';

    private types: any = {
        straightFlush: {
            rank: 1,
            sort: (a: string[], b: string[]): number => {
                return (
                    <number>this.getStraightHighestCard(b, false) -
                    <number>this.getStraightHighestCard(a, false)
                );
            },
            display: (h: string[]): string => {
                const val = this.getStraightHighestCard(h, true);

                if (val == 'Ace') {
                    return 'Royal Flush';
                }

                return `${val} High Straight Flush`;
            },
            sortCardsForDisplay: (h: string[]): string[] => this.straightSort(h),
        },
        fourKind: {
            rank: 2,
            sort: (a: string[], b: string[]): number => {
                const av = <number>this.getFourOfKindVal(a, false);
                const bv = <number>this.getFourOfKindVal(b, false);

                // console.logS

                if (av != bv) {
                    return bv - av;
                } else {
                    // console.log(this.getFourKindExtra(a), this.getFourKindExtra(b));
                    return this.getFourKindExtra(b) - this.getFourKindExtra(a);
                }
            },
            display: (h: string[]): string => {
                const high = this.fixWordForPlural(<string>this.getFourOfKindVal(h, true));
                return `Quad ${high}`;
            },
            sortCardsForDisplay: (h: string[]): string[] => {
                return this.sortFourOfKind(h);
            },
        },
        fullHouse: {
            rank: 3,
            sort: (a: string[], b: string[]): number => {
                let aval = <number>this.getFullHouseVal(a, true, false);
                let bval = <number>this.getFullHouseVal(b, true, false);

                if (aval == bval) {
                    aval = <number>this.getFullHouseVal(a, false, false);
                    bval = <number>this.getFullHouseVal(b, false, false);
                }

                return bval - aval;
            },
            display: (h: string[]): string => {
                const high = this.fixWordForPlural(<string>this.getFullHouseVal(h, true, true));
                const low = this.fixWordForPlural(<string>this.getFullHouseVal(h, false, true));

                return `${high} Over ${low}`;
            },
            sortCardsForDisplay: (h: string[]): string[] => {
                const hv = this.getFullHouseVal(h, true, false);
                const lv = this.getFullHouseVal(h, false, false);
                const high = this.fixWordForPlural(<string>hv);
                const low = this.fixWordForPlural(<string>lv);
                const top = h.filter(c => this.cardValue(c, false, false) == hv);
                const bot = h.filter(c => this.cardValue(c, false, false) == lv);

                return [...top, ...bot];
            },
        },
        flush: {
            rank: 4,
            sort: (a: string[], b: string[]): number => {
                return this.sortNoPair(a, b);
            },
            display: (h: string[]): string => {
                h = this.sortHandNumerically(h);

                const high = this.cardValue(h[4], true, true);
                return `${high} High Flush`;
            },
            sortCardsForDisplay: (h: string[]): string[] => {
                h = this.sortHandNumerically(h);

                return h.reverse();
            },
        },
        straight: {
            rank: 5,
            sort: (a: string[], b: string[]): number => {
                return (
                    <number>this.getStraightHighestCard(b, false) -
                    <number>this.getStraightHighestCard(a, false)
                );
            },
            display: (h: string[]): string => {
                const high = this.getStraightHighestCard(h, true);
                return `${high} High Straight`;
            },
            sortCardsForDisplay: (h: string[]): string[] => this.straightSort(h),
        },
        threeKind: {
            rank: 6,
            sort: (a: string[], b: string[]): number => {
                let av = <number>this.getThreeOfKindVal(a, false);
                let bv = <number>this.getThreeOfKindVal(b, false);

                if (av != bv) {
                    return bv - av;
                } else {
                    const remA = this.getThreeKindRemainingCards(a, false);
                    const remB = this.getThreeKindRemainingCards(b, false);

                    return this.sortNoPair(remA, remB);
                }
            },
            display: (h: string[]): string => {
                return `Three ${this.fixWordForPlural(<string>this.getThreeOfKindVal(h, true))}`;
            },
            sortCardsForDisplay: (h: string[]): string[] => {
                let sorted = [];
                let tok = <number>this.getThreeOfKindVal(h, false);

                h = this.sortHandNumerically(h);

                let rem = this.getThreeKindRemainingCards(h, true);

                h.forEach(c => {
                    if (Number(c.slice(1, c.length)) == tok) {
                        sorted.push(c);
                    }
                });

                sorted = sorted.concat(rem);

                return sorted;
            },
        },
        twoPair: {
            rank: 7,
            sort: (a: string[], b: string[]): number => {
                let aval = <number>this.getTwoPairVal(a, true, false, false);
                let bval = <number>this.getTwoPairVal(b, true, false, false);

                if (aval == bval) {
                    aval = <number>this.getTwoPairVal(a, false, false, false);
                    bval = <number>this.getTwoPairVal(b, false, false, false);
                }

                if (aval == bval) {
                    aval = <number>this.getTwoPairVal(a, false, true, false);
                    bval = <number>this.getTwoPairVal(b, false, true, false);
                }

                return bval - aval;
            },
            display: (h: string[]): string => {
                const hp = this.fixWordForPlural(<string>this.getTwoPairVal(h, true, false, true));
                const lp = this.fixWordForPlural(<string>this.getTwoPairVal(h, false, false, true));

                return `Two Pair ${hp} & ${lp}`;
            },
            sortCardsForDisplay: (h: string[]): string[] => {
                const hp = this.getTwoPairVal(h, true, false, false);
                const lp = this.getTwoPairVal(h, false, false, false);
                const high = h.filter(c => this.cardValue(c, false, false) == hp);
                const low = h.filter(c => this.cardValue(c, false, false) == lp);
                const other = this.getTwoPairVal(h, false, true, false);

                let otherCard;

                h.forEach(c => {
                    if (this.cardValue(c, false, false) == other) {
                        otherCard = c;
                    }
                });

                return [...high, ...low, otherCard];
            },
        },
        pair: {
            rank: 8,
            sort: (a: string[], b: string[]): number => {
                let aval = <number>this.getPairVal(a, false);
                let bval = <number>this.getPairVal(b, false);

                if (aval == bval) {
                    const remA = this.getPairRemainingCards(a, false);
                    const remB = this.getPairRemainingCards(b, false);

                    return this.sortNoPair(remA, remB);
                } else {
                    return bval - aval;
                }
            },
            display: (h: string[]): string => {
                const val = this.fixWordForPlural(<string>this.getPairVal(h, true));

                return `Pair of ${val}`;
            },
            sortCardsForDisplay: (h: string[]): string[] => {
                let sorted = [];
                const pair = this.getPairVal(h, false);

                h = this.sortHandNumerically(h);

                let rem = this.getPairRemainingCards(h, true);

                h.forEach(c => {
                    if (Number(c.slice(1, c.length)) == pair) {
                        sorted.push(c);
                    }
                });

                sorted = sorted.concat(rem);

                return sorted;
            },
        },
        highCard: {
            rank: 9,
            sort: (a: string[], b: string[]): number => {
                return this.sortNoPair(a, b);
            },
            display: (h: string[]): string => {
                let highestCard =
                    this.cardValue(h[4], false, false) == 14
                        ? 'Ace'
                        : this.cardValue(h[4], true, true);

                return `${highestCard} High`;
            },
            sortCardsForDisplay: (h: string[]): string[] => {
                h = this.sortHandNumerically(h);
                return h.reverse();
            },
        },
    };

    constructor(
        private http: Http,
        private errorService: ErrorService,
        private gameService: GameService,
        @Inject(APP_CONFIG) private config: AppConfigInterface
    ) {}

    loadGameHand(gameId: string): Promise<SortableHand[] | Hand[]> {
        let path = this.apiPrefix + '/game/' + gameId;

        return this.http
            .get(path)
            .toPromise()
            .then(response => {
                // console.log(response.json());
                return response.json();
            })
            .catch(this.errorService.handleError);
    }

    createHand(g: string, p: string, h: string): Promise<Hand> {
        const path = this.apiPrefix;
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        const payload = { g, p, h };

        return this.http
            .post(path, payload, options)
            .toPromise()
            .then(response => {
                const H = <Hand>response.json();
                const G = <Game>H.g;
                this.gameService.checkIfGameIsLatestAndTriggerUpdate(G._id);
                return H;
            })
            .catch(this.errorService.handleError);
    }

    // updateHand(id: string, g: string, p: string, h: string): Promise<Hand> {
    //     const path = this.apiPrefix + '/' + id;
    //     const headers = new Headers({ 'Content-Type': 'application/json' });
    //     const options = new RequestOptions({ headers: headers });
    //     const payload = { g, p, h };

    //     return this.http
    //         .post(path, payload, options)
    //         .toPromise()
    //         .then(response => {
    //             const H = <Hand>response.json();
    //             this.gameService.checkIfGameIsLatestAndTriggerUpdate(<string>H.g);
    //             // console.log(H);
    //             return H;
    //         })
    //         .catch(this.errorService.handleError);
    // }

    loadHighHands(year: number): Promise<Hand[]> {
        let path = this.apiPrefix + '/year/' + year;

        return this.http
            .get(path)
            .toPromise()
            .then(response => {
                const hands = <Hand[]>response.json();

                // hands.forEach(h => {
                //     const type = geth
                // })

                return hands;
            })
            .catch(this.errorService.handleError);
    }

    sortHand(hand: string[]): string[] {
        const type = this.determineHandType(hand);

        return this.types[type].sortCardsForDisplay(hand);
    }

    sortHighHands(hands: Hand[]): SortableHand[] {
        const sortableHands = this.genSortableHands(hands);
        return this.sortHands(sortableHands);
    }

    private genSortableHands(hands: Hand[]): SortableHand[] {
        const ret: SortableHand[] = [];

        hands.forEach(hand => {
            const h = hand.h.split(',');
            const type = this.determineHandType(h);

            ret.push({
                g: hand.g,
                p: hand.p,
                createdAt: hand.createdAt,
                updatedAt: hand.updatedAt,
                type,
                rank: this.types[type].rank,
                display: this.types[type].display(h),
                hand: this.types[type].sortCardsForDisplay(h),
                highHandValue: hand.highHandValue,
            });
        });

        return ret;
    }

    getHandDisplay(hand: SortableHand): string {
        return this.types[hand.type].display(hand.hand);
    }

    determineHandType(hand: string[]): string {
        const suits = {};
        const counts = {};
        let numbSuits = 0;
        let numbCounts = 0;

        hand.forEach(c => {
            const suit = c.slice(0, 1);
            const count = c.slice(1, c.length);

            if (!suits[suit]) {
                ++numbSuits;
                suits[suit] = 0;
            }

            if (!counts[count]) {
                ++numbCounts;
                counts[count] = 0;
            }

            ++suits[suit];
            ++counts[count];
        });

        if (numbCounts < 5) {
            let highest = 2;
            let moreThanTwo = 0;

            for (let prop in counts) {
                const val = counts[prop];

                if (val > highest) {
                    highest = val;
                }

                if (val > 1) {
                    ++moreThanTwo;
                }
            }

            if (moreThanTwo == 2) {
                return highest == 3 ? 'fullHouse' : 'twoPair';
            } else {
                switch (highest) {
                    case 4:
                        return 'fourKind';
                    // break
                    case 3:
                        return 'threeKind';
                    // break
                    default:
                        return 'pair';
                }
            }
        } else {
            const straight = this.isStraight(hand);
            const flush = numbSuits == 1;

            if (straight && flush) {
                return 'straightFlush';
            } else if (flush) {
                return 'flush';
            } else if (straight) {
                return 'straight';
            }

            return 'highCard';
        }
    }

    private isStraight(hand: string[]): boolean {
        const hasAce = this.cardValue(hand[4], false, false) == 14;

        if (hasAce) {
            return (
                <number>this.cardValue(hand[0], false, false) == 10 ||
                <number>this.cardValue(hand[3], false, false) == 5
            );
        } else {
            return (
                <number>this.cardValue(hand[4], false, false) -
                    <number>this.cardValue(hand[0], false, false) ==
                4
            );
        }
    }

    private cardValue(
        c: string,
        convertBigNames: boolean,
        convertNumbers: boolean
    ): number | string {
        // console.log(c, convertBigNames)
        let val: number | string = Number(c.slice(1, c.length));
        // console.log(val)

        if (convertBigNames) {
            switch (val) {
                case 11:
                    val = 'Jack';
                    break;
                case 12:
                    val = 'Queen';
                    break;
                case 13:
                    val = 'King';
                    break;
                case 14:
                    val = 'Ace';
                    break;
            }
        }

        if (convertNumbers) {
            switch (val) {
                case 2:
                    val = 'Two';
                    break;
                case 3:
                    val = 'Three';
                    break;
                case 4:
                    val = 'Four';
                    break;
                case 5:
                    val = 'Five';
                    break;
                case 6:
                    val = 'Six';
                    break;
                case 7:
                    val = 'Seven';
                    break;
                case 8:
                    val = 'Eight';
                    break;
                case 9:
                    val = 'Nine';
                    break;
                case 10:
                    val = 'Ten';
                    break;
            }
        }

        return val;
    }

    private sortHands(hands: SortableHand[]): SortableHand[] {
        return hands.sort(
            (a: SortableHand, b: SortableHand): number => {
                if (a.rank != b.rank) {
                    return a.rank - b.rank;
                } else {
                    return this.types[a.type].sort(a.hand, b.hand);
                }
            }
        );
    }

    private getStraightHighestCard(hand: string[], returnName: boolean): number | string {
        const c4 = this.cardValue(hand[3], false, false);
        const c4n = this.cardValue(hand[3], true, true);
        const c5 = this.cardValue(hand[4], false, false);
        const c5n = this.cardValue(hand[4], true, true);

        if (c4 == 5 && c5 == 14) {
            return returnName ? c4n : c4;
        }

        return returnName ? c5n : c5;
    }

    private straightSort(h: string[]): string[] {
        const hasAce = this.cardValue(h[4], false, false) == 14;

        if (hasAce) {
            const aceIsHighEndOfStraight = this.isAceHighEnd(h);

            if (!aceIsHighEndOfStraight) {
                h = this.convertHandAcesToOnes(h);
            }
        }

        h = this.sortHandNumerically(h);

        return h.reverse();
    }

    private isAceHighEnd(hand: string[]): boolean {
        return this.cardValue(hand[0], false, false) == 10;
    }

    private convertHandAcesToOnes(hand: string[]): string[] {
        let ret = [];

        hand.forEach(i => {
            if (this.cardValue(i, false, false) == 14) {
                ret.push(i.charAt(0) + 1);
            } else {
                ret.push(i);
            }
        });

        return ret;
    }

    private sortHandNumerically(h: string[]): string[] {
        return h.sort(
            (a: string, b: string): number =>
                <number>this.cardValue(a, false, false) - <number>this.cardValue(b, false, false)
        );
    }

    private sortNoPair(a: string[], b: string[]): number {
        let aval = 0;
        let bval = 0;
        let i = a.length - 1;

        while (i >= 0) {
            const av = <number>this.cardValue(a[i], false, false);
            const bv = <number>this.cardValue(b[i], false, false);

            if (av != bv && aval == 0) {
                aval = av;
                bval = bv;
            }

            --i;
        }

        return bval - aval;
    }

    private sortFourOfKind(hand: string[]): string[] {
        hand = hand.sort((a, b) => {
            const an = <number>this.cardValue(a, false, false);
            const bn = <number>this.cardValue(b, false, false);

            return an - bn;
        });

        if (
            <number>this.cardValue(hand[0], false, false) !=
            <number>this.cardValue(hand[1], false, false)
        ) {
            hand.reverse();
        }

        return hand;
    }

    private getFourOfKindVal(hand: string[], convertBigNames: boolean): number | string {
        if (
            this.cardValue(hand[1], convertBigNames, convertBigNames) !=
            this.cardValue(hand[2], convertBigNames, convertBigNames)
        ) {
            hand = this.sortFourOfKind(hand);
        }

        // let cv = this.cardValue(hand[1], true, false);
        let cv = this.cardValue(hand[1], convertBigNames, convertBigNames);

        return cv;
    }

    private getFourKindExtra(hand: string[]): number {
        const a = <number>this.cardValue(hand[0], false, false);
        const b = <number>this.cardValue(hand[1], false, false);
        const c = <number>this.cardValue(hand[4], false, false);

        return a == b ? c : a;
    }

    private fixWordForPlural(word: string): string {
        const fixes = {
            Six: 'Sixes',
        };
        return fixes[word] || `${word}s`;
    }

    private getFullHouseVal(
        hand: string[],
        over: boolean,
        convertBigNames: boolean
    ): number | string {
        hand = hand.sort((a, b) => {
            const av = Number(a.slice(1, a.length));
            const bv = Number(b.slice(1, b.length));

            return av - bv;
        });

        const a = this.cardValue(hand[1], convertBigNames, convertBigNames);
        const b = this.cardValue(hand[2], convertBigNames, convertBigNames);
        const c = this.cardValue(hand[3], convertBigNames, convertBigNames);

        if (a == b) {
            return over ? a : c;
        } else {
            return over ? b : a;
        }
    }

    private getThreeOfKindVal(hand: string[], convertBigNames: boolean): number | string {
        return this.cardValue(hand[2], convertBigNames, convertBigNames);
    }

    private getThreeKindRemainingCards(hand: string[], rev: boolean): string[] {
        const tv = this.getThreeOfKindVal(hand, false);
        const ret = [];

        hand.forEach(i => {
            const v = this.cardValue(i, false, false);

            if (v != tv) {
                ret.push(i);
            }
        });

        if (rev) {
            ret.reverse();
        }

        return ret;
    }

    private getTwoPairVal(
        hand: string[],
        top: boolean,
        nonpair: boolean,
        convertBigNames: boolean
    ): number | string {
        const c1 = this.cardValue(hand[0], convertBigNames, convertBigNames);
        const c2 = this.cardValue(hand[1], convertBigNames, convertBigNames);
        const c3 = this.cardValue(hand[2], convertBigNames, convertBigNames);
        const c4 = this.cardValue(hand[3], convertBigNames, convertBigNames);
        const c5 = this.cardValue(hand[4], convertBigNames, convertBigNames);

        if (nonpair) {
            if (c1 == c2) {
                return c3 == c4 ? c5 : c3;
            } else {
                return c1;
            }
        }

        if (convertBigNames) {
            return top ? (c2 == 'Ace' ? c2 : c4) : c2 == 'Ace' ? c4 : c2;
        } else {
            return top ? (c2 == 14 ? c2 : c4) : c2 == 14 ? c4 : c2;
        }
    }

    private getPairVal(hand: string[], convertBigNames: boolean): number | string {
        let pair;
        let i = 0;

        while (!pair) {
            const plus = i + 1;

            if (
                hand[plus] &&
                this.cardValue(hand[i], false, false) == this.cardValue(hand[plus], false, false)
            ) {
                pair = this.cardValue(hand[i], convertBigNames, convertBigNames);
            }

            ++i;
        }

        return pair;
    }

    private getPairRemainingCards(hand: string[], rev: boolean): string[] {
        const pv = <number>this.getPairVal(hand, false);
        const ret = [];

        hand.forEach(i => {
            const v = this.cardValue(i, false, false);

            if (v != pv) {
                ret.push(i);
            }
        });

        if (rev) {
            ret.reverse();
        }

        return ret;
    }
}
