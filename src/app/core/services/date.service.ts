import { Injectable } from '@angular/core'

@Injectable()
export class DateService {

    private _selectedYear : number

    private monthNames = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ]

    private shortMonthNames = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec'
    ]

    get selectedYear() : number {
        return this._selectedYear
    }

    set selectedYear(y : number) {
        this._selectedYear = y
    }

    getMonthText(m : number, short? : boolean) : string {
        if(short) {
            return this.shortMonthNames[m]
        }
        return this.monthNames[m]
    }

    addOrGetDateSuffix(d : number, addIt : boolean) : string {

        if(!d) {
            return ''
        }

        if(d == 1 || d == 21 || d == 31) {
            return addIt ? d + 'st' : 'st'
        }else if(d == 2 || d == 22){
            return addIt ? d + 'nd' : 'nd'
        }else if(d == 3 || d == 23){
            return addIt ? d + 'rd' : 'rd'
        }else{
            return addIt ? d + 'th' : 'th'
        }
    }

    areDatesEqual(d1 : Date, d2 : Date) : boolean {

        let d1b = new Date(d1.toString())
        let d2b = new Date(d2.toString())

        d1b.setHours(0,0,0,0)
        d2b.setHours(0,0,0,0)

        return d1b.getTime() === d2b.getTime()
    }

    isPastDate(d : Date) : boolean {

        let d1 = new Date(d.toString())
        let d2 = new Date()

        d1.setHours(0,0,0,0)
        d2.setHours(0,0,0,0)

        return d1.getTime() <= d2.getTime()

    }

    isToday(d : Date) : boolean {

        let d1 = new Date(d.toString())
        let d2 = new Date()

        d1.setHours(0,0,0,0)
        d2.setHours(0,0,0,0)

        return d1.getTime() == d2.getTime()
    }

    /*isDateTodayOrOlder(d : Date) : boolean {

    }*/

    private monthDifference(d1, d2) : number {
        var months
        months = (d2.getFullYear() - d1.getFullYear()) * 12
        months -= d1.getMonth() + 1
        months += d2.getMonth()
        return months <= 0 ? 0 : months
    }

    getDuration(d : Date) : string {

        const now = new Date()
        const diff = now.getTime() - d.getTime()
        const days = diff / (1000 * 60 * 60 * 24)
        let months = this.monthDifference(d, now)
        const years = Math.floor(days / 365)

        let ystr = ''
        let gap = ''
        let mstr = ''

        if(years > 0) {
            months -= years * 12
            ystr = years == 1 ? 'year' : 'years'
            ystr = years + ' ' + ystr
            gap = ' '
        }

        if(months > 0) {
            mstr = months == 1 ? 'month' : 'months'
            mstr = months + ' ' + mstr
        }else{
            gap = ''
        }

        return ystr + gap + mstr
    }
}
