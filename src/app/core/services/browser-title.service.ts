import { Injectable } from '@angular/core'
import { Title } from '@angular/platform-browser'

@Injectable()
export class BrowserTitleService {
    constructor(private _title : Title) {}
    public set title(t : string) {
        this._title.setTitle(`${t} | TheRavenhurst`)
    }
}