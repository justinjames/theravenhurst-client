import { Injectable, Inject } from '@angular/core'
import { Http, Headers, RequestOptions } from '@angular/http'
// import { BehaviorSubject } from 'rxjs'
import 'rxjs/add/operator/toPromise'

import { APP_CONFIG, AppConfigInterface } from '../../config'
import { ErrorService } from './error.service'  // do this to avoid circular dependency error
import { Entry } from '../'

@Injectable()
export class EntryService {

    private apiPrefix = this.config.apiEndpoint + '/entries'

    constructor(
        private http : Http,
        private errorService : ErrorService,
        @Inject(APP_CONFIG) private config: AppConfigInterface
    ) {}

    loadGameEntries(gameId : string) : Promise<Entry[]> {

        let path = this.apiPrefix + '/game/' + gameId

        return this.http.get(path)
            .toPromise()
            .then(response => {
                return <Entry[]>response.json()
            })
            .catch(this.errorService.handleError)
    }

    addPlayerToGame(gameId : string, gameDate : Date,  playerId : string) : Promise<Entry> {

        let path = this.apiPrefix
        let headers = new Headers({ 'Content-Type': 'application/json' })
        let options = new RequestOptions({ headers: headers })
        let body = { g: gameId, date: gameDate, p: playerId }

        return this.http.post(path, body, options)
            .toPromise()
            .then(response => {
                return response.json()
            })
            .catch(this.errorService.handleError)
    }

    removePlayerFromGame(entryId : string) : Promise<Entry> {

        let path = this.apiPrefix + '/' + entryId

        return this.http.delete(path)
            .toPromise()
            .then(response => {
                return response.json()
            })
            .catch(this.errorService.handleError)
    }

    updateEntry(entry : Entry) : Promise<Entry> {

        let path = this.apiPrefix + '/' + entry._id
        let headers = new Headers({ 'Content-Type': 'application/json' })
        let options = new RequestOptions({ headers: headers })

        return this.http.post(path, entry, options)
            .toPromise()
            .then(response => {
                return response.json()
            })
            .catch(this.errorService.handleError)
    }

}

/*
entryRouter.post('/', auth.isAdmin, (req, res) => {
    Entries.create(req.body, callback(res))
})

entryRouter.post('/:entryId', auth.isAdmin, (req, res) => {
    Entries.update(req.params.entryId, req.body, callback(res))
})

entryRouter.delete('/:entryId', auth.isAdmin, (req, res) => {
    Entries.delete(req.params.entryId, callback(res))
})

entryRouter.get('/', auth.isLoggedIn, (req, res) => {
    Entries.readAll(callback(res))
})

entryRouter.get('/player/:playerId', auth.isLoggedIn, (req, res) => {
    Entries.readAllByPlayer(req.params.playerId, callback(res))
})

entryRouter.get('/:entryId', auth.isLoggedIn, (req, res) => {
    Entries.readEntry(req.params.entryId, callback(res))
})

*/