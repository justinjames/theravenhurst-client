import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Subject } from 'rxjs/Subject';

import { Message, Player, UsersSocketObject } from '../';

@Injectable()
export class SocketService {
    private socketUrl =
        window.location.host == 'localhost:4200'
            ? 'localhost:1234'
            : 'https://www.theravenhurst.com';
    private socket: any;
    private me: Player;

    private _users: UsersSocketObject;
    usersSubject = new Subject<UsersSocketObject>();

    private _message: Message;
    messageSubject = new Subject<Message>();

    init(p: Player) {
        this.me = p;
        this.connectSocket();
    }

    private connectSocket() {
        this.socket = io.connect(this.socketUrl, {
            query: `token=+${this.me._id}`,
            forceNew: true,
        });
        this.setupTypes();
    }

    private setupTypes() {
        this.socket.on('message', (m: Message) => {
            this.messageSubject.next(m);
        });

        this.socket.on('users', (users: UsersSocketObject) => {
            this.usersSubject.next(users);
        });
    }

    emitMessage(m: Message) {
        this.socket.emit('message', m);
    }
}
