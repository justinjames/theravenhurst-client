import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Params, Router } from '@angular/router';

@Injectable()
export class NavigationService {
    private _loginRoute: ActivatedRouteSnapshot;
    constructor(private router: Router) {}
    goto(link: string[] | string, qp?: Params) {
        const path = typeof link == 'string' ? [link] : link;
        this.router.navigate(path, { queryParams: qp });
    }
    get loginRoute(): ActivatedRouteSnapshot {
        return this._loginRoute;
    }
    set loginRoute(r: ActivatedRouteSnapshot) {
        this._loginRoute = r;
    }
}
