import { Injectable, Inject } from '@angular/core'
import { Http, Headers, RequestOptions } from '@angular/http'
import { BehaviorSubject } from 'rxjs'
import 'rxjs/add/operator/toPromise'

import { APP_CONFIG, AppConfigInterface } from '../../config'
import { ErrorService } from './error.service'  // do this to avoid circular dependency error
// import { PlayerService } from './player.service'
import { Announcement } from '../'

@Injectable()
export class AnnouncementService {

    private apiPrefix = this.config.apiEndpoint + '/announcements'

    _announcements : Announcement[] = []
    announcementsSubject = new BehaviorSubject<Announcement[]>(this._announcements)

    constructor(
        private http : Http,
        private errorService : ErrorService,
        // private playerService : PlayerService,
        @Inject(APP_CONFIG) private config: AppConfigInterface
    ) {}

    init() {
        this.loadAllAnnouncements()
    }

    private loadAllAnnouncements() {

        let path = this.apiPrefix

        this.http.get(path)
            .toPromise()
            .then(response => {
                this._announcements = <Announcement[]>response.json()
                this.announcementsSubject.next(this._announcements)
            })
            .catch(this.errorService.handleError)
    }

    createNewAnnouncement(message : string) : Promise<Announcement> {

        const path = this.apiPrefix
        const headers = new Headers({ 'Content-Type': 'application/json' })
        const options = new RequestOptions({ headers: headers })
        const payload = { message }

        return this.http.post(path, payload, options)
            .toPromise()
            .then(response => {

                let res = <Announcement>response.json()

                this._announcements.unshift(res)
                this.announcementsSubject.next(this._announcements)

                return res
            })
            .catch(this.errorService.handleError)

    }
}
