import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/toPromise';

import { APP_CONFIG, AppConfigInterface, SessionUser } from '../../config';
import { ErrorService } from './error.service'; // do this to avoid circular dependency error
import { GameService } from './game.service';
import { AnnouncementService } from './announcement.service';
import { SocketService } from './socket.service';
import { MessageService } from './message.service';
import {
    UsersSocketObject,
    Player,
    PlayerDictionary,
    PlayerStats,
    PlayerResponseObject,
} from '../';

const su = SessionUser;

@Injectable()
export class PlayerService {
    private bareApiPrefix = this.config.apiEndpoint;
    private apiPrefix = this.config.apiEndpoint + '/players';

    _player: Player;
    playerSubject = new BehaviorSubject<Player>(this._player);
    _players: Player[] = [];
    playersSubject = new BehaviorSubject<Player[]>(this._players);
    // _leaders : Player[] = []
    // leadersSubject = new BehaviorSubject<Player[]>(this._leaders)

    private playerDictionary: PlayerDictionary = {};

    constructor(
        private http: Http,
        // private arrayService : ArrayService,
        private errorService: ErrorService,
        private announcementService: AnnouncementService,
        private gameService: GameService,
        private socketService: SocketService,
        private messageService: MessageService,
        @Inject(APP_CONFIG) private config: AppConfigInterface,
    ) {}

    init() {
        this.player = <Player>su.user;

        if (this.player) {
            this.getAllPlayers();
            // this.loadLeaders()
            this.announcementService.init();
            this.gameService.init();
            this.messageService.init();
        }
    }

    get player(): Player {
        return this._player;
    }

    set player(u: Player) {
        this._player = u;
        this.playerSubject.next(this._player);
    }

    login(payload: any): Promise<any> {
        let path = this.bareApiPrefix + '/login';
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http
            .post(path, payload, options)
            .toPromise()
            .then(response => {
                let res = response.json();

                if (res.success) {
                    this._player = res.player as Player;
                }

                this.getAllPlayers();
                // this.loadLeaders()
                this.announcementService.init();
                this.gameService.init();
                this.messageService.init();

                return res;
            })
            .catch(this.errorService.handleError);
    }

    logout(): Promise<any> {
        let path = this.bareApiPrefix + '/logout';
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http
            .post(path, {}, options)
            .toPromise()
            .then(response => {
                this.player = null;
                this._players.length = 0;
                this.messageService.logout();
                return response;
            })
            .catch(this.errorService.handleError);
    }

    private getAllPlayers() {
        let path = this.apiPrefix;

        return this.http
            .get(path)
            .toPromise()
            .then(response => {
                // put this in array service.  use some kind of while and array.shift()

                this._players = <Player[]>response.json();

                this._players.forEach(p => {
                    p.first = p.first.trim();
                    p.last = p.last.trim();
                });

                this.sortPlayers();
                this.updatePlayerDictionary();
                this.playersSubject.next(this._players);
                this.subscribeToUsersAndInitSocket();
            })
            .catch(this.errorService.handleError);
    }

    // private loadLeaders() {

    //     let path = this.apiPrefix + '/points'

    //     return this.http.get(path)
    //         .toPromise()
    //         .then(response => {

    //             let most = 0

    //             this._leaders = <Player[]>response.json()
    //             this._leaders.forEach(l => {
    //                 most = l.points > most ? l.points : most
    //             })
    //             this._leaders.forEach(l => {
    //                 if(l.points == most) {
    //                     l.topPlayer = true
    //                 }
    //             })

    //             this.leadersSubject.next(this._leaders)
    //         })
    //         .catch(this.errorService.handleError)
    // }

    loadLeaders(year: number): Promise<Player[]> {
        let path = this.apiPrefix + '/points/' + year;

        return this.http
            .get(path)
            .toPromise()
            .then(response => {
                let most = 0;
                let ret = <Player[]>response.json();

                // this._leaders = <Player[]>response.json()
                ret.forEach(l => {
                    most = l.points > most ? l.points : most;
                });

                ret.forEach(l => {
                    if (l.points == most) {
                        l.topPlayer = true;
                    }
                });

                // this.leadersSubject.next(this._leaders)
                return ret;
            })
            .catch(this.errorService.handleError);
    }

    private sortPlayers() {
        this._players.sort((a, b) => {
            let aVal = a.first.toLowerCase();
            let bVal = b.first.toLowerCase();

            if (aVal == bVal) {
                aVal = a.last.toLowerCase();
                bVal = b.last.toLowerCase();
            }

            if (aVal < bVal) {
                return -1;
            } else if (aVal > bVal) {
                return 1;
            }

            return 0;
        });
    }

    private updatePlayerDictionary() {
        this._players.forEach(p => {
            this.playerDictionary[p._id] = p;
        });
    }

    private subscribeToUsersAndInitSocket() {
        this.socketService.usersSubject.subscribe((updatedUsersList: UsersSocketObject) => {
            this.updateOnlineUsers(updatedUsersList);
        });

        this.socketService.init(this._player);
    }

    updateOnlineUsers(users: UsersSocketObject) {
        this._players.forEach(p => {
            p.online = users['+' + p._id];
        });
        this.playersSubject.next(this._players);
    }

    getPlayer(playerId: string): Promise<PlayerResponseObject> {
        let path = this.apiPrefix + '/' + playerId;

        return this.http
            .get(path)
            .toPromise()
            .then(response => {
                return <PlayerResponseObject>response.json();
            })
            .catch(this.errorService.handleError);
    }

    getExtraName(p: Player): string {
        if (p.say || p.nick) {
            let arr: string[] = [];
            let rand: number;

            if (p.say) {
                arr.push(p.say);
            }

            if (p.nick) {
                arr.push(p.nick);
            }

            rand = Math.floor(Math.random() * arr.length);

            return arr[rand];
        }

        return '';
    }

    createPlayer(payload: Player): Promise<Player> {
        let path = this.apiPrefix;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http
            .post(path, payload, options)
            .toPromise()
            .then(response => {
                const newPlayer = <Player>response.json();

                this._players.push(newPlayer);
                this.sortPlayers();
                this.updatePlayerDictionary();
                this.playersSubject.next(this._players);

                return newPlayer;
            })
            .catch(this.errorService.handleError);
    }

    updatePlayer(playerId: string, payload: Player): Promise<Player> {
        let path = this.apiPrefix + '/' + playerId;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http
            .post(path, payload, options)
            .toPromise()
            .then(response => <Player>response.json())
            .catch(this.errorService.handleError);
    }

    udpdatePassword(playerId: string, payload: Player): Promise<Player> {
        let path = this.apiPrefix + '/' + playerId + '/password';
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http
            .post(path, payload, options)
            .toPromise()
            .then(response => <Player>response.json())
            .catch(this.errorService.handleError);
    }

    newDonkey(playerId: string): Promise<Player> {
        let path = this.apiPrefix + '/' + playerId + '/randomImage';

        return this.http
            .get(path)
            .toPromise()
            .then(response => {
                return <PlayerResponseObject>response.json();
            })
            .catch(this.errorService.handleError);
    }

    uploadImage(playerId: string, file: File): Promise<Player> {
        let path = this.apiPrefix + '/' + playerId + '/uploadImage';
        let headers = new Headers({ Accept: 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let formData: FormData = new FormData();

        formData.append('recfile', file, file.name);

        return this.http
            .post(path, formData, options)
            .toPromise()
            .then(response => {
                return <Player>response.json();
            })
            .catch(this.errorService.handleError);
    }

    getShortName(pl: Player): string {
        let sameFirst = this._players.filter(p => p.first.toLowerCase() == pl.first.toLowerCase());
        return sameFirst.length == 1 ? pl.first : pl.first + ' ' + pl.last.charAt(0);
    }

    getPlayerFromId(id: string): Player {
        return this.playerDictionary[id] || {};
    }

    readPlayerStats(playerId: string, year: number): Promise<PlayerStats> {
        const path = this.apiPrefix + '/' + playerId + '/stats/' + year;

        return this.http
            .get(path)
            .toPromise()
            .then(response => {
                return <PlayerStats>response.json();
            })
            .catch(this.errorService.handleError);
    }

    readAverageStats(year: number): Promise<PlayerStats> {
        const path = this.apiPrefix + '/statsaverage/' + year;

        return this.http
            .get(path)
            .toPromise()
            .then(response => {
                return <PlayerStats>response.json();
            })
            .catch(this.errorService.handleError);
    }

    readAllStats(year: number): Promise<PlayerStats[]> {
        const path = this.apiPrefix + '/stats/' + year;

        return this.http
            .get(path)
            .toPromise()
            .then(response => {
                return <PlayerStats[]>response.json();
            })
            .catch(this.errorService.handleError);
    }

    activatePlayerIfNotActive(playerId: string): void {
        const p = this._players.find(p => p._id == playerId);

        if (p) {
            p.active = true;
            this.playersSubject.next(this._players);
        }
    }
}
