import { Injectable } from '@angular/core'
import { Location } from '@angular/common'

@Injectable()
export class LocationService {
    constructor(private _location : Location) {}
    set location(newLoc : string) {
        this._location.go(newLoc)
    }
}