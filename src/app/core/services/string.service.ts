import { Injectable } from '@angular/core'


@Injectable()
export class StringService {

    constructor() {}

    replaceUrl(s : string) : string {

        const exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig

        return s.replace(exp,`<a href="$1" target="_blank">$1</a>`)
    }

    // addDeleteButtonToEndOfMessage(s : string) : stirng {
    //     const but = `<button (click)="deleteMe()">Delete</button>`
    //     console.log(but)
    //     console.log(`${s}${but}`)
    //     return `${s}${but}`
    // }

}