import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { BehaviorSubject, Subject } from 'rxjs';
import 'rxjs/add/operator/toPromise';

import { APP_CONFIG, AppConfigInterface } from '../../config';
import { ErrorService } from './error.service'; // do this to avoid circular dependency error
import { SocketService } from './socket.service';
import { Message } from '../';

interface UploadImageReply {
    filePath: string;
}

interface MessagePayload {
    text: string;
    image?: string;
}

@Injectable()
export class MessageService {
    private apiPrefix = this.config.apiEndpoint + '/messages';

    private _storedMessages: Message[] = [];
    storedMessagesSubject = new BehaviorSubject<Message[]>(
        this._storedMessages
    );

    liveMessageSubject = new Subject<Message>();
    deletedMessageSubject = new Subject<Message>();

    constructor(
        private http: Http,
        private errorService: ErrorService,
        private socketService: SocketService,
        @Inject(APP_CONFIG) private config: AppConfigInterface
    ) {}

    init() {
        this.subscribeToSocketMessage();
        this.loadAllMessages();
    }

    logout() {
        this._storedMessages.length = 0;
        this.storedMessagesSubject.next(this._storedMessages);
    }

    private subscribeToSocketMessage() {
        this.socketService.messageSubject.subscribe((m: Message) => {
            const adjustedMessge = this.adjustMessage(m);

            this.liveMessageSubject.next(adjustedMessge);
            this._storedMessages.push(adjustedMessge);
        });
    }

    private loadAllMessages() {
        let path = this.apiPrefix;

        this.http
            .get(path)
            .toPromise()
            .then(response => {
                let ms = <Message[]>response.json();

                ms.forEach(m => {
                    this._storedMessages.push(this.adjustMessage(m));
                });

                this.storedMessagesSubject.next(this._storedMessages);
            })
            .catch(this.errorService.handleError);
    }

    private adjustMessage(m: Message): Message {
        const msg: Message = {
            file: m.file,
            id: m.id,
            date: new Date(m.id),
            text: m.text,
            user: m.user,
        };

        if (m.image) {
            msg.image = m.image;
        }

        return msg;
    }

    createNewMessage(text: string, imgPath: string): Promise<Message> {
        let path = this.apiPrefix;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        const payload: MessagePayload = {
            text,
        };

        if (imgPath) {
            payload.image = imgPath;
        }

        return this.http
            .post(path, payload, options)
            .toPromise()
            .then(response => {
                const newMessage = <Message>response.json();
                this.socketService.emitMessage(newMessage);
                return newMessage;
            })
            .catch(this.errorService.handleError);
    }

    uploadImage(file: File): Promise<string> {
        let path = this.apiPrefix + '/uploadImage';
        let headers = new Headers({ Accept: 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let formData: FormData = new FormData();

        formData.append('recfile', file, file.name);

        return this.http
            .post(path, formData, options)
            .toPromise()
            .then(response => {
                const res = <UploadImageReply>response.json();
                // const filePath = res.filePath
                return res.filePath;
            })
            .catch(this.errorService.handleError);
    }

    deleteMessage(messageFileName: string) {
        let path = this.apiPrefix + '/' + messageFileName;

        this.http
            .delete(path)
            .toPromise()
            .then(response => {
                const idx = this._storedMessages.findIndex(m => {
                    return m.file == messageFileName;
                });

                if (idx != -1) {
                    const removed = this._storedMessages.splice(idx, 1);
                    this.deletedMessageSubject.next(removed[0]);
                }
            })
            .catch(this.errorService.handleError);
    }
}
