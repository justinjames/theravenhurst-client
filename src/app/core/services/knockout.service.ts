import { Injectable, Inject } from '@angular/core'
import { Http, Headers, RequestOptions } from '@angular/http'
// import { BehaviorSubject } from 'rxjs'
import 'rxjs/add/operator/toPromise'

import { APP_CONFIG, AppConfigInterface } from '../../config'
import { ErrorService } from './error.service'  // do this to avoid circular dependency error
import { Knockout, KnockoutDeleted } from '../'

@Injectable()
export class KnockoutService {

    private apiPrefix = this.config.apiEndpoint + '/knockouts'

    constructor(
        private http : Http,
        private errorService : ErrorService,
        @Inject(APP_CONFIG) private config: AppConfigInterface
    ) {}

    loadGameKnockouts(gameId : string) : Promise<Knockout[]> {

        let path = this.apiPrefix + '/game/' + gameId

        return this.http.get(path)
            .toPromise()
            .then(response => {

                const ks = response.json()

                ks.forEach(k => {
                    k.s = k.s || k.createdAt
                })

                return ks
            })
            .catch(this.errorService.handleError)
    }

    loadTimesEliminatedOther(playerId : string, opponentId : string, year : number) : Promise<Knockout[]> {

        const path = `${this.apiPrefix}/eliminated/${playerId}/${opponentId}/${year}`

        return this.http.get(path)
            .toPromise()
            .then(response => {
                return <Knockout[]>response.json()
            })
            .catch(this.errorService.handleError)
    }

    createKnockout(g : string, w : string, l : string, bounty : boolean) : Promise<Knockout> {

        const path = this.apiPrefix
        const headers = new Headers({ 'Content-Type': 'application/json' })
        const options = new RequestOptions({ headers: headers })
        const b = bounty ? 1 : 0
        const payload = {g, w, l, b}

        return this.http.post(path, payload, options)
            .toPromise()
            .then(response => {
                return <Knockout>response.json()
            })
            .catch(this.errorService.handleError)
    }

    removeKnockout(id : string) : Promise<KnockoutDeleted> {

        const path = this.apiPrefix + '/' + id

        return this.http.delete(path)
            .toPromise()
            .then(response => {
                return <KnockoutDeleted>response.json()
            })
            .catch(this.errorService.handleError)
    }

    updateKnockoutSort(id : string, s : Date) : Promise<Knockout> {

        const path = `${this.apiPrefix}/${id}`
        const headers = new Headers({ 'Content-Type': 'application/json' })
        const options = new RequestOptions({ headers: headers })
        const payload = {s}

        return this.http.post(path, payload, options)
            .toPromise()
            .then(response => {
                return <Knockout>response.json()
            })
            .catch(this.errorService.handleError)
    }

    updateKnockoutBounty(id : string, b : number) : Promise<Knockout> {

        const path = `${this.apiPrefix}/${id}`
        const headers = new Headers({ 'Content-Type': 'application/json' })
        const options = new RequestOptions({ headers: headers })
        const payload = {b}

        return this.http.post(path, payload, options)
            .toPromise()
            .then(response => {
                return <Knockout>response.json()
            })
            .catch(this.errorService.handleError)
    }

    // login(payload : any) : Promise<any> {

    //     let path = this.apiPrefix + '/login'
    //     let headers = new Headers({ 'Content-Type': 'application/json' })
    //     let options = new RequestOptions({ headers: headers })

    //     return this.http.post(path, payload, options)
    //         .toPromise()
    //         .then(response => {

    //             let res = response.json()

    //             if(res.success) {
    //                 this._game = res.game as Game
    //             }

    //             return res
    //         })
    //         .catch(this.errorService.handleError)
    // }
}