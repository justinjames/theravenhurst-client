import { Injectable } from '@angular/core'

import { ObjectService } from './object.service'

@Injectable()
export class ArrayService {

    constructor(
        private objectService : ObjectService
    ) {}

    // copyArray(arr : any[]) : any[] {
    //     return arr.slice(0)
    // }

    // doesInclude(arr : any[], item :(any[]|any)) : boolean {
    //     // looks in array to see if item or item of items exist
    //     if(item instanceof Array) {
    //         let inc = false
    //         for(let i of item) {
    //             inc = arr.includes(i)
    //             if(inc) {
    //                 break
    //             }
    //         }
    //         return inc
    //     }else{
    //         return arr.includes(item)
    //     }
    // }

    // private getReturnValue(aVal : any, bVal : any, propType = '') {

    // }

    sortByObjectProperty(arr : any[], propertiesToSort : string, reverse = false) : any[] {

        let arr2 = arr.slice(0)
        let props = propertiesToSort.split(',')

        arr2.sort((a : any, b : any) : number => {

            let prop = props[0]
            let aVal = this.objectService.getObjectPropValue(a, prop)
            let bVal = this.objectService.getObjectPropValue(b, prop)
            let cVal

            if(aVal == bVal && props.length > 1) {
                for(let i = 1; i < props.length; ++i) {
                    if(aVal == bVal) {
                        aVal = this.objectService.getObjectPropValue(a, props[i])
                        bVal = this.objectService.getObjectPropValue(b, props[i])
                    }
                }
            }

            const isString = typeof aVal == 'string'

            // if(propType != 'number') {
            if(isString) {
                aVal = aVal.toLowerCase()
                bVal = bVal.toLowerCase()
            }

            if(reverse){
                cVal = aVal
                aVal = bVal
                bVal = cVal
            }

            if(aVal < bVal) {
                return isString ? -1 : 1
            }else if(aVal > bVal) {
                return isString ? 1 : -1
            }else{
                return 0
            }
        })

        return arr2
    }

    // findOtherObjectProperty(arr : any[], lookForProperty : string, findValue : any, returnProperty : string) : any {

    //     let idx = arr.findIndex( item => {
    //         if(item[lookForProperty]) {
    //             return item[lookForProperty] == findValue
    //         }
    //         return false
    //     })

    //     if(idx == -1) {
    //         return undefined
    //     }

    //     return arr[idx][returnProperty]
    // }

    // findFullObjectFromProperty(arr : any[], lookForProperty : string, find : any) : any {

    //     let idx = arr.findIndex( item => {
    //         if(item[lookForProperty]) {
    //             return item[lookForProperty] == find
    //         }
    //         return false
    //     })

    //     if(idx == -1) {
    //         return undefined
    //     }

    //     return arr[idx]
    // }

    // stripArrayObjectsOfExcess(arr : any[], propertiesToKeep : string) : any[] {

    //     let ret = []

    //     for(let i of arr) {
    //         ret.push(this.objectService.stripProperties(i, propertiesToKeep))
    //     }

    //     return ret
    // }

    // filterByStringAndProperties(arr : any[], filter : string, propertiesToCheck : string) : any[] {

    //     let returnAll = filter == ''
    //     let filters = filter.split(',')
    //     let props = propertiesToCheck.split(',')

    //     return arr.filter(item => {

    //         if(returnAll) {
    //             return true
    //         }

    //         let match = false
    //         let value : any

    //         for(let i of props) {
    //             value = this.objectService.getObjectPropValue(item, i)
    //             if(!match && value) {
    //                 for(let f of filters) {
    //                     if(!match) {
    //                         f = f.toLowerCase()
    //                         match = value.toLowerCase().search(f) != -1
    //                     }
    //                 }
    //             }
    //         }

    //         return match
    //     })
    // }

    // removeAnyMatching(arr1 : any[], arr2 : any[], match : string) : any[] {
    //     return arr1.filter(item1 => {
    //         return arr2.findIndex((item2) : boolean => {
    //             return item2[match] == item1[match]
    //         }) == -1
    //     })
    // }

}