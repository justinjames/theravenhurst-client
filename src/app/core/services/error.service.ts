import { Injectable } from '@angular/core';

@Injectable()
export class ErrorService {
    constructor() {}

    handleError(error: any) {
        console.log('error', error);

        let msg;

        if (
            error._body &&
            /^[\],:{}\s]*$/.test(
                error._body
                    .replace(/\\["\\\/bfnrtu]/g, '@')
                    .replace(
                        /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,
                        ']'
                    )
                    .replace(/(?:^|:|,)(?:\s*\[)+/g, '')
            )
        ) {
            // console.log('JSON')

            const body = JSON.parse(error._body);
            // console.log('body', body)
            // let msg : string = body.reason._message ? body.reason._message : body.reason

            if (!body.reason) {
                msg = body;
            }
            if (body.reason._message) {
                msg = body.reason._message;
            } else if (body.reason._body) {
                msg = body.reason._body;
            } else {
                msg = body.reason;
            }
        } else {
            // console.log('NOT JSON')
            msg = error._body;
        }

        // console.log('msg', msg)
        return Promise.reject(msg);
    }
}
