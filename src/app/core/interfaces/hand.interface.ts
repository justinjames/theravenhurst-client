import { Game, Player } from './';

export interface Hand {
    _id?: string;
    g?: Game | string;
    p?: Player;
    h?: string;
    createdAt?: Date;
    updatedAt?: Date;
    highHandValue?: number;
}

export interface SortableHand extends Hand {
    // g? : Game
    // p? : Player
    // h? : String
    // createdAt? : Date
    // updatedAt? : Date
    type?: string;
    rank?: number;
    display?: string;
    hand?: string[];
    // highHandValue? : number
}

// export interface HandType {
//     [key : string] : HandTypeDetails
// }

// export interface HandTypeDetails {
//     rank : number
//     sort : (a : string[], b : string[]) => number
//     display : (h : string[]) => string
//     sortCardsForDisplay : (h : string[]) => string[]
// }
