import { Player } from './player.interface'

export interface Message {
    id? : string
    file? : string
    date? : Date // frontend only
    user? : string
    player? : Player // frontend only
    text? : string
    message? : string
    image? : string
    adjusted? : boolean
}