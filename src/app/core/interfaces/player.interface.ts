import { Hand } from "./hand.interface";
import { Game } from "./game.interface";

export interface Player {
    _id?: string;
    first?: string;
    last?: string;
    name?: string;
    nick?: string;
    say?: string;
    phone?: string;
    display?: string;
    email?: string;
    afile?: string;
    aurl?: string;
    createdAt?: Date;
    updatedAt?: Date;
    login?: Date;
    admin?: boolean;
    hasPass?: boolean;
    perms?: string;
    hide?: string;
    selected?: boolean;
    cannotRemove?: boolean;
    saving?: boolean;
    alive?: boolean;
    online?: boolean;
    points?: number;
    topPlayer?: boolean;
    active?: boolean;
}

export interface IncomingUser {
    user?: Player;
}

export interface UsersSocketObject {
    [key: string]: boolean;
}

export interface PlayerDictionary {
    [key: string]: Player;
}

interface KnockoutTotal extends Player {
    total?: number;
}

// interface MostKnockouts {
//     gameId: string;
//     count: number;
// }

export interface PlayerStats {
    totalGamesForYear: number;
    totalEntries: number;
    buyinRatio: number; //
    attendance: number;
    totalPoints: number;
    winPercent: number;
    totalEntryCost: number;
    totalPotMoneyWon: number;
    highestCash: number;
    totalThirdPlaceFinishes: number;
    potNet: number;
    bountiesWon: number;
    bountiesLost: number;
    bountiesNet: number;
    totalHighHandEntries: number;
    longestStreak: Game[];
    longestStreakLength: number;
    longestStreakActive: boolean;
    highHandsNet: number;
    highHandsWon: number;
    overallNet: number;
    netRatio: number; //
    totalEliminations: number;
    totalEliminated: number;
    knockoutWins: KnockoutTotal[];
    knockoutLosses: KnockoutTotal[];
    knockoutRatio: number; //
    highHands: Hand[];
    player?: Player;
    // mostKosInAGame?: MostKnockouts;
    mostKosGame?: string;
    mostKos?: number;
    totalRebuys?: number;
    totalGamesWithoutRebuy?: number;
    totalWinsWithoutRebuy?: number;
}

export interface PlayerResponseObject {
    player: Player;
    hasPass: boolean;
}

export interface PlayerStatsProp {
    stat?: string;
    prop?: string;
    class?: string;
    string?: boolean;
    currency?: boolean;
    percent?: boolean;
    notes?: string;
    includeAverage?: boolean;
}
