export interface Margins {
    top : number
    right : number
    bottom : number
    left : number
}

export interface GraphData {
    x? : number
    xDisp? : number
    y? : number
    name? : string
    type? : string
}