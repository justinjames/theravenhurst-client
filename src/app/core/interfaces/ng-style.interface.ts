export interface NgStyle {
    backgroundColor?: string;
    width?: string;
    top?: string;
    left?: string;
    padding?: string;
    'padding-top'?: number | string;
    'padding-bottom'?: number | string;
    'margin-top'?: number | string;
    'margin-bottom'?: number | string;
    opacity?: number;
    'background-image'?: string;
    'background-repeat'?: string;
    'background-position'?: string;
    'max-width'?: string;
}
