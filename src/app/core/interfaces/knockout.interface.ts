import { Game, Player } from './'

export interface Knockout {
    _id? : string
    gid? : string
    g? : Game
    w? : Player
    l? : Player
    b? : number
    r? : boolean
    s? : Date // sort date
    date? : Date
    createdAt? : Date
    updatedAt? : Date
}

export interface KnockoutDeleted {
    notFound? : boolean
    deleted? : boolean
}

export interface KnockoutRequestMove {
    index : number
    up : boolean
}