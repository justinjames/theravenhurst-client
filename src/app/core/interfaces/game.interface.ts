import { Entry, Hand, Knockout, Player } from './';

export interface Game {
    _id?: string;
    d?: number;
    m?: number;
    y?: number;
    pot?: number;
    date?: Date;
    s?: number;
    createdAt?: Date;
    updatedAt?: Date;
    del?: boolean;
    name?: string;
    n?: string; // notes
    entries?: Entry[];
    knockouts?: Knockout[];
}

export interface GamesMonth {
    month: string;
    monthShort: string;
    m: number;
    y: number;
    games: Game[];
}

export interface GameWinner {
    first?: string;
    last?: string;
    name?: string;
    isMe?: boolean;
}

export interface LatestResults {
    game?: Game;
    entries?: Entry[];
    highHands?: Hand[];
}

export interface YearCount {
    count: number;
}

export interface CachedYear {
    date: Date;
    games: Game[];
}

export interface YearsHolder {
    [key: number]: CachedYear;
}

export interface YearInReviewPlayer {
    p: Player;
    pt: boolean;
    b: number;
    ks: number;
    c: number;
}

// export interface YearInReviewHighHand {
//     winner: string;
//     hand: Hand;
// }

export interface YearInReviewGame {
    _id: string;
    d: number;
    m: number;
    y: number;
    date: Date;
    name: string;
    n: string;
    winners: YearInReviewPlayer[];
    hh: Hand;
}
