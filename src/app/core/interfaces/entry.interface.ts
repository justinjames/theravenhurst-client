import { Player, Game } from "./";

export interface Entry {
    _id?: string;
    g?: Game;
    date?: Date; // game date
    p?: Player;
    pt?: boolean; // got points
    c?: number; // cash out amount
    b?: number; // number of buyins
    q?: number; // qualifies
    hh?: number; // played in high hand
    createdAt?: Date;
    updatedAt?: Date;
    ks?: number;
    deleted?: boolean;
}
