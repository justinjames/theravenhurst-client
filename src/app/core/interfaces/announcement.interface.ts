export interface Announcement {
    date? : string | Date
    text? : string
}