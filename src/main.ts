import { enableProdMode } from '@angular/core'
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic'

import { AppModule } from './app/app.module'
import { environment } from './environments/environment'

import { SessionUser } from './app/config'
import { IncomingUser } from './app/core'

import 'hammerjs'

import 'whatwg-fetch'

(() => {

    fetch('/api/user', { method: 'get', credentials: "same-origin" })
        .then(response => {
            return response.json()
        }).then(json => {
            SessionUser.user = json.user
            proceed()
        }).catch(err => {
            // console.log('parsing failed', err)
            proceed()
        })

    function proceed() {
        if (environment.production) {
            enableProdMode()
        }
        platformBrowserDynamic().bootstrapModule(AppModule)
    }
})()